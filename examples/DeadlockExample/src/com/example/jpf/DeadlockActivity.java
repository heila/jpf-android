package com.example.jpf;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * Very basic Android application containing only this DeadlockActivity. The Activity starts two Async tasks.
 * These Async tasks will deadlock when both Friends bow waiting for the other to bow back.
 * 
 * @author Heila van der Merwe
 * 
 */
public class DeadlockActivity extends Activity {

  // create two friends
  Friend alphonse = new Friend("Alphonse");
  Friend gaston = new Friend("Gaston");

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);

    // button 1 starts Gastron bowing at Alphonse
    final Button b1 = (Button) findViewById(R.id.button1);
    final Button b2 = (Button) findViewById(R.id.button2);

    b1.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        BowingTask bt = new BowingTask();
        bt.execute(gaston, alphonse);
      }
    });

    b2.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        BowingTask bt = new BowingTask();
        bt.execute(alphonse, gaston);
      }
    });

  }

  private static class BowingTask extends AsyncTask<Friend, Integer, String> {

    @Override
    protected String doInBackground(Friend... friend) {
      friend[0].bow(friend[1]);
      return "Bowed";
    }
  }

//  private static class BowingTask2 implements Runnable {
//
//    Friend[] friends = new Friend[2];
//
//    public BowingTask2(Friend... friend) {
//      friends[0] = friend[0];
//      friends[1] = friend[1];
//    }
//
//    @Override
//    public void run() {
//      friends[0].bow(friends[1]);
//    }
//  }
//
  static class Friend {
    private final String name;

    public Friend(String name) {
      this.name = name;
    }

    public String getName() {
      return this.name;
    }

    public synchronized void bow(Friend bower) {
      System.out.format("%s: %s" + "  has bowed to me!%n", this.name, bower.getName());
      bower.bowBack(this);
    }

    public synchronized void bowBack(Friend bower) {
      System.out.format("%s: %s" + " has bowed back to me!%n", this.name, bower.getName());
    }
  }

}