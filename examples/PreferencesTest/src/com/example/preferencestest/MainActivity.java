package com.example.preferencestest;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
  public static final String PREFS_NAME = "MyPrefsFile";

  boolean mSilentMode = false;

  TextView t = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    // SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
    SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
    boolean silent = settings.getBoolean("silentMode", false);

    t = (TextView) findViewById(R.id.prefs);

    setSilent(silent);

    Button b1 = (Button) findViewById(R.id.button1);
    b1.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        setSilent(!mSilentMode);

      }
    });

    Button b2 = (Button) findViewById(R.id.button2);
    b2.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("silentMode", mSilentMode);

        // Commit the edits!
        editor.commit();

        startActivity(new Intent(MainActivity.this, PreferenceTestActivity.class));

      }
    });

  }

  public void setSilent(boolean silent) {
    this.mSilentMode = silent;
    t.setText("Silent: " + silent);
  }

}
