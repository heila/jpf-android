JavaPathfinder core system v8.0 (rev ${version}) - (C) 2005-2014 United States Government. All rights reserved.


====================================================== system under test
com.example.preferencestest.MainActivity.main()

====================================================== search started: 2017/01/06 10:45 AM

====================================================== results
no errors detected

====================================================== statistics
elapsed time:       00:00:04
states:             new=8,visited=12,backtracked=20,end=0
search:             maxDepth=6,constraints=0
choice generators:  thread=3 (signal=0,lock=1,sharedRef=0,threadApi=0,reschedule=0), data=6
heap:               new=12555,released=7306,maxLive=4730,gcCycles=20
instructions:       211181
max memory:         125611MB
loaded code:        classes=324,methods=6652

====================================================== search finished: 2017/01/06 10:45 AM
