JavaPathfinder core system v8.0 (rev ${version}) - (C) 2005-2014 United States Government. All rights reserved.


====================================================== system under test
com.example.MainActivity.main()

====================================================== search started: 2017/01/05 11:09 AM

====================================================== results
no errors detected

====================================================== statistics
elapsed time:       00:00:05
states:             new=9,visited=16,backtracked=25,end=0
search:             maxDepth=8,constraints=0
choice generators:  thread=4 (signal=0,lock=1,sharedRef=0,threadApi=0,reschedule=0), data=6
heap:               new=12269,released=7607,maxLive=4491,gcCycles=25
instructions:       227031
max memory:         125611MB
loaded code:        classes=289,methods=6251

====================================================== search finished: 2017/01/05 11:10 AM
