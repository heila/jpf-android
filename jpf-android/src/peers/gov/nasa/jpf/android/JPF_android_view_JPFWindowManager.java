//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA). All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3. The NOSA has been approved by the Open Source
// Initiative. See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.android;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.annotation.MJI;
import gov.nasa.jpf.util.StateExtensionClient;
import gov.nasa.jpf.util.StateExtensionListener;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.NativePeer;

import java.util.logging.Logger;

/**
 * Implements the native methods of the WindowManager class.
 * 
 * One of the main functions of this class is to keep a list of all the View-objects of the application
 * registered in R.java. Their unique id's and names are resolved from the R.java file and stored as a map
 * <code>componentMap</code>. This is used during layout inflation of the views to make sure the views
 * registered in the R.java file gets assigned the right id. This is is used in the application's code to
 * identify the views.
 * 
 * The componentMap contains ViewEntry objects. They keep a reference to the actual View -objects in memory.
 * 
 */
public class JPF_android_view_JPFWindowManager extends NativePeer implements StateExtensionClient<String> {
  static Logger log = JPF.getLogger("JPF_android_view_WindowManager");

  static String windowName = "default";

  @MJI
  public void init0(MJIEnv env, int robj) {
    registerListener(env.getJPF());
  }

  @MJI
  public static void setNewWindow(MJIEnv env, int clsObject, int windowRef) {
    // get event tree and set
    windowName = env.getStringObject(windowRef);
    JPF_gov_nasa_jpf_util_event_AndroidEventProducer.setNewWindow(windowName);
  }

  /**
   * Returns the name of the current Window that is showing on the screen. It is used to determine which part
   * of the script has to be executed.
   * 
   * @param env
   * @return the name of the section of the script to execute or default if no window is currently on the
   *         screen
   */
  public static String getCurrentWindow(MJIEnv env) {
    return windowName;
  }

  @Override
  public String getStateExtension() {
    return windowName;
  }

  @Override
  public void restore(String stateExtension) {
    this.windowName = stateExtension;
  }

  @Override
  public void registerListener(JPF jpf) {
    StateExtensionListener sel = new StateExtensionListener<String>(this);
    jpf.addSearchListener(sel);
  }
}
