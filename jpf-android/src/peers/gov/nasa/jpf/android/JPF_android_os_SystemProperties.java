package gov.nasa.jpf.android;

import gov.nasa.jpf.annotation.MJI;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.NativePeer;

import java.util.HashMap;
import java.util.Map;

public class JPF_android_os_SystemProperties extends NativePeer {

  static Map<String, Object> properties = new HashMap<String, Object>();


  @MJI
  public static int native_get__Ljava_lang_String_2__Ljava_lang_String_2(MJIEnv env, int objRef, int skey) {
    String key = env.getStringObject(skey);
    return env.newString("");

  }

  @MJI
  public static int native_get__Ljava_lang_String_2Ljava_lang_String_2__Ljava_lang_String_2(MJIEnv env,
                                                                                            int objRef,
                                                                                            int key, int def) {
    return def;
  }

  @MJI
  public static int native_get_int(MJIEnv env, int objRef, int key, int def) {
    return def;
  }

  @MJI
  public static long native_get_long(MJIEnv env, int objRef, int key, long def) {
    return def;

  }

  @MJI
  public static boolean native_get_boolean(MJIEnv env, int objRef, int key, boolean def) {
    return def;
  }

  @MJI
  public static void native_set(MJIEnv env, int objRef, int key, int def) {
  }

  @MJI
  public static void native_add_change_callback(MJIEnv env, int objRef) {

  }

}
