package gov.nasa.jpf.android;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.annotation.MJI;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.NativePeer;

import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;

import nhandler.conversion.ConversionException;
import nhandler.conversion.jvm2jpf.JVM2JPFConverter;

/**
 * Native peer of the LayoutInflater. Makes use of {@link DocumentBuilder} to
 * parse the layout files.
 * 
 * @author "Heila van der Merwe"
 * 
 */
public class JPF_android_preference_PreferenceInflater extends NativePeer {
  public final static String TAG = JPF_android_preference_PreferenceInflater.class.getSimpleName();
  static Logger log = JPF.getLogger("gov.nasa.jpf.android");
  public static final JPFXMLParser parser = JPFXMLParser.getInstance();

  @MJI
  public int loadXMLFile(MJIEnv env, int objref, int resourceID) {
    String filename = "";
    try {
      filename = parser.loadXML(resourceID, "prefs");
    } catch (Exception e) {
      log.severe("PreferenceInflater could not parse file: " + filename);
      throw new RuntimeException("PreferenceInflater could not parse file: " + filename);
    }

    // now the layout file is loaded and we have its LayoutInfo
    return env.newString(filename);
  }

  @MJI
  public int getRootHash(MJIEnv env, int objref, int resourceID) {
    // retrieve the file name of the layout resource
    return parser.getRootHash(resourceID);
  }

  @MJI
  public int getNodeInfo(MJIEnv env, int objref, int hashcode, int resourceID) {
    String[] nodeInfo = parser.getNodeInfo(hashcode, resourceID);
    if (nodeInfo == null) {
      log.warning(TAG + ": Could not find XML node with hashcode " + hashcode);
      return MJIEnv.NULL;
    }
    return env.newStringArray(nodeInfo);

  }

  @MJI
  public int getNodeAttributes(MJIEnv env, int objref, int hashcode, int resourceID) {
    Object[] returnAtt = parser.getNodeAttributes(hashcode, resourceID);
    if (returnAtt == null) {
      log.warning(TAG + ": Could not find XML node with hashcode " + hashcode);
      return MJIEnv.NULL;
    }
    int refArr = MJIEnv.NULL;
    for (Object att : returnAtt) {
      Object[] attribute = (Object[]) att;
      String attName = (String) attribute[1];
      if (attName.startsWith("@")) {
        Object value = getValue(attName);
        if (value != null) {
          attribute[1] = value;
        } else {
          log.warning("Could not find value for attribute: " + attName);

        }
      }
    }

    try {
      refArr = JVM2JPFConverter.obtainJPFObj(returnAtt, env);
    } catch (ConversionException e) {
      e.printStackTrace();
    }
    return refArr;
  }

  @MJI
  public int getChildren(MJIEnv env, int objref, int hashcode, int resourceID) {

    int[] returns = parser.getChildren(hashcode, resourceID);
    if (returns != null) {
      return env.newIntArray(returns);
    }
    return MJIEnv.NULL;

  }

  @MJI
  public int getRValue(MJIEnv env, int objref, int resourceNameRef) {
    String resourceName = env.getStringObject(resourceNameRef);
    if (resourceName.startsWith("@+id/")) {
      resourceName = resourceName.substring(5);
      return AndroidProjectInfo.get().getRFile().getViewIdForName(resourceName);
    } else if (resourceName.startsWith("@layout/")) {
      resourceName = resourceName.substring(8);
      return AndroidProjectInfo.get().getRFile().getLayoutIdForName(resourceName);
    }
    log.warning("PreferenceInflator could not get ID for resource with name " + resourceName);
    return -1;
  }

  public Object getValue(String attName) {
    if (AndroidProjectInfo.isInitialized()) {
      Object value = AndroidProjectInfo.get().getValue(attName);
      if (value == null)
        return new Object();
      else
        return value;
    } else {
      if (attName.startsWith("@string")) {
        return "value";
      } else if (attName.startsWith("@array")) {
        return new String[] { "0", "2", "3", "4" };
      } else if (attName.startsWith("@bool")) {
        return false;
      } else {
        return attName;
      }
    }

  }

}
