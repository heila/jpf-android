package gov.nasa.jpf.android;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.annotation.MJI;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.NativePeer;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.UnknownHostException;
import java.util.logging.Logger;

import android.util.Log;

/**
 * Implements the native methods of android.util.Log. Writes the log as a JPF log with sort of the same log
 * level.
 * 
 * @author "Heila van der Merwe"
 * 
 */
public class JPF_android_util_Log extends NativePeer {
  static Logger log = JPF.getLogger("gov.nasa.jpf.android");

  private static final String TAG = "[APP] ";

  @MJI
  public int println_native(MJIEnv env, int clsObjRef, int v0, int type, int stag, int smsg) {

    String msg = env.getStringObject(smsg);
    String tag = env.getStringObject(stag);

    switch (type) {
    case Log.INFO:
      System.out.println(TAG + tag + ": " + msg);
      break;
    case Log.DEBUG:
      System.out.println(TAG + tag + ": " + msg);
      break;
    case Log.ERROR:
      System.out.println(TAG + tag + ": " + msg);
      break;
    case Log.WARN:
      System.out.println(TAG + tag + ": " + msg);
      break;
    case Log.VERBOSE:
      System.out.println(TAG + tag + ": " + msg);
      break;
    case Log.ASSERT:
      System.out.println(TAG + tag + ": " + msg);
      break;
    default:
      System.out.println(TAG + tag + ": " + msg);
    }
    return msg.length();
  }

  @MJI
  public static int getStackTraceString(MJIEnv env, int clsObjRef, int itr) {
    Throwable tr = null;
    // if (itr != MJIEnv.NULL) {
    // try {
    // tr = (Throwable) JPF2JVMConverter.obtainJVMObj(itr, env);
    // } catch (ConversionException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
    // }
    if (tr == null) {
      return env.newString("");
    }

    // This is to reduce the amount of log spew that apps do in the non-error
    // condition of the network being unavailable.
    Throwable t = tr;
    while (t != null) {
      if (t instanceof UnknownHostException) {
        return env.newString("");
      }
      t = t.getCause();
    }

    OutputStream sw = new ByteArrayOutputStream(245);
    PrintStream s = new PrintStream(sw);
    tr.printStackTrace(s);
    s.flush();
    return env.newString(sw.toString());
  }

  @MJI
  public boolean isLoggable(MJIEnv env, int clsObjRef, int rString0, int v1) {
    boolean v = true;
    return v;
  }

}