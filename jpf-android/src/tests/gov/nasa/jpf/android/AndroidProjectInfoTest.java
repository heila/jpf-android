package gov.nasa.jpf.android;

import gov.nasa.jpf.android.AndroidProjectInfo.BuildType;
import gov.nasa.jpf.android.AndroidProjectInfo.ProjectParseException;
import gov.nasa.jpf.util.test.TestJPF;

import java.util.Arrays;

import org.junit.Test;

public class AndroidProjectInfoTest extends TestJPF {

  @Test
  public void testParseRFile() {
    AndroidProjectInfo info = new AndroidProjectInfo("src/tests/gov/nasa/jpf/android",
        BuildType.ANT);

    try {
      info.parseRFile("src/tests/gov/nasa/jpf/android/R.java");
    } catch (ProjectParseException e) {
      e.printStackTrace();
      assertTrue("Exception in parsing Rfile", false);
    }
    RFile r = info.getRFile();
    assertNotNull(r);
    assertTrue(r.getArrayNameForId(0x7f080002).equals("clipboard_timeout_options"));

  }

  @Test
  public void testParseManifest() {
    AndroidProjectInfo info = new AndroidProjectInfo("src/tests/gov/nasa/jpf/android",
        BuildType.ANT);

    try {
      info.parseAndroidManifest("src/tests/gov/nasa/jpf/android/AndroidManifest.xml");
      assertNotNull(info.getAndroidManifest());
    } catch (ProjectParseException e) {
      e.printStackTrace();
    }

  }

  @Test
  public void testParseResourceValues() {
    AndroidProjectInfo info = new AndroidProjectInfo("src/tests/gov/nasa/jpf/android",
        BuildType.ANT);

    try {
      info.parseRFile("src/tests/gov/nasa/jpf/android/R2.java");
      info.parseResourceValues("src/tests/gov/nasa/jpf/android");
      String[] a = (String[]) info.getValue("@array/scheduleFreq");
      String[] a2 = info.getArrayObject("scheduleFreq");
      String[] a3 = info.getArrayObject("scheduleFreqValues");

      String[] b = { "Never", "Daily", "Weekly", "Monthly" };
      assertTrue(Arrays.equals(a, b));
      assertTrue(Arrays.equals(a2, b));
      assertTrue(Arrays.equals(a3, new String[] { "0", "1", "2", "3" }));

      String s = info.getStringValue("resultsdescription.notfound");
      assertTrue(s.equals("Contact not found"));

    } catch (ProjectParseException e) {
      e.printStackTrace();
    }

  }

}
