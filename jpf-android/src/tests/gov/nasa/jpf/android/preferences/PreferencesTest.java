package gov.nasa.jpf.android.preferences;

import gov.nasa.jpf.util.test.TestJPF;

import org.junit.Test;

import android.app.ContextImpl;
import android.preference.Preference;
import android.preference.PreferenceGroup;
import android.preference.PreferenceInflater;

public class PreferencesTest extends TestJPF {

  @Test
  public void testInflate() {
    if (verifyNoPropertyViolation()) {
      ContextImpl c = new ContextImpl();
      PreferenceInflater p = new PreferenceInflater(c);
      Preference pref = p.inflate(-1, (Preference) null);
      visit(pref, "");
    }
  }

  public void printPrefernce(Preference p, String space) {
    System.out.println(space + p.toString());
  }

  public void visit(Preference pref, String space) {
    printPrefernce(pref, space);

    if (pref instanceof PreferenceGroup) {
      // print children
      for (int i = 0; i < ((PreferenceGroup) pref).getPreferenceCount(); i++) {
        Preference child = ((PreferenceGroup) pref).getPreference(i);
        visit(child, space + " ");
      }

    }
  }

}
