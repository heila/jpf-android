SCRIPT {
	SECTION default {
		ANY {
		CHOICE {
			    $button0.onClick()
		}
		CHOICE {
			ANY {
				CHOICE {
				    $button1.onClick()
				}
				CHOICE {
				    $button2.onClick()
				}
				CHOICE {
				    $button3.onClick()
				}
			}
		}
		CHOICE {
			REPEAT 2 {
			REPEAT 2 {
			    $button4.onClick()
			}
			}
		}
		}
	}
}