package gov.nasa.jpf.test.util.event.tree;

import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.NoEvent;
import gov.nasa.jpf.util.event.tree.EventNode;
import gov.nasa.jpf.util.event.tree.EventTree;
import gov.nasa.jpf.util.test.TestJPF;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class TestEventTree extends TestJPF {

  @Test
  public void testEmptyTree() {
    EventTree tree = new EventTree();
    Assert.assertNotNull(tree);
  }

  @Test
  public void testEmptyRoot() {
    EventTree tree = new EventTree();
    tree.setRoot(null);
    Assert.assertTrue(tree.root() == null);
  }

  @Test
  public void testNoEventRoot() {
    EventTree tree = new EventTree();
    tree.setRoot(new NoEvent());
    Assert.assertNotNull(tree.root());
    Assert.assertTrue(tree.root().getEvent() instanceof NoEvent);
  }

  @Test
  public void testAddEvent() {
    EventTree tree = new EventTree();
    EventNode e1 = new EventNode(new NoEvent());
    EventNode e2 = tree.addEvent(e1, new NoEvent());
    List<EventNode> children = new LinkedList<EventNode>();
    for (Iterator<EventNode> it = e1.getChildren(); it.hasNext();) {
      children.add(it.next());
    }
    Assert.assertTrue(children.get(0).equals(e2));
    Assert.assertEquals(1, children.size());
    Assert.assertTrue(e2.getParent().equals(e1));
  }

  @Test
  public void testSizeZero() {
    EventTree tree = new EventTree();
    Assert.assertTrue(tree.root() == null);
    Assert.assertEquals(0, tree.size());
  }

  @Test
  public void testSizeOne() {
    EventTree tree = new EventTree();
    tree.setRoot(new NoEvent());
    Assert.assertNotNull(tree.root());
    Assert.assertTrue(tree.root().getEvent() instanceof NoEvent);
    Assert.assertEquals(1, tree.size());
  }

  @Test
  public void testSizeMany() {
    EventTree tree = new EventTree();
    Event tmp = new NoEvent();
    tree.setRoot(tmp);
    EventNode tmp2 = tree.addEvent(tree.root(), tmp);
    tmp2 = tree.addEvent(tmp2, tmp);
    tmp2 = tree.addEvent(tmp2, tmp);

    Assert.assertEquals(4, tree.size());

  }

  @Test
  public void testManyPrint() {
    EventTree tree = new EventTree();
    Event tmp = new NoEvent();
    tree.setRoot(tmp);
    EventNode tmp2 = tree.addEvent(tree.root(), tmp);
    tmp2 = tree.addEvent(tmp2, tmp);
    tmp2 = tree.addEvent(tmp2, tmp);
    PrintWriter w = new PrintWriter(System.out);
    tree.printTree(w);
    w.flush();
    w.close();
    ByteArrayOutputStream output = new ByteArrayOutputStream();
    w = new PrintWriter(output);
    tree.printTree(w);
    w.flush();
    Assert.assertEquals("NoEvent []\n" + ". NoEvent []\n" + ". . NoEvent []\n" + ". . . NoEvent []", output
        .toString().trim());

  }

  @Test
  public void testZeroPrint() {
    EventTree tree = new EventTree();
    PrintWriter w = new PrintWriter(System.out);
    tree.printTree(w);
    w.flush();
    w.close();
    ByteArrayOutputStream output = new ByteArrayOutputStream();
    w = new PrintWriter(output);
    tree.printTree(w);
    w.flush();
    Assert.assertEquals("Empty", output.toString().trim());
    w.close();
  }

  @Test
  public void testTreePrint() {
    EventTree tree = new EventTree();
    Event t1 = new NoEvent("root");
    Event t2 = new NoEvent("2");
    Event t3 = new NoEvent("3");
    Event t4 = new NoEvent("4");
    Event t5 = new NoEvent("5");

    tree.setRoot(t1);

    EventNode tmp2 = tree.addEvent(tree.root(), t2);
    EventNode tmp3 = tree.addEvent(tree.root(), t3);

    EventNode tmp4 = tree.addEvent(tmp3, t4);
    EventNode tmp5 = tree.addEvent(tmp4, t5);

    PrintWriter w = new PrintWriter(System.out);
    tree.printTree(w);
    w.flush();
    w.close();
    ByteArrayOutputStream output = new ByteArrayOutputStream();
    w = new PrintWriter(output);
    tree.printTree(w);
    w.flush();
    System.out.println("NoEvent [root]\n" + ". NoEvent [2]\n" + ". . NoEvent [4]\n" + ". . . NoEvent [5]");
    System.out.println(output.toString().trim());
    Assert.assertEquals("NoEvent [root]\n" + ". NoEvent [2]\n" + ". . NoEvent [4]\n"
 + ". . . NoEvent [5]",
        output.toString().trim());
    w.close();
  }

  @Test
  public void testIsExternal() {
    EventTree tree = new EventTree();
    Event tmp = new NoEvent();
    tree.setRoot(tmp);
    EventNode tmp2 = tree.addEvent(tree.root(), tmp);
    tmp2 = tree.addEvent(tmp2, tmp);
    tmp2 = tree.addEvent(tmp2, tmp);

    Assert.assertTrue(tree.isExternal(tmp2));
    Assert.assertFalse(tree.isExternal(tree.root()));

  }

  @Test
  public void testIsInternal() {
    EventTree tree = new EventTree();
    Event tmp = new NoEvent();
    tree.setRoot(tmp);
    EventNode tmp2 = tree.addEvent(tree.root(), tmp);
    EventNode tmp3 = tree.addEvent(tmp2, tmp);
    EventNode tmp4 = tree.addEvent(tmp3, tmp);

    Assert.assertTrue(tree.isInternal(tmp3));
    Assert.assertFalse(tree.isInternal(tmp4));

  }

  @Test
  public void testIsRoot() {
    EventTree tree = new EventTree();
    Event tmp = new NoEvent();
    tree.setRoot(tmp);
    EventNode node = new EventNode(tmp);
    Assert.assertTrue(tree.isRoot(tree.root()));
    Assert.assertFalse(tree.isRoot(node));
  }

  @Test
  public void testDepth() {
    EventTree tree = new EventTree();
    Assert.assertEquals(0, tree.getDepth(tree.root()));

    Event tmp = new NoEvent();
    tree.setRoot(tmp);
    EventNode tmp2 = tree.addEvent(tree.root(), tmp, 0);
    tmp2 = tree.addEvent(tmp2, tmp, 0);
    tmp2 = tree.addEvent(tmp2, tmp, 0);

    Assert.assertEquals(4, tree.getDepth(tmp2));
    Assert.assertEquals(1, tree.getDepth(tree.root()));

  }

  @Test
  public void testIsEmpty() {
    EventTree tree = new EventTree();
    Assert.assertTrue(tree.isEmpty());

    Event tmp = new NoEvent();
    tree.setRoot(tmp);

    Assert.assertFalse(tree.isEmpty());

  }

}
