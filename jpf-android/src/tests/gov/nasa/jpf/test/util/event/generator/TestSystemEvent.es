SCRIPT {
	SECTION default {
		@intent2.setAction("Test")
		startActivity(@intent2)
		
		@intent2.putExtraString("Test2", "String")
		startActivity(@intent2)
		
		@intent2.putExtraInt("TestInt",2)
		startActivity(@intent2)
		
		@intent2.putExtraBool("TestBool",true)
		startActivity(@intent2)
		
		
		@intent2.putExtraFloat("TestFloat",2.2)
		startActivity(@intent2)
		
		
		@intent2.putExtraDouble("TestDouble",333)
		startActivity(@intent2)
		
		
		@intent2.putExtraByte("TestByte",1)
		startActivity(@intent2)
		
		
		@intent2.putExtraChar("TestChar","c")
		startActivity(@intent2)
		
		
		@intent2.putExtraShort("TestShort",11)
		startActivity(@intent2)
		
		
		@intent3.setComponent("com.android.test.Tester")
		startActivity(@intent3)
		
		@intent2.addCategory("CATEGORY")
		startActivity(@intent2)
		
	}
}