package gov.nasa.jpf.test.android.os;

import gov.nasa.jpf.util.TypeRef;
import gov.nasa.jpf.util.test.TestJPF;

import org.junit.Test;

import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.os.MockMainLooper;

/**
 * 
 * @author Heila van der Merwe
 * 
 */
public class LooperTest extends TestJPF {
  static final TypeRef PROPERTY = new TypeRef("gov.nasa.jpf.listener.PreciseRaceDetector");
  static final String LISTENER = "+listener=gov.nasa.jpf.listener.PreciseRaceDetector";

  @Test
  public void testLooper() {

    if (verifyNoPropertyViolation()) {
      MockMainLooper.prepare();
      Handler H = new Handler(new Callback() {
        public boolean handleMessage(Message msg) {
          System.out.println("Message " + msg.arg1 + msg.arg2);
          return false;
        }
      });

      Message m1 = new Message();
      m1.arg1 = 1;
      H.sendMessage(m1);
      Message m2 = new Message();
      m2.arg2 = 2;
      H.sendMessage(m2);
      // H.sendMessage(new Message());
      MockMainLooper.loop();

      Message m3 = new Message();
      m3.arg2 = 3;
      boolean t = H.sendMessage(m3);
      System.out.println("Message 3 delivered?" + t);
      Message m4 = new Message();
      m4.arg2 = 4;
      t = H.sendMessage(m4);
      System.out.println("Message 4 delivered?" + t);

      Message m5 = new Message();
      m5.arg2 = 5;
      t = H.sendMessage(m5);
      System.out.println("Message 5 delivered?" + t);

    }

  }

  public static void main(String testMethods[]) throws Throwable {
    runTestsOfThisClass(testMethods);
  }

}