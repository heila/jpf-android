package gov.nasa.jpf.test.android;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.util.test.TestJPF;

import org.junit.Before;
import org.junit.Test;

public class AndroidThreadReduceTest extends TestJPF {
  @FilterField
  String config = "+listener+=,gov.nasa.jpf.jvm.CGListener,gov.nasa.jpf.jvm.ThreadCGSingleChoiceListener,";

  @Before
  public void setUp() {
  }

  int i;
  int j;

  @Test
  public void testParseActivity() {
    if (verifyNoPropertyViolation(config)) {
      new Thread(new Runnable() {

        @Override
        public void run() {
          // for (i = 0; i < 1; i++) {
            System.out.println("i = " + i);
          // }
        }
      }).start();
      new Thread(new Runnable() {

        @Override
        public void run() {
          // for (j = 0; j < 1; j++) {
            System.out.println("j = " + j);
          // }
        }
      }).start();
    }
  }
}
