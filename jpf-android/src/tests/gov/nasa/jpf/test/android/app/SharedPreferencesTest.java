package gov.nasa.jpf.test.android.app;

import gov.nasa.jpf.util.test.TestJPF;

import org.junit.Test;

import android.app.ContextImpl;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.MockMainLooper;

public class SharedPreferencesTest extends TestJPF {

  @Test
  public void testSharedPrefStoreAndRead() {

    if (verifyNoPropertyViolation()) {
      MockMainLooper.prepareMainLooper();
      ContextImpl c = new ContextImpl();

      SharedPreferences sharedPref = c.getSharedPreferences("android.app.TestActivity",
          Context.MODE_PRIVATE);
      SharedPreferences.Editor editor = sharedPref.edit();
      editor.putInt("Test", 45);
      editor.commit();

      MockMainLooper.testloop();

      sharedPref = c.getSharedPreferences("android.app.TestActivity", Context.MODE_PRIVATE);
      int defaultValue = 5;
      long highScore = sharedPref.getInt("Test", defaultValue);
      System.out.println("Found sharedPrefs: " + highScore);
      assertEquals(45, highScore);

      MockMainLooper.getMainLooper().quitSafely();
    }

  }

  @Test
  public void testSharedPrefListener() {

    if (verifyNoPropertyViolation()) {
      MockMainLooper.prepareMainLooper();
      ContextImpl c = new ContextImpl();

      SharedPreferences sharedPref = c.getSharedPreferences("android.app.TestActivity",
          Context.MODE_PRIVATE);
      sharedPref.registerOnSharedPreferenceChangeListener(new OnSharedPreferenceChangeListener() {

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
          System.out.println("prefs changed: " + key);
        }
      });
      SharedPreferences.Editor editor = sharedPref.edit();
      editor.putInt("Test", 45);
      editor.commit();

      MockMainLooper.testloop();

      sharedPref = c.getSharedPreferences("android.app.TestActivity", Context.MODE_PRIVATE);
      int defaultValue = 5;
      long highScore = sharedPref.getInt("Test", defaultValue);
      System.out.println("Found sharedPrefs: Test=" + highScore);
      assertEquals(45, highScore);

      MockMainLooper.getMainLooper().quitSafely();
    }

  }

}
