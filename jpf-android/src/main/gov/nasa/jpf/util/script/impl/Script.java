package gov.nasa.jpf.util.script.impl;

import java.util.List;

public class Script extends ScriptElementContainer {

  String name;

  public Script(int linenumber) {
    super(linenumber, null);
  }

  public Script(int linenumber, ScriptElementContainer parent) {
    super(linenumber, parent);
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }
  
  public List<Section> getSections(){
    
    return (List<Section>) getChildren();
  }
}
