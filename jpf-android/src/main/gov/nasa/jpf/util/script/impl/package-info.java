/*
 * Copyright 2013 Heila van der Merwe
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

/**
 * This package (and sub packages) contains the script objects into which the JPF-Android script is parsed
 * and is used to build an Abstract Syntax Tree (AST) of the script.
 * 
 * The abstract models of the script elements are:
 * 
 *  + {@link za.vdm.translator.script.ScriptElement}
 *  + {@link za.vdm.translator.script.ScriptElementContainer}
 * 
 * These models are implemented by classes in the sub-package `za.vdm.translator.script.impl` to 
 * form the actual script elements. 
 * 
 * 
 */
package gov.nasa.jpf.util.script.impl;

