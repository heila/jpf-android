package gov.nasa.jpf.util.script.impl;


public interface Visitor {

  public void visit(ScriptElement element);

  public void visit(ScriptElementContainer element);
}
