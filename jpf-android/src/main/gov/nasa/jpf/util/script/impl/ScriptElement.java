package gov.nasa.jpf.util.script.impl;


/**
 * This class is the base class of all elements of the JPF-Android input script. It represents a node in the
 * Abstract Syntax Tree (AST) into which the script in parsed. It stores 3 main attributes for each element:
 * 
 * + The {@link ScriptElement#linenumber line number} of the element in the script.
 * 
 * + The {@link ScriptElement#parent parent} element of this script element (used to navigate up in the AST.
 * 
 * + The {@link ScriptElement#sibling sibling} of the script element to move to the next element in the
 * script.
 * 
 * @author Heila <heila@ml.sun.ac.za>
 * 
 */
public class ScriptElement {

  public static int S_ID = -1;

  public int id = -1;

  /**
   * Stores the number of the line where this script element appears in the input script (used to notify user
   * of where an error happened).
   */
  protected int linenumber;

  /**
   * Stores a reference to the parent of this script element. This is used to navigate the AST.
   * 
   */
  protected ScriptElementContainer parent;

  /**
   * Stores the number of the line where this script element appears in the input script (used to notify user
   * of where an error happened).
   */
  protected ScriptElement sibling;

  /**
   * This constructor is used to create a new script element. It does not take the sibling of an element as
   * parameter since the sibling might only be known later.
   * 
   * @param linenumber
   *          The number of the line in the script the element is located.
   * @param parent
   *          Reference to the parent script element.
   */
  public ScriptElement(int linenumber, ScriptElementContainer parent) {
    this.linenumber = linenumber;
    this.parent = parent;
    this.sibling = null;
    if (parent != null)
      parent.addChild(this);

    this.id = ++S_ID;
  }


  /**
   * 
   * @return the parent script element
   */
  public ScriptElementContainer getParent() {
    return parent;
  }

  /**
   * 
   * @return the line number the element appeared on in the script.
   */
  public int getLine() {
    return linenumber;
  }

  /**
   * 
   * @return the next sibling in the Abstract Syntax Tree (AST)
   */
  public ScriptElement getNextSibling() {
    return sibling;
  }

  /**
   * Sets the sibling of this script element
   * 
   * @param element
   */
  public void setNextSibling(ScriptElement element) {
    sibling = element;
  }

  /**
   * 
   * This is used to traverse the Abstract Syntax Tree (AST) of the script and visit each node of the tree.
   * 
   * @param v
   * @throws Exception
   */
  public void accept(Visitor v) {
    v.visit(this);
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

}
