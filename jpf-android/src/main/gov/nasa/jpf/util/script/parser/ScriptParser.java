package gov.nasa.jpf.util.script.parser;

import gov.nasa.jpf.util.StringExpander;
import gov.nasa.jpf.util.script.impl.Any;
import gov.nasa.jpf.util.script.impl.Any.Choice;
import gov.nasa.jpf.util.script.impl.Event;
import gov.nasa.jpf.util.script.impl.Repeat;
import gov.nasa.jpf.util.script.impl.RepeatInfinate;
import gov.nasa.jpf.util.script.impl.ResetRepeat;
import gov.nasa.jpf.util.script.impl.Script;
import gov.nasa.jpf.util.script.impl.ScriptElement;
import gov.nasa.jpf.util.script.impl.ScriptElementContainer;
import gov.nasa.jpf.util.script.impl.Section;

import java.io.Reader;
import java.util.LinkedList;
import java.util.List;

/**
 * <pre>
 * script = 'SCRIPT' '{' { section } '}'.
 * section = 'SECTION' ( 'default' | id ) '{' { sequence } '}'. 
 * sequence = iteration | selection | event.
 * iteration = 'REPEAT' num '{' { sequence } '}'.
 * iteration = 'REPEAT' * '{' { sequence } '}'.
 * selection = 'ANY' '{' choice { choice } '}'.
 * choice = CHOICE '{' { sequence } '}'.
 * event = uievent | sysevent
 * 
 * uievent = '$' target '.' action '(' params ')'.
 * sysevent = ['@' target '.' | 'device' '.' ] action '(' params ')'. target = id.
 * action = id.
 * params = [ param {, param } ].
 * param = id | '"' id '"'.
 * id = letter { letter | digit | '_' }.
 * num = digit { digit }.
 * </pre>
 * 
 * 
 * @author Heila <heilamvdm@gmail.com>
 * 
 */
public class ScriptParser {

  private ScriptScanner scanner;
  private Token t;

  public ScriptParser(Reader r) throws ParseException {
    scanner = new ScriptScanner(r);
    t = scanner.getToken(); // 1 symbol lookahead
  }

  public Script parse() throws ParseException {
    if (t == null) {
      throw new ParseException("Could not parse script file", 0);
    }
    return script();
  }

  protected Script script() throws ParseException {

    Script s = new Script(t.getLinenumber());
    matchOrThrow(TokenType.SCRIPT);
    matchOrThrow(TokenType.LBRACE);

    while (t.getType().equals(TokenType.SECTION)) {
      section(s);
    }

    matchOrThrow(TokenType.RBRACE);

    return s;

  }

  private void section(ScriptElementContainer parent) throws ParseException {
    matchOrThrow(TokenType.SECTION);
    String id = "";
    if (t.getType().equals(TokenType.ID)) {
      id = t.getId();
      t = scanner.getToken();
    }
    matchOrThrow(TokenType.LBRACE);

    Section section = new Section(t.getLinenumber(), parent);
    section.setName(id);

    ScriptElement e = null;

    matchSequence(section);

    matchOrThrow(TokenType.RBRACE);
  }

  /**
   * Parses the repeat structure in the script into Repeat objects.
   * 
   * @param parent
   * @throws ParseException
   */
  private void repeat(ScriptElementContainer parent) throws ParseException {
    matchOrThrow(TokenType.REPEAT);
    Repeat r = null;

    if (t.getType().equals(TokenType.STAR)) {
      // Infinite repeat
      r = new RepeatInfinate(t.getLinenumber(), parent);
      t = scanner.getToken();

    } else {

      int num = (int) Math.floor(t.getValue());
      t = scanner.getToken();

      ResetRepeat rr = new ResetRepeat(t.getLinenumber(), parent);
      r = new Repeat(t.getLinenumber(), parent);
      r.setNumber(num);

    }
    matchOrThrow(TokenType.LBRACE);

    matchSequence(r);

    matchOrThrow(TokenType.RBRACE);

  }

  private void any(ScriptElementContainer parent) throws ParseException {

    matchOrThrow(TokenType.ANY);

    matchOrThrow(TokenType.LBRACE);

    Any a = new Any(t.getLinenumber(), parent);
    while (!t.getType().equals(TokenType.EOF) && !t.getType().equals(TokenType.RBRACE)) {
      choice(a);
    }

    matchOrThrow(TokenType.RBRACE);

  }

  private void choice(ScriptElementContainer parent) throws ParseException {
    matchOrThrow(TokenType.CHOICE);

    matchOrThrow(TokenType.LBRACE);

    Choice c = new Choice(t.getLinenumber(), parent);

    matchSequence(c);

    matchOrThrow(TokenType.RBRACE);

  }

  private void event(ScriptElementContainer parent) throws ParseException {
    String id = "";
    String param = ""; // to be expanded

    if (t.getType().equals(TokenType.ID)) {
      id = t.getId();
      t = scanner.getToken();
    } else
      throw new ParseException("Expected Event got " + t.getType().toString());

    matchOrThrow(TokenType.LPAREN);
    param += "(";

    List<Object> params = new LinkedList<>();
    while (!t.getType().equals(TokenType.EOF) && !t.getType().equals(TokenType.RPAREN)) {
      if (t.getType().equals(TokenType.ID)) {
        params.add(t.getId());
        param += t.getId() + ",";
        t = scanner.getToken();
      } else if (t.getType().equals(TokenType.NUMBER)) {
        params.add(t.getValue());
        param += t.getValue() + ",";
        t = scanner.getToken();
      }
    }
    if (param.endsWith(","))
      param = param.substring(0, param.length() - 1);
    matchOrThrow(TokenType.RPAREN);
    param += ")";

    StringExpander ex = new StringExpander(id + param);
    List<String> expanded = ex.expand();

    if (expanded.size() > 1) {
      expandAny(expanded, parent);

    } else {

      Event e = new Event(t.getLinenumber(), parent);

      String[] ev = id.split("\\.");
      if (ev.length > 1) {
        e.setTarget(ev[0]);
        e.setAction(ev[1]);
      } else {
        e.setAction(ev[0]);
      }

      for (Object p : params) {
        e.addParam(p);
      }
    }

  }

  private void expandAny(List<String> expanded, ScriptElementContainer parent) {
    Any a = new Any(t.getLinenumber(), parent);
    for (String s : expanded) {

      Choice c = new Choice(t.getLinenumber(), a);
      Event e = new Event(t.getLinenumber(), c);

      String[] ev = s.split("\\.");
      int indexDot = s.indexOf('.');
      String params = null;

      if (ev.length > 1) {
        int indexParen = ev[1].indexOf("(");
        e.setTarget(s.substring(0, indexDot));
        e.setAction(ev[1].substring(0, indexParen));
        params = ev[1].substring(indexParen + 1, ev[1].indexOf(")"));
      } else {
        int indexParen = ev[0].indexOf("(");
        e.setAction(ev[0].substring(0, indexParen));
        params = ev[0].substring(indexParen + 1, ev[0].indexOf(")"));
      }
      if (params != null) {
        for (String p : params.split(",")) {
          if (p != null)
            e.addParam(StringParser.getObjectFromString(p));
        }
      }
    }

  }

  private void matchSequence(ScriptElementContainer parent) throws ParseException {
    ScriptElement e = null;

    while (!t.getType().equals(TokenType.EOF) && !t.getType().equals(TokenType.RBRACE)) {
      if (t.getType().equals(TokenType.REPEAT)) {
        repeat(parent);
      } else if (t.getType().equals(TokenType.ANY)) {
        any(parent);
      } else if (t.getType().equals(TokenType.ID)) {
        event(parent);
      } else {
        // no such token
      }
    }
  }

  private boolean match(TokenType type) throws ParseException {
    if (t.getType().equals(type)) {
      t = scanner.getToken();
      return true;
    }
    return false;
  }

  private void matchOrThrow(TokenType type) throws ParseException {
    if (t.getType().equals(type)) {
      t = scanner.getToken();
    } else
      throw new ParseException("Expected Token " + type.name() + " got token " + t.toString() + ".");
  }
}
