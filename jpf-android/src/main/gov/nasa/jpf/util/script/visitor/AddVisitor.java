package gov.nasa.jpf.util.script.visitor;

import gov.nasa.jpf.util.script.impl.Any;
import gov.nasa.jpf.util.script.impl.Any.Choice;
import gov.nasa.jpf.util.script.impl.Event;
import gov.nasa.jpf.util.script.impl.Repeat;
import gov.nasa.jpf.util.script.impl.ResetRepeat;
import gov.nasa.jpf.util.script.impl.Script;
import gov.nasa.jpf.util.script.impl.ScriptElement;
import gov.nasa.jpf.util.script.impl.ScriptElementContainer;
import gov.nasa.jpf.util.script.impl.Section;
import gov.nasa.jpf.util.script.impl.Visitor;

import java.util.ArrayList;
import java.util.Map;

public class AddVisitor implements Visitor {

  /** maps id scriptelement */
  Map<Integer, ScriptElement> idToScriptElementMap;

  /** maps name of section to its current pointer */
  Map<String, ScriptElement> currentSectionStateByName;

  ArrayList<String> sectionNameToIndexMap;

  ArrayList<Repeat> indexToRepeatIDMap = new ArrayList<Repeat>();

  public AddVisitor(Map<Integer, ScriptElement> idToScriptElementMap,
      Map<String, ScriptElement> currentSectionStateByName, ArrayList<String> sectionNameToIndexMap,
      ArrayList<Repeat> indexToRepeatIDMap) {
    this.idToScriptElementMap = idToScriptElementMap;
    this.currentSectionStateByName = currentSectionStateByName;
    this.sectionNameToIndexMap = sectionNameToIndexMap;
    this.indexToRepeatIDMap = indexToRepeatIDMap;
  }

  @Override
  public void visit(ScriptElement element) {
    addElement(element);
  }

  @Override
  public void visit(ScriptElementContainer element) {

    visit((ScriptElement) element);

    ScriptElement pointer = element.getChildren();

    while (pointer != null) {
      pointer.accept(this);
      pointer = pointer.getNextSibling();
    }

  }

  public void addElement(ScriptElement element) {
    idToScriptElementMap.put(element.getId(), element);

    if (element instanceof Any) {
    } else if (element instanceof Repeat) {
      this.indexToRepeatIDMap.add((Repeat) element);
    } else if (element instanceof Event) {
    } else if (element instanceof ResetRepeat) {
    } else if (element instanceof Choice) {
    } else if (element instanceof Script) {
    } else if (element instanceof Section) {
      sectionNameToIndexMap.add(((Section) element).getName());
      currentSectionStateByName.put(((Section) element).getName(), element);
    } else {
      throw new RuntimeException("Unidentifed element " + element.toString());
    }

  }
}