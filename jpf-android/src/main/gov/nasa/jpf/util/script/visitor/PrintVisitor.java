package gov.nasa.jpf.util.script.visitor;

import gov.nasa.jpf.util.script.impl.Any;
import gov.nasa.jpf.util.script.impl.Any.Choice;
import gov.nasa.jpf.util.script.impl.Event;
import gov.nasa.jpf.util.script.impl.Repeat;
import gov.nasa.jpf.util.script.impl.RepeatInfinate;
import gov.nasa.jpf.util.script.impl.ResetRepeat;
import gov.nasa.jpf.util.script.impl.Script;
import gov.nasa.jpf.util.script.impl.ScriptElement;
import gov.nasa.jpf.util.script.impl.ScriptElementContainer;
import gov.nasa.jpf.util.script.impl.Section;
import gov.nasa.jpf.util.script.impl.Visitor;

public class PrintVisitor implements Visitor {

  private int tab = 0;

  @Override
  public void visit(ScriptElement element) {
    System.out.println(identifyElement(element) + " [" + element.id + "]");
  }

  @Override
  public void visit(ScriptElementContainer element) {

    System.out.println(identifyElement(element) + " { [" + element.id + "]");

    // store tabs
    int temp = ++tab;

    ScriptElement pointer = element.getChildren();

    while (pointer != null) {
      printTabs(tab);
      pointer.accept(this);
      pointer = pointer.getNextSibling();
      tab = temp;
    }

    printTabs(temp - 1);
    System.out.println("}");

  }

  public String identifyElement(ScriptElement element) {
    String ret = "";

    if (element instanceof Any) {
      ret = "ANY ";
    } else if (element instanceof RepeatInfinate) {
      ret = "REPEAT * ";
    } else if (element instanceof Repeat) {
      ret = "REPEAT " + ((Repeat) element).getNumber();
    } else if (element instanceof Event) {
      ret = "EVENT " + ((((Event) element).getTarget() == null) ? "device" : ((Event) element).getTarget())
          + "." + ((Event) element).getAction() + "(";
      if (((Event) element).getParams() != null && getLengthOfArgs(((Event) element).getParams()) > 0) {
        for (Object s : ((Event) element).getParams()) {
          if (s != null)
            if (s instanceof String) {
              ret += "\"" + s.toString() + "\", ";
            } else {
              ret += s.toString() + ", ";
            }
        }
        ret = ret.substring(0, ret.length() - 2);
      }
      ret += ")";
    } else if (element instanceof Choice) {
      ret = "CHOICE ";
    } else if (element instanceof Script) {
      ret = "SCRIPT ";
    } else if (element instanceof Section) {
      ret = "SECTION " + ((Section) element).getName();
    } else if (element instanceof ResetRepeat) {
      ret = "RESETREPEAT";
    } else {

      throw new RuntimeException("Unidentifed element " + element.toString());
    }
    return ret;

  }

  public int getLengthOfArgs(Object[] args) {
    int j = 0;
    if (args != null && args.length > 0) {
      do {
        if (args[j] == null) {
          break;
        }
        j++;
      } while (args != null && j < args.length);
    }
    return j;
  }

  public void printTabs(int num) {
    for (int i = 0; i < num; i++) {
      System.out.print("  ");
    }
  }
}