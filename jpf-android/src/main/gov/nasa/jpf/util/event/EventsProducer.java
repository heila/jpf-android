package gov.nasa.jpf.util.event;

import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.tree.EventNode;
import gov.nasa.jpf.vm.MJIEnv;

/**
 * Interface between for EventGenerator to access certain information from EventProducer
 * 
 * @author heila
 *
 */
public interface EventsProducer {

  public String getCurrentWindow(MJIEnv env, int objRef);

  public Event[] getEvents();

  public boolean isFirstEvent(MJIEnv env);

  public EventNode getCurrentEvent();

}
