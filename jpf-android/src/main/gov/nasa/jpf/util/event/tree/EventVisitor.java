package gov.nasa.jpf.util.event.tree;


public interface EventVisitor {

  public void visit(EventNode node);

}
