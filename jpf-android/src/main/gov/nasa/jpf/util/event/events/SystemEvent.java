//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.util.event.events;

import java.util.Arrays;

import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.BatteryManager;
import android.os.Bundle;

/**
 * This models a single system interaction, which maps to a (reflection) call of a method
 * 
 * <h2>Supported SystemEvents</h2>
 * 
 * <h4>External Intent</h4>
 * 
 * <pre>
 * startIntent.setComponent("za.android.vdm.rssreader.TimelineActivity") 
 * startActivity(@startIntent)
 * </pre>
 * 
 * <h4>Connectivity State</h4>
 * <p>
 * See {@link ConnectivityManager} for adding new events for connectivity.
 * </p>
 * 
 * <pre>
 *  device.setWifi("ON")
 * </pre>
 * 
 * <h4>BatteryState</h4>
 * <p>
 * See {@link BatteryManager}
 * </p>
 * 
 * <pre>
 *  device.setBattery("10%")
 * </pre>
 * 
 * <h4>Custom Intent</h4>
 * <p>
 * These Intents can be constructed and broadcast at anytime. To intercept custom events, addd the relevant
 * code in the native AndroidEventProducer class.
 * </p>
 * 
 * <pre>
 * urlInputIntent.putExtraString("url","http://")
 * urlInputIntent.putExtraString("file","src/input.rss") sendBroadcast(@urlInputStreamIntent)
 * </pre>
 * 
 * <h4>Location updates</h4>
 * <p>
 * See {@link LocationManager}
 * </p>
 * 
 * <pre>
 * locationUpdateIntent.putExtraString("latitude","33.9200")
 * urlInputStreamIntent.putExtraString("longitude","18.8600") 
 * sendBroadcast(@urlInputStreamIntent)
 * </pre>
 * 
 * @author Heila van der Merwe <heilamvdm@gmail.com>
 * @date Edited 27 March 2015
 */
public class SystemEvent extends EventImpl {

  protected String target; // the component id (enumeration number)
  protected String action; // the method name

  protected Object[] arguments;

  protected Intent intent;

  public SystemEvent(String target, String action) {
    this(target, action, null);
  }

  public SystemEvent(Intent intent) {
    this.intent = intent;
  }

  public SystemEvent(String target, String action, Object[] arguments) {

    this.target = target;
    this.action = action;
    this.arguments = arguments;
  }

  /**
   * 'spec' has the form $target.action target is either a number or a hierarchical string of ids, e.g.
   * $MyFrame.Ok
   */
  public SystemEvent(String spec, Object[] args) {

    int i = spec.lastIndexOf('.');
    if (i > 0) {
      target = spec.substring(0, i);
      spec = spec.substring(i + 1);
    }

    action = spec;
  }

  public String getTarget() {
    return target;
  }

  public void setTarget(String s) {
    target = s;
  }

  public String getAction() {
    return action;
  }

  public void setAction(String s) {
    action = s;
  }

  public Object[] getArguments() {
    return arguments;
  }

  public Intent getIntent() {
    return intent;
  }

  public void setIntent(Intent intent) {
    this.intent = intent;
  }

  @Override
  public String toString() {
    return "SystemEvent [target="
        + target
        + ", action="
        + action
        + ((arguments != null && arguments.length > 0) ? ", arguments=" + Arrays.toString(arguments) : "")
        + ((intent == null) ? "intent=null" : ", intent=[ action:" + intent.getAction() + ", cat: " + intent.getCategories() + " , component:"
            + intent.getComponent() + ", extra: " + ((intent.getExtras() == null) ? "no extras" : describeBundle(intent.getExtras()))) + " ]]";
  }

  @Override
  public String print() {
    StringBuilder b = new StringBuilder();

    if (target != null) {
      b.append(target);
      b.append('.');
    }
    if (action != null) {
      b.append(action);
      b.append('(');

    }
    b.append(((arguments == null) ? "" : "arguments=" + Arrays.toString(arguments))
        + ((intent == null) ? "intent=null" : (", intent=[ " + "  action:" + intent.getAction() + ", component:" + intent.getComponent() + ", flags:"
            + intent.getFlags() + ((intent.getCategories() == null) ? "" : ", cat: " + intent.getCategories())
            + ((intent.getExtras() == null) ? "" : ", extras: " + describeBundle(intent.getExtras()))
            + ((intent.getData() == null) ? "" : ", data:" + intent.getData().toSafeString())
            + ((intent.getPackage() == null) ? "" : ", package:" + intent.getPackage())
            + ((intent.getSelector() == null) ? "" : ", selector:" + intent.getSelector())
            + ((intent.getType() == null) ? "" : ", type:" + intent.getType()) + " ]")));
    b.append(") ");
    return b.toString();
  }

  public String describeBundle(Bundle b) {
    StringBuilder sb = new StringBuilder();
    if (b != null)
      for (String e : b.keySet()) {
        Object o = b.get(e);
        if (o != null)
          sb.append(" (" + e + ", " + o + ")");
      }
    return sb.toString();
  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    Intent i = (Intent) intent.clone();
    SystemEvent event = new SystemEvent(i);
    event.action = this.action;
    event.target = this.target;
    if (this.arguments != null)
      event.arguments = Arrays.copyOf(this.arguments, this.arguments.length);
    if (getWindowName() != null)
      event.setWindowName(getWindowName());
    return event;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((action == null) ? 0 : action.hashCode());
    result = prime * result + ((arguments == null || arguments.length == 0) ? 0 : Arrays.hashCode(arguments));
    result = prime * result + ((intent == null) ? 0 : intent.filterHashCode());
    result = prime * result + ((intent.getExtras() == null || intent.getExtras().size() == 0) ? 0 : describeBundle(intent.getExtras()).hashCode());
    result = prime * result + ((target == null) ? 0 : target.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof SystemEvent)) {
      return false;
    }
    SystemEvent other = (SystemEvent) obj;
    if (action == null) {
      if (other.action != null) {
        return false;
      }
    } else if (!action.equals(other.action)) {
      return false;
    }
    if (arguments == null) {
      if (other.arguments != null) {
        return false;
      }
    } else if (!Arrays.equals(arguments, other.arguments)) {
      return false;
    }
    if (target == null) {
      if (other.target != null) {
        return false;
      }
    } else if (!target.equals(other.target)) {
      return false;
    }
    if (intent == null) {
      if (other.intent != null) {
        return false;
      }
    } else if (other.intent == null || !intent.filterEquals(other.intent)) {
      return false;

    } else if (intent.getExtras() == null) {
      if (other.intent.getExtras() != null) {
        return false;
      }
    } else if (other.intent.getExtras() == null || !describeBundle(intent.getExtras()).equals(describeBundle(other.intent.getExtras()))) {
      return false;
    }
    return true;
  }

}
