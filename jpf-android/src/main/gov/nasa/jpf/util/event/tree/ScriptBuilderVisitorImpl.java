package gov.nasa.jpf.util.event.tree;

import gov.nasa.jpf.util.event.events.AndroidEvent;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ScriptBuilderVisitorImpl implements EventVisitor {

  Map<String, EventNode> activityTree = new HashMap<String, EventNode>();

  static EventNode currentNode = new EventNode();

  static {
    currentNode.setEvent(AndroidEvent.DEFAULT);
  }

  @Override
  public void visit(EventNode node) {
    EventNode pointer = currentNode;
    pointer.addChild(node.getEvent(), 0);

    Iterator<EventNode> it = node.getChildren();
    while (it.hasNext()) {
      EventNode e = it.next();
      e.accept(this);
    }

  }

  // add node as child of currentNode?
  public void preVisit(EventNode node, EventNode currentNode) {

  }

}
