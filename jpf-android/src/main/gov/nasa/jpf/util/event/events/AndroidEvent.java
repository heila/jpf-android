package gov.nasa.jpf.util.event.events;

/**
 * Default Android Event
 * 
 * @author Heila van der Merwe <heilamvdm@gmail.com>
 * @date
 *
 */
public class AndroidEvent extends EventImpl {
  public static final AndroidEvent DEFAULT = new AndroidEvent();

  public AndroidEvent() {
  }

  @Override
  /**
   * Long printed version of event
   */
  public String print() {
    return toString();
  }

  @Override
  /**
   * Short print version of event
   */
  public String toString() {
    return "AndroidEvent[DEFAULT]";
  }

  @Override
  public int hashCode() {
    return 1;
  }

  @Override
  public boolean equals(Object object) {
    if (object == null) {
      return false;
    } else if (!(object instanceof AndroidEvent)) {
      return false;
    }
    return true;
  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    return new AndroidEvent();
  }
}
