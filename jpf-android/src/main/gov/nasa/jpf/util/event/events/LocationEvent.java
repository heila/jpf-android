package gov.nasa.jpf.util.event.events;

public class LocationEvent extends EventImpl {
  String mListener;

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((mListener == null) ? 0 : mListener.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    LocationEvent other = (LocationEvent) obj;
    if (mListener == null) {
      if (other.mListener != null)
        return false;
    } else if (!mListener.equals(other.mListener))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return mListener + "()";
  }

  public String getListener() {
    return mListener;
  }

  public void setListener(String mListener) {
    this.mListener = mListener;
  }

  @Override
  public String print() {
    return toString();
  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    LocationEvent l = new LocationEvent();
    l.setListener(mListener);
    return l;
  }

}
