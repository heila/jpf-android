package gov.nasa.jpf.util.event.events;

/**
 * Preference change event
 * 
 * @author Heila van der Merwe <heilamvdm@gmail.com>
 * @date
 *
 */
public class SharedPreferenceEvent extends EventImpl {

  String mKey;
  String mListener;
  Event mEvent;

  public Event getEvent() {
    return mEvent;
  }

  public void setEvent(Event mEvent) {
    this.mEvent = mEvent;
  }

  public String getListener() {
    return mListener;
  }

  public void setListener(String mListener) {
    this.mListener = mListener;
  }

  public SharedPreferenceEvent(String key) {
    mKey = key;
  }

  public SharedPreferenceEvent() {
  }

  public String getKey() {
    return mKey;
  }

  public void setKey(String mKey) {
    this.mKey = mKey;
  }

  @Override
  /**
   * Long printed version of event
   */
  public String print() {
    return toString();
  }

  @Override
  /**
   * Short print version of event
   */
  public String toString() {
    return "Preference[" + mKey + "]." + ((getListener() != null) ? mListener : mEvent.toString());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((mEvent == null) ? 0 : mEvent.hashCode());
    result = prime * result + ((mKey == null) ? 0 : mKey.hashCode());
    result = prime * result + ((mListener == null) ? 0 : mListener.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    SharedPreferenceEvent other = (SharedPreferenceEvent) obj;
    if (mEvent == null) {
      if (other.mEvent != null)
        return false;
    } else if (other.mEvent == null || !(mEvent.equals(other.mEvent)))
      return false;
    if (mKey == null) {
      if (other.mKey != null)
        return false;
    } else if (!mKey.equals(other.mKey))
      return false;
    if (mListener == null) {
      if (other.mListener != null)
        return false;
    } else if (other.mListener == null || !(mListener.equals(other.mListener)))
      return false;
    return true;
  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    SharedPreferenceEvent sp = new SharedPreferenceEvent(mKey);
    sp.mListener = this.mListener;
    if (this.mEvent != null)
      sp.mEvent = (Event) ((EventImpl) this.mEvent).clone();
    return sp;
  }
}
