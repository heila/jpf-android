package gov.nasa.jpf.util.event.generator.dynamic;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.excludeId;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;

import java.util.LinkedList;
import java.util.List;

import org.bson.Document;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongoDBWrapper {

  private MongoClient mongoClient;
  private MongoDatabase db;
  private MongoCollection<Document> collection;

  public MongoDBWrapper(String url, String dbname, String collectionName) throws MongoDBException {
    try {
      mongoClient = new MongoClient(url);
      db = mongoClient.getDatabase(dbname);
      collection = db.getCollection(collectionName);
    } catch (Exception e) {
      throw new MongoDBException("Could not connect to mongodb", e.getCause());
    }
  }

  public MongoDBWrapper(String dbname, String collectionName) throws MongoDBException {
    try {
      mongoClient = new MongoClient("mongodb3", 27017);
      db = mongoClient.getDatabase(dbname);
      collection = db.getCollection(collectionName);
    } catch (Exception e) {
      throw new MongoDBException("Could not connect to mongodb", e.getCause());
    }
  }

  /**
   * This method finds a list of return values collected for a specific class and method and input parameters.
   * Case 1: No values have been collected In this case a list of length 0 will be returned (not null)
   * 
   * Case 2: The method does not take params If the method takes no parameters or we want to collect return
   * values for any parameters paramsString must either be null or "". In this case we do not filter using
   * this string.
   * 
   * Case 3: The
   * 
   * @param className
   * @param methodSignature
   * @param paramsString
   * @return
   * @throws MongoDBException
   */
  public List<String> getReturnObject(String className, String methodSignature, String paramsString)
      throws MongoDBException {
    // make very sure correct inputs are given
    if (className == null || className.length() < 1 || methodSignature == null
        || methodSignature.length() < 1) {
      throw new MongoDBException(
          "getReturnObject() requires className and methodSignature to be not null and not empty: classname="
              + className + " methodSignature=" + methodSignature);
    }

    final List<String> returnObjects = new LinkedList<String>();

    try {

      FindIterable<Document> iterable;
      if (paramsString == null || paramsString.length() == 0) {
        // filter using only class & method name
        iterable = collection.find(and(eq("classname", className), eq("method_signature", methodSignature)))
            .projection(fields(include("ret"), excludeId()));
      } else {
        iterable = collection.find(
            and(eq("classname", className), eq("method_signature", methodSignature),
                eq("params", paramsString))).projection(fields(include("ret"), excludeId()));
      }

      iterable.forEach(new Block<Document>() {
        @Override
        public void apply(final Document document) {
          // System.out.println(document);
          returnObjects.add(document.getString("ret"));
        }
      });

    } catch (Exception e) {
      throw new MongoDBException("Error while quering db", e.getCause());
    }

    return returnObjects;

  }

  /**
   * This method finds a list of return values collected for a specific class and method and input parameters.
   * Case 1: No values have been collected In this case a list of length 0 will be returned (not null)
   * 
   * Case 2: The method does not take params If the method takes no parameters or we want to collect return
   * values for any parameters paramsString must either be null or "". In this case we do not filter using
   * this string.
   * 
   * Case 3: The
   * 
   * @param className
   * @param methodSignature
   * @param paramsString
   * @return
   * @throws MongoDBException
   */
  public List<String> getInputParameters(String className, String methodSignature) throws MongoDBException {
    // make very sure correct inputs are given
    if (className == null || className.length() < 1 || methodSignature == null
        || methodSignature.length() < 1) {
      throw new MongoDBException(
          "getInputParameters() requires className and methodSignature to be not null and not empty: classname="
              + className + " methodSignature=" + methodSignature);
    }

    final List<String> returnObjects = new LinkedList<String>();

    try {

      FindIterable<Document> iterable;
      // filter using only class & method name
      iterable = collection.find(and(eq("classname", className), eq("method_signature", methodSignature)))
          .projection(fields(include("params"), excludeId()));

      iterable.forEach(new Block<Document>() {
        @Override
        public void apply(final Document document) {
          // System.out.println(document);
          returnObjects.add(document.getString("params"));
        }
      });

    } catch (Exception e) {
      throw new MongoDBException("Error while quering db", e.getCause());
    }

    return returnObjects;
  }

  public void close() {
    mongoClient.close();

  }

  public static class MongoDBException extends Exception {
    private static final long serialVersionUID = -4816039479069128397L;

    public MongoDBException(String message) {
      super(message);
    }

    public MongoDBException(String message, Throwable cause) {
      super(message, cause);
    }
  }

}
