package gov.nasa.jpf.util.event.generator.dynamic;

import java.io.StringWriter;

import android.view.KeyEvent;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.reflection.ReflectionProvider;
import com.thoughtworks.xstream.io.xml.CompactWriter;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.mapper.Mapper;

public class XStreamConverter {

  public XStream xstream;

  public XStreamConverter() throws XStreamConverterException {

    try {
      xstream = new XStream(new DomDriver()); // does not require XPP3 library
      // Replaces the default Converter for these classes
      // xstream.registerConverter(new CursorConverter());
      Mapper mapper = xstream.getMapper();
      ReflectionProvider reflectionProvider = xstream.getReflectionProvider();
      xstream.registerConverter(new BundleConverter(mapper, reflectionProvider));
      xstream.registerConverter(new ParcelConverter());
      // xstream.registerConverter(new ContextConverter());
      xstream.alias("android.view.KeyEvent", KeyEvent.class);
      // xstream.ignoreUnknownElements();
      xstream.alias("mMap", android.util.ArrayMap.class);
      xstream.ignoreUnknownElements();
    } catch (Exception e) {
      throw new XStreamConverterException("Could not instanciate XStreamConverter", e.getCause());
    }

  }

  /**
   * This method inflates an XML String to the object it represents.
   * 
   * @param xmlobject
   *          should be a valid XStream XML String
   * @return Object
   * @throws XStreamConverterException
   */
  public Object getObject(String xmlobject) throws XStreamConverterException {
    Object obj = null;
    try {
      obj = xstream.fromXML(xmlobject);
    } catch (Exception e) {
      e.printStackTrace();
      throw new XStreamConverterException("Could not convert xml to object", e.getCause());
    }
    return obj;
  }

  public static class XStreamConverterException extends Exception {
    private static final long serialVersionUID = 6741788297741411539L;

    public XStreamConverterException(String message) {
      super(message);
    }

    public XStreamConverterException(String message, Throwable cause) {
      super(message, cause);
    }
  }

  public String getXML(String parameterSignature, Object[] parameters) {
    StringBuilder s = new StringBuilder();
    Class<?>[] parameterTypes = getTypes(parameterSignature, parameters.length);
    for (int i = 0; i < parameters.length; i++) {
      s.append((serialized(parameters[i], parameterTypes[i])).replace('\n', ' '));
      if (i + 1 < parameters.length) {
        s.append(", ");
      }
    }
    return s.toString();
  }

  /**
   * Gets an array of type classes specified in the signature.
   * 
   * @param signature
   *          The parameter signature.
   * @param numparams
   *          The number of parameters.
   * @return An array of types specified in the signature.
   */
  protected Class<?>[] getTypes(String signature, int numparams) {
    Class<?>[] types = new Class<?>[numparams];

    // From the sig, we can infer if these were basic types or not.
    int j = signature.indexOf("(") + 1, k = signature.indexOf(";", j);
    if (k < j) {
      k = signature.indexOf(")", j);
    }

    for (int i = 0; i < numparams; i++) {
      types[i] = getType(signature.substring(j, j + 1));

      // Determines whether to skip by one char, or until the next
      // semicolon.
      if (types[i] == void.class || simpleType(types[i])) {
        j++;
        k++;
      } else {
        j = k;
        k = signature.indexOf(";", k);
        if (k < j) {
          k = signature.indexOf(")", k);
        }
      }
    }
    return types;
  }

  /**
   * Gets the type class specified by the type signature.
   * 
   * @param p
   *          The type signature.
   * @return the type class specified by the type signature.
   */
  protected Class<?> getType(String p) {
    if (p.equals("B")) {
      return byte.class;
    } else if (p.equals("C")) {
      return char.class;
    } else if (p.equals("D")) {
      return double.class;
    } else if (p.equals("F")) {
      return float.class;
    } else if (p.equals("I")) {
      return int.class;
    } else if (p.equals("J")) {
      return long.class;
    } else if (p.equals("S")) {
      return short.class;
    } else if (p.equals("V")) {
      return void.class;
    } else if (p.equals("Z")) {
      return boolean.class;
    } else {
      return Object.class;
    }
  }

  /**
   * Serializes the object or simple Java type.
   * 
   * @param obj
   *          The data to serialize.
   * @param type
   *          The type of data.
   * @return The serialized data.
   */
  protected String serialized(Object obj, Class<?> type) {
    if (type == void.class) {
      return "void";
    } else if (simpleType(type)) {
      return obj.toString();
    } else {
      return serialize(obj);
    }
  }

  /**
   * Returns true iff <code>type</code> is a simple Java type (int, bool, etc., but not void or Object).
   * 
   * @param type
   *          Type of data.
   * @return true iff <code>type</code> is a simple Java type.
   */
  protected boolean simpleType(Class<?> type) {
    return type == int.class || type == byte.class || type == short.class || type == long.class
        || type == boolean.class || type == float.class || type == double.class;
  }

  /**
   * Serializes the given object.
   * 
   * @param obj
   *          Object to serialize.
   * @return The serialized object.
   */
  public String serialize(Object obj) {
    StringWriter sw = new StringWriter();
    xstream.marshal(obj, new CompactWriter(sw));
    return xstream.toXML(obj);
  }

}
