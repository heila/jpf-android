//
// Copyright (C) 2013 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.util.event.tree;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.util.event.events.Event;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * an EventTree with UIAction events and specialized constructors
 */
public class AndroidEventTree extends EventTree {

  private int maxDepth = 7;

  public AndroidEventTree(Config config) {
    this(config, null);
  }

  protected AndroidEventTree(Config config, Event root) {
    maxDepth = config.getInt("event.max_depth", maxDepth);
  }

  public int getMaxDepth() {
    return maxDepth;
  }

  public boolean reachedMaxDepth(int depth) {
    return depth == maxDepth;
  }

  /**
   * Determines the depth of this current element from the last script event
   * 
   * @return
   */
  public int getDepthOfGeneratedEvents(Event pointer) {
    int counter = 0;
    // while (pointer != null && (((AndroidEvent)
    // pointer).getGeneratedBy().equals(InputStrategy.DEFAULT))) {
    // pointer = pointer.getPrev();
    // counter++;
    // }
    return counter;
  }

  @Override
  public EventNode addEvent(EventNode current, Event e, int stateID) {
    if (!reachedMaxDepth(getDepth(current))) {
      return super.addEvent(current, e, stateID);
    } else {
      Logger l = Logger.getLogger(this.getClass().getSimpleName());
      l.log(Level.SEVERE, "Reached max depth");
      return null;
    }
  }

  public void remove(EventNode current, EventNode parent) {
    if (parent != null) {
      parent.removeChild(current);
    }
  }

}
