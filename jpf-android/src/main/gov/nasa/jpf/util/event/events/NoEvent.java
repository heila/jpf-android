package gov.nasa.jpf.util.event.events;

public class NoEvent extends EventImpl {
  String id = "";

  public NoEvent() {
  }

  public NoEvent(String id) {
    this.id = id;
  }

  @Override
  public String toString() {
    if (id != null && id.length() > 0)
      return "NoEvent [" + id + "]";
    else
      return "NoEvent []";
  }

  @Override
  public String print() {
    return this.toString();
  }

  @Override
  public int hashCode() {
    return 0;
  }

  @Override
  public boolean equals(Object object) {
    if (object != null && object instanceof NoEvent)
      return true;
    else
      return false;

  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    return new NoEvent(id);
  }

}
