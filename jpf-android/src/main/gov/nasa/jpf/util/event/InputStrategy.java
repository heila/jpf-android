package gov.nasa.jpf.util.event;

public enum InputStrategy {
  EVENTTREE("event_tree"), SCRIPT("script"), DEFAULT("default"), SCRIPT_GENERATE("script_generate"), DYNAMIC(
      "dynamic"), HEURISTIC("heuristic"), HEURISTIC2("heuristic2");
  String name;

  InputStrategy(String name) {
    this.name = name;
  }

  public static InputStrategy getInputStrategy(String name) {
    InputStrategy strategy = null;

    if (name.equals(InputStrategy.SCRIPT.toString())) {
      strategy = InputStrategy.SCRIPT;

    } else if (name.equals(InputStrategy.SCRIPT_GENERATE.toString())) {
      strategy = InputStrategy.SCRIPT_GENERATE;

    } else if (name.equals(InputStrategy.EVENTTREE.toString())) {
      strategy = InputStrategy.EVENTTREE;

    } else if (name.equals(InputStrategy.DYNAMIC.toString())) {
      strategy = InputStrategy.DYNAMIC;

    } else if (name.equals(InputStrategy.HEURISTIC.toString())) {
      strategy = InputStrategy.HEURISTIC;
    } else if (name.equals(InputStrategy.HEURISTIC2.toString())) {
      strategy = InputStrategy.HEURISTIC2;
    } else {
      strategy = InputStrategy.DEFAULT;

    }
    return strategy;
  }

  @Override
  public String toString() {
    return this.name;
  }
}