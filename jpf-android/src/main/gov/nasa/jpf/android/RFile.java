package gov.nasa.jpf.android;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.util.JPFLogger;

import java.util.HashMap;
import java.util.Map.Entry;

/**
 * This class contains the mapping of int ids to names of resources defined in the R.java file. In Android the
 * R.java file is generated from the list of resources in the "res" directory. Each resource is mapped to a
 * unique int value.
 * 
 * In Android these values are used to reference resources from the Java code. These values are looked up by
 * using the Resources.java that queries the AssetManager for the actual value of the resource (using native
 * code).
 * 
 * 
 * For JPF-Android we can not use the AssetManager since it is mainly implemented in the native code. We use
 * the RFile object stored in the AndroidProjectInfo object to map the value in the code (for example
 * 0x70123455 or R.id.title) to the name and type of the resource (type: id name: title) This allows up to go
 * and lookup the value in the values.xml file.
 * 
 * TODO need to check that hashmaps are distributed evenly enough
 * 
 * @author Heila <heilamvd@gmail.com>
 *
 */
public class RFile {

  static JPFLogger log = JPF.getLogger("gov.nasa.jpf.android.RParser");

  /**
   * Maps the resource ID of a layout to the name of the layout file. This information is read from the R.java
   * class and stored as a map fir quick lookup by the LayoutInflater.
   */
  HashMap<Integer, String> layoutIdToNameMap = new HashMap<Integer, String>();

  HashMap<Integer, String> stringIdToNameMap = new HashMap<Integer, String>();
  HashMap<Integer, String> arrayIdToNameMap = new HashMap<Integer, String>();
  HashMap<Integer, String> boolIdToNameMap = new HashMap<Integer, String>();
  HashMap<Integer, String> intIdToNameMap = new HashMap<Integer, String>();

  // used in main.xml to name views
  HashMap<Integer, String> viewIdToNameMap = new HashMap<Integer, String>();
  HashMap<String, Integer> viewNameToIdMap = new HashMap<String, Integer>();
  HashMap<Integer, String> xmlIdToNameMap = new HashMap<Integer, String>();
  HashMap<Integer, String> menuIdToNameMap = new HashMap<Integer, String>();

  // general map that includes all resources
  // HashMap<Integer, String> IdToNameMap = new HashMap<Integer, String>();

  public String getLayoutNameForId(Integer layoutID) {
    String layoutFilename = layoutIdToNameMap.get(layoutID);
    if (layoutFilename == null) {
      log.warning("RFile: No layout with id " + layoutID);
    }
    return layoutFilename;
  }

  public int getLayoutIdForName(String resourceName) {
    for (Entry<Integer, String> entry : layoutIdToNameMap.entrySet()) {
      String name = entry.getValue();
      if (name.equals(resourceName))
        return entry.getKey();
    }
    log.warning("RFile: No layout with name " + resourceName);
    return -1;
  }

  public String getViewNameForId(int viewID) {
    String viewFilename = viewIdToNameMap.get(viewID);
    if (viewFilename == null) {
      log.warning("RFile: No view with id " + viewID);
    }
    return viewFilename;
  }

  public String getViewNameForId(String viewID) {
    String viewFilename = viewIdToNameMap.get(Integer.parseUnsignedInt(viewID.trim().substring(2), 16));
    if (viewFilename == null) {
      log.warning("RFile: No view with id " + viewID);
    }
    return viewFilename;
  }

  public String getStringNameForId(int stringID) {
    String stringName = stringIdToNameMap.get(stringID);
    if (stringName == null) {
      log.warning("RFile: No string with id " + stringID);
    }
    return stringName;
  }

  public String getBoolNameForId(int stringID) {
    String stringName = boolIdToNameMap.get(stringID);
    if (stringName == null) {
      log.warning("RFile: No bool with id " + stringID);
    }
    return stringName;
  }

  public String getArrayNameForId(int stringID) {
    String stringName = arrayIdToNameMap.get(stringID);
    if (stringName == null) {
      log.warning("RFile: No array with id " + stringID);
    }
    return stringName;
  }

  public String getXmlNameForId(int stringID) {
    String stringName = xmlIdToNameMap.get(stringID);
    if (stringName == null) {
      log.warning("RFile: No xml with id " + stringID);
    }
    return stringName;
  }

  public String getMenuNameForId(int stringID) {
    String stringName = menuIdToNameMap.get(stringID);
    if (stringName == null) {
      log.warning("RFile: No menu with id " + stringID);
    }
    return stringName;
  }

  public Integer getViewIdForName(String name) {
    Integer id = viewNameToIdMap.get(name);
    if (id == null) {
      log.warning("RFile: No view with name " + name);
    }
    return id;
  }

  // public String getResourceName(int resourceID) {
  // String name = IdToNameMap.get(resourceID);
  // if (name == null) {
  // log.warning("RFile: No resource found with id " + resourceID);
  // }
  // return name;
  // }

}
