/*
 * Copyright (C) 2014, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 *
 * The Java Pathfinder core (jpf-core) platform is licensed under the
 * Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0. 
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package gov.nasa.jpf.android;

import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.serialize.FieldAmmendmentByName;

public class IgnoreEmmaReflectiveNames extends FieldAmmendmentByName {
  /**
   * These are not critical to state because the objects that contain them also have int values that can be
   * used to look up the name within the same VM execution.
   */
  static final String[] reflectiveNameFields = { "$VRc", "TOP", "$assertionsDisabled" };

  public IgnoreEmmaReflectiveNames() {
    super(reflectiveNameFields, POLICY_IGNORE);
  }

  @Override
  public boolean ammendFieldInclusion(FieldInfo fi, boolean sofar) {
    if (fullFieldNames.contains(fi.getName())) {
      return policy;
    } else {
      return sofar;
    }
  }

  // must be at bottom!
  public static final IgnoreEmmaReflectiveNames instance = new IgnoreEmmaReflectiveNames();

}
