/** 
 * Simple stub for the Abstraction class. 
 * Used for compilation of examples only.
 *
 */
package gov.nasa.jpf.vm;

import gov.nasa.jpf.annotation.NeverBreak;

public class Abstraction {
  @NeverBreak
  public static int TOP_INT = 1;
  @NeverBreak
  public static boolean TOP_BOOL = false;
  @NeverBreak
  public static long TOP_LONG = 0;
  @NeverBreak
  public static short TOP_SHORT = 0;
  @NeverBreak
  public static double TOP_DOUBLE = 0;
  @NeverBreak
  public static char TOP_CHAR = 'a';
  @NeverBreak
  public static byte TOP_BYTE = 0;
  @NeverBreak
  public static String TOP_STRING = "TOP";
  @NeverBreak
  public static float TOP_FLOAT = 0;
  @NeverBreak
  public static Object TOP_OBJ = new Object();

  public static Object getTopObject(String type) {
    return null;
  }

  public static int getTopInt() {
    return 0;
  }

  public static boolean getTopBool() {
    return false;
  }

  public static long getTopLong() {
    return 0;
  }

  public static short getTopShort() {
    return 0;
  }

  public static double getTopDouble() {
    return 0;
  }

  public static float getTopFloat() {
    return 0;
  }

  public static String getTopString() {
    return "top";
  }

  public static String getChoiceString() {
    if (Verify.randomBool())
      return "abc";
    else
      return "";
  }

  public static String getSymbolicString() {
    if (Verify.randomBool())
      return "symbolic1";
    else
      return "symbolic2";
  }

  public static boolean getChoiceBool() {
    return Verify.randomBool();

  }

  public static Object randomObject(String string) {
    return null;
  }

}
