package gov.nasa.jpf.vm;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.annotation.NeverBreak;

import java.util.HashMap;
import java.util.Map;

public class AndroidVerify {

  @FilterField
  @NeverBreak
  private static final Map<String, Boolean> choices = new HashMap<String, Boolean>();
  @FilterField
  @NeverBreak
  private static final Map<String, Integer> choices_int = new HashMap<String, Integer>();
  @FilterField
  @NeverBreak
  private static final Map<String, String> choices_str = new HashMap<String, String>();
  @FilterField
  @NeverBreak
  private static final Map<String, Object> choices_obj = new HashMap<String, Object>();

  /**
   * this is the new boolean choice generator. Since there's no real heuristic involved with boolean values,
   * we skip the id (it's a hardwired "boolean")
   * 
   */
  public static boolean getBoolean(String uniqueID) {
    if (!getConfigString("verify.choice", "all").equals("all")) {
      return getBooleanNative(uniqueID);
    }

    synchronized (choices) {
      if (!choices.containsKey(uniqueID)) {
        choices.put(uniqueID, getBooleanNative(uniqueID));
      }
      return choices.get(uniqueID);
    }
  }

  public static native boolean getBooleanNative(String uniqueID);

  /**
   * Returns int nondeterministically between (including) min and (including) max.
   */
  public static int getInt(int min, int max, String uniqueID) {
    if (!getConfigString("verify.choice", "all").equals("all")) {
      return getIntNative(min, max, uniqueID);
    }

    synchronized (choices_int) {
      if (!choices_int.containsKey(uniqueID)) {
        choices_int.put(uniqueID, getIntNative(min, max, uniqueID));
      }
      return choices_int.get(uniqueID);
    }
  }

  public static native int getIntNative(int min, int max, String uniqueID);

  public static String getString(String[] values, String uniqueID) {
    if (!getConfigString("verify.choice", "all").equals("all")) {
      return getStringNative(values, uniqueID);
    }
    synchronized (choices_str) {
      if (!choices_str.containsKey(uniqueID)) {
        choices_str.put(uniqueID, getStringNative(values, uniqueID));
      }
      return choices_str.get(uniqueID);
    }
  }

  public static Object getValues(Object[] values, String uniqueID) {
    if (!getConfigString("verify.choice", "all").equals("all")) {
      return getValuesNative(values, uniqueID);
    }

    synchronized (choices_obj) {
      if (!choices_obj.containsKey(uniqueID)) {
        choices_obj.put(uniqueID, getValuesNative(values, uniqueID));
      }
      return choices_obj.get(uniqueID);
    }
  }

  public static native Object getValuesNative(Object[] values, String uniqueID);

  public static native String getStringNative(String[] values, String uniqueID);

  public static native String getConfigString(String string, String def);

  public static native boolean getConfigBoolean(String string, boolean def);

  public static native String getTextInput(String string);

}
