package gov.nasa.jpf.vm;

import gov.nasa.jpf.Config;

public class AndroidPathSharednessPolicy extends PathSharednessPolicy {

  boolean race = false;

  public AndroidPathSharednessPolicy(Config config) {
    super(config);
    race = config.getBoolean("jpf-android.detectRace");
  }

  /**
   * not transitive
   * 
   * these are the public interfaces towards FieldInstructions. Callers have to be aware this will change the
   * /referenced/ ElementInfo in case the respective object becomes exposed
   */
  @Override
  public boolean setsSharedObjectCG(ThreadInfo ti, Instruction insn, ElementInfo eiFieldOwner, FieldInfo fi) {
    if (race) {
      return super.setsSharedObjectCG(ti, insn, eiFieldOwner, fi);
    } else {
      return false;
    }
  }

  @Override
  public boolean setsSharedClassCG(ThreadInfo ti, Instruction insn, ElementInfo eiFieldOwner, FieldInfo fi) {
    return false;
  }

  @Override
  public boolean setsSharedArrayCG(ThreadInfo ti, Instruction insn, ElementInfo eiArray, int index) {
    return false;
  }
}