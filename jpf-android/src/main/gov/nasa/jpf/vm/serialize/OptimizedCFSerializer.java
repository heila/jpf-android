/*
 * Copyright (C) 2014, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 *
 * The Java Pathfinder core (jpf-core) platform is licensed under the
 * Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0. 
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package gov.nasa.jpf.vm.serialize;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.util.FinalBitSet;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.StaticElementInfo;
import gov.nasa.jpf.vm.Statics;
import gov.nasa.jpf.vm.VM;

/**
 * a CFSerializer that stores the serialized program state in a readable/diffable format.
 * 
 * Automatically used by Debug..StateSet if the configured vm.storage.class is .vm.DebugJenkinsStateSet
 */
public class OptimizedCFSerializer extends CFSerializer {

  ClassLoadingListener listener;

  @Override
  public void attach(VM vm) {
    super.attach(vm);
    listener = new ClassLoadingListener();
    JPF jpf = vm.getJPF();
    jpf.addListener(listener);
  }

  @Override
  protected void serializeStatics(Statics statics) {
    for (StaticElementInfo sei : statics.liveStatics()) {
      ClassInfo ci = sei.getClassInfo();
      if (ci.getAttr() != null && ci.getAttr(String.class) != null
          && ci.getAttr(String.class).equals("filter")) {
      } else {
        serializeClass(sei);
      }
    }
  }
  
  @Override
  protected void serializeFrame(StackFrame frame) {
    buf.add(frame.getMethodInfo().getGlobalId());

    Instruction pc = frame.getPC();
    if (getFramePolicy(frame.getMethodInfo()).includePC)
      buf.add(pc != null ? pc.getInstructionIndex() : -1);

    if (getFramePolicy(frame.getMethodInfo()).includeLocals) {
      int len = frame.getTopPos() + 1;
      buf.add(len);

      // unfortunately we can't do this as a block operation because that
      // would use concrete reference values as hash data, i.e. break heap symmetry
      int[] slots = frame.getSlots();
      for (int i = 0; i < len; i++) {
        if (frame.isReferenceSlot(i)) {
          processReference(slots[i]);
        } else {
          buf.add(slots[i]);
        }
      }
    } else {
      // System.out.println("method excluding locals");
    }
  }
  @Override
  protected FinalBitSet getStaticFilterMask(ClassInfo ci) {
    return super.getStaticFilterMask(ci);
  }
  
  

  private class ClassLoadingListener extends gov.nasa.jpf.ListenerAdapter {

    @Override
    public void classLoaded(VM vm, ClassInfo loadedClass) {

      FieldInfo[] sFields = loadedClass.getDeclaredStaticFields();

      // has no static fields
      if (sFields == null || sFields.length == 0) {
        markClass(loadedClass);
      } else if (filter(loadedClass)) {
        // fields has been filtered or has constant value that will not change
        markClass(loadedClass);
      } else {
        // TODO? filter all static final fields
      }

    }

    public void markClass(ClassInfo ci) {
      ci.addAttr("filter");
    }

    public boolean filter(ClassInfo ci) {
      FinalBitSet filterMask = getStaticFilterMask(ci);
      int max = ci.getNumberOfStaticFields();
      for (int i = 0; i < max; i++) {
        if (filterMask != null && filterMask.get(i)) {
          continue;
        } else if (ci.getStaticField(i).getConstantValue() != null && ci.getStaticField(i).isFinal()) {
          continue;
        } else {
          return false;
        }
      }
      return true;
    }
  }
}
