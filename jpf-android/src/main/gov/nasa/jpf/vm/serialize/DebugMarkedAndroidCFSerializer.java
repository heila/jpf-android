package gov.nasa.jpf.vm.serialize;

import gov.nasa.jpf.util.FinalBitSet;
import gov.nasa.jpf.vm.DebugStateSerializer;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.JPFOutputStream;
import gov.nasa.jpf.vm.NativeStateHolder;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.StaticElementInfo;
import gov.nasa.jpf.vm.ThreadInfo;

import java.io.OutputStream;

public class DebugMarkedAndroidCFSerializer extends AndroidCFSerializer implements DebugStateSerializer {

  JPFOutputStream os;

  // this is for debugging purposes only
  public DebugMarkedAndroidCFSerializer() {
    os = new JPFOutputStream(System.out);
  }

  @Override
  public void setOutputStream(OutputStream s) {
    os = new JPFOutputStream(s);
  }

  @Override
  protected void processReferenceQueue() {
    os.println();
    os.printCommentLine("--- heap");
    os.println();
    super.processReferenceQueue();
  }

  @Override
  public void process(ElementInfo ei) {
    super.process(ei);
    FinalBitSet filtered = !ei.isArray() ? getInstanceFilterMask(ei.getClassInfo()) : null;
    os.print(ei, filtered);
    os.println();
  }

  @Override
  protected void serializeClassLoaders() {
    os.println();
    os.printCommentLine("--- classes");
    os.println();
    super.serializeClassLoaders();
  }

  @Override
  protected void serializeClass(StaticElementInfo sei) {
    super.serializeClass(sei);
    FinalBitSet filtered = getStaticFilterMask(sei.getClassInfo());
    os.print(sei, filtered);
    os.println();
  }

  @Override
  protected void serializeStackFrames() {
    os.println();
    os.printCommentLine("--- threads");
    os.println();
    super.serializeStackFrames();
  }

  @Override
  protected void serializeStackFrames(ThreadInfo ti) {
    os.println();
    os.print(ti);
    os.println();
    super.serializeStackFrames(ti);
  }

  @Override
  protected void serializeFrame(StackFrame frame) {
    os.print(frame);
    os.println();
    super.serializeFrame(frame);
  }

  @Override
  protected void serializeNativeStateHolders() {
    os.println();
    os.printCommentLine("--- native state holders");
    os.println();
    super.serializeNativeStateHolders();
  }

  @Override
  protected void serializeNativeStateHolder(NativeStateHolder nsh) {
    os.print(nsh);
    os.println();
    super.serializeNativeStateHolder(nsh);
  }

}
