/*
 * Copyright (C) 2014, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 *
 * The Java Pathfinder core (jpf-core) platform is licensed under the
 * Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0. 
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package gov.nasa.jpf.vm.serialize;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.Heap;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.StaticElementInfo;
import gov.nasa.jpf.vm.Statics;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.ThreadList;
import gov.nasa.jpf.vm.VM;

import java.util.Iterator;

/**
 * a CFSerializer that stores the serialized program state in a readable/diffable format.
 * 
 * Automatically used by Debug..StateSet if the configured vm.storage.class is .vm.DebugJenkinsStateSet
 */
public class AndroidCFSerializer extends CFSerializer {

  // this is for debugging purposes only
  public AndroidCFSerializer() {
  }


  @Override
  protected int[] computeStoringData() {

    buf.clear();
    heap = ks.getHeap();
    initReferenceQueue();

    // --- serialize all live objects and loaded classes
    serializeStackFrames(); // no references
    serializeClassLoaders();// only application classes
    processApplicationComponents();
    processReferenceQueue(); // only activity classes

    // --- now serialize the thread states (which might refer to live objects)
    // we do this last because threads contain some internal references
    // (locked objects etc) that should NOT set the canonical reference serialization
    // values (if they are encountered before their first explicit heap reference)
    serializeThreadStates();

    // --- last is serialization of native state holders
    // serializeNativeStateHolders();

    return buf.toArray();
  }

  @Override
  protected void serializeStatics(Statics statics) {

    for (StaticElementInfo sei : statics.liveStatics()) {
      // if (sei.getClassInfo().isInstanceOf("android.app.Activity")
      // || sei.getClassInfo().isInstanceOf("android.app.Service")
      // || sei.getClassInfo().isInstanceOf("android.content.BroadcastReceiver")
      // || sei.getClassInfo().isInstanceOf("android.app.ActivityManager")
      // || sei.getClassInfo().isInstanceOf("android.app.ActivityThread")
      // || sei.getClassInfo().isInstanceOf("android.app.Application")
      // || sei.getClassInfo().isInstanceOf("android.content.ContentProvider")) {
      // serializeClass(sei);
      if (isStartUpClass(sei.getClassInfo().getName())) {
        serializeClass(sei);
        // } else if (!isSystemClass(sei.getClassInfo())) {
        // serializeClass(sei);
      }
    }
  }

  Config c = VM.getVM().getConfig();
  String[] extraStartupClasses = c.getStringArray("vm.extra_startup_classes");

  public boolean isStartUpClass(String classname) {
    for (String s : extraStartupClasses) {
      if (s.trim().equals(classname)) {
        return true;
      }
    }
    return false;
  }

  public boolean isSystemClass(ClassInfo ci) {
    String name = ci.getName();
    return name.startsWith("android.") || name.startsWith("com.android.") || name.startsWith("java.")
        || name.startsWith("models") || name.startsWith("gov.nasa") || name.startsWith("sun.")
        || name.contains("com.vladium") || ci.isBoxClass() || ci.isPrimitive() || (ci.isArray());
  }

  @Override
  protected void serializeStackFrames() {
    ThreadList tl = ks.getThreadList();

    for (Iterator<ThreadInfo> it = tl.canonicalLiveIterator(); it.hasNext();) {
      serializeStackFrames(it.next());
    }
  }

  @Override
  protected void serializeFrame(StackFrame frame) {
    buf.add(frame.getMethodInfo().getGlobalId());

    Instruction pc = frame.getPC();
    buf.add(pc != null ? pc.getInstructionIndex() : -1);

//    int len = frame.getTopPos() + 1;
//    buf.add(len);

    // unfortunately we can't do this as a block operation because that
    // would use concrete reference values as hash data, i.e. break heap symmetry
    // int[] slots = frame.getSlots();
    // for (int i = 0; i < len; i++) {
    // if (frame.isReferenceSlot(i)) {
    // processReference(slots[i]);
    // } else {
    // buf.add(slots[i]);
    // }
    // }
  }

  public void processApplicationComponents() {

    Heap heap = VM.getVM().getHeap();

    String type = "android.app.ActivityThread";

    for (ElementInfo ei : heap.liveObjects()) {
      ClassInfo ci = ei.getClassInfo();
      if (ci.isInstanceOf(type)) {
        queueReference(ei);
      }
    }
  }

}
