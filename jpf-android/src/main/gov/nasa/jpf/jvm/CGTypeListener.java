package gov.nasa.jpf.jvm;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.report.Publisher;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.VM;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Collects the number of CG per type
 * 
 * @author Heila van der Merwe
 * 
 */
public class CGTypeListener extends ListenerAdapter {

  private static class CGWrapper {
    int count = 0;
    int countChoices = 0;
    String name;
    int[] numChoices = new int[50];

    @Override
    public String toString() {
      return "name=" + name + ":" + count + " (" + countChoices + ") choices:\t" + Arrays.toString(numChoices);
    }

  }

  HashMap<String, CGWrapper> statistics = new HashMap<String, CGWrapper>();

  /** events directory where to put results in */
  String outputDir;

  public CGTypeListener(Config config) {
    String output = config.getString("jpf-android.output.run", ".");
    try {
      // Assume file does not exist
      Path p = Files.createDirectories(Paths.get(output + File.separator + "cgs"));
      outputDir = p.toFile().getAbsolutePath();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void choiceGeneratorSet(VM vm, ChoiceGenerator<?> newCG) {
    ChoiceGenerator<?> cg = VM.getVM().getChoiceGenerator();

    String id = cg.getClass().getName();
    CGWrapper w = null;
    if (statistics.containsKey(id)) {
      w = statistics.get(id);
      w.count++;
    } else {
      w = new CGWrapper();
      w.count = 1;
      w.name = id;
      statistics.put(id, w);
    }
    int choices = cg.getTotalNumberOfChoices();
    if (choices > 0 && choices < 50) {
      w.numChoices[choices - 1]++;
      w.countChoices += choices;
    }
  }

  @Override
  public void searchFinished(Search search) {
    printCGs();
    super.searchFinished(search);
  }

  @Override
  public void searchConstraintHit(Search search) {
    printCGs();
    super.searchConstraintHit(search);
  }

  @Override
  public void publishConstraintHit(Publisher publisher) {
    printCGs();
    super.publishConstraintHit(publisher);
  }

  public void printCGs() {
    PrintWriter w;
    try {
      w = new PrintWriter(Files.newBufferedWriter(Paths.get(outputDir + File.separator + "CGTypes.log")));
      w.write("****************** ChoiceGenerators *****************\n");
      for (CGWrapper cgw : statistics.values()) {
        w.write(cgw.toString() + "\n");
      }
      w.write("****************** ChoiceGenerators *****************");
      w.flush();
      w.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
