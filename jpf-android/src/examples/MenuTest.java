import java.io.IOException;

public class MenuTest {

  public static void main(String[] args) throws IOException {
    int SEARCH_DEPTH = 1000;
    int EVENT_DEPTH = 20;
    String EVENT_STRATEGY = "default";

    String[] config_properties = { "-show", "src/examples/TestMenu.jpf",
        "+search.depth_limit=" + SEARCH_DEPTH, "+event.max_depth=" + EVENT_DEPTH,
        "+event.strategy=" + EVENT_STRATEGY };

    RunJPFAndroid run = new RunJPFAndroid("menu_example", config_properties);
    run.runJPF();
  }

}
