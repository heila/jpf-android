package java.net;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

public final class URL implements Serializable {

  String spec;

  public URL(String spec) throws MalformedURLException {
    // to be intercepted
  }

  public InputStream openStream() throws IOException {
    // boolean b = AndroidVerify.getBoolean("URL.openStream() - success?");
    //
    // if (!b) {
    // throw new IOException("Could not open Url");
    // } else {
    return new ByteArrayInputStream(getURLInput());
    // }

  }

  protected native byte[] getURLInput();

  public String getProtocol() {
    return "http";

  }
}
