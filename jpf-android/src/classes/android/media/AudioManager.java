package android.media;

import android.content.ComponentName;

public class AudioManager {

  /**
   * A failed focus change request.
   */
  public static final int AUDIOFOCUS_REQUEST_FAILED = 0;
  /**
   * A successful focus change request.
   */
  public static final int AUDIOFOCUS_REQUEST_GRANTED = 1;

  /**
   * @hide Used to indicate no audio focus has been gained or lost.
   */
  public static final int AUDIOFOCUS_NONE = 0;

  /**
   * Used to indicate a gain of audio focus, or a request of audio focus, of unknown duration.
   * 
   * @see OnAudioFocusChangeListener#onAudioFocusChange(int)
   * @see #requestAudioFocus(OnAudioFocusChangeListener, int, int)
   */
  public static final int AUDIOFOCUS_GAIN = 1;
  /**
   * Used to indicate a temporary gain or request of audio focus, anticipated to last a short amount of time.
   * Examples of temporary changes are the playback of driving directions, or an event notification.
   * 
   * @see OnAudioFocusChangeListener#onAudioFocusChange(int)
   * @see #requestAudioFocus(OnAudioFocusChangeListener, int, int)
   */
  public static final int AUDIOFOCUS_GAIN_TRANSIENT = 2;
  /**
   * Used to indicate a temporary request of audio focus, anticipated to last a short amount of time, and
   * where it is acceptable for other audio applications to keep playing after having lowered their output
   * level (also referred to as "ducking"). Examples of temporary changes are the playback of driving
   * directions where playback of music in the background is acceptable.
   * 
   * @see OnAudioFocusChangeListener#onAudioFocusChange(int)
   * @see #requestAudioFocus(OnAudioFocusChangeListener, int, int)
   */
  public static final int AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK = 3;
  /**
   * Used to indicate a temporary request of audio focus, anticipated to last a short amount of time, during
   * which no other applications, or system components, should play anything. Examples of exclusive and
   * transient audio focus requests are voice memo recording and speech recognition, during which the system
   * shouldn't play any notifications, and media playback should have paused.
   * 
   * @see #requestAudioFocus(OnAudioFocusChangeListener, int, int)
   */
  public static final int AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE = 4;
  /**
   * Used to indicate a loss of audio focus of unknown duration.
   * 
   * @see OnAudioFocusChangeListener#onAudioFocusChange(int)
   */
  public static final int AUDIOFOCUS_LOSS = -1 * AUDIOFOCUS_GAIN;
  /**
   * Used to indicate a transient loss of audio focus.
   * 
   * @see OnAudioFocusChangeListener#onAudioFocusChange(int)
   */
  public static final int AUDIOFOCUS_LOSS_TRANSIENT = -1 * AUDIOFOCUS_GAIN_TRANSIENT;
  /**
   * Used to indicate a transient loss of audio focus where the loser of the audio focus can lower its output
   * volume if it wants to continue playing (also referred to as "ducking"), as the new focus owner doesn't
   * require others to be silent.
   * 
   * @see OnAudioFocusChangeListener#onAudioFocusChange(int)
   */
  public static final int AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK = -1 * AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK;

  public interface OnAudioFocusChangeListener {

    public void onAudioFocusChange(int focusChange);
  }

  public static final int STREAM_MUSIC = 0;

  public AudioManager() {
  }

  public void registerMediaButtonEventReceiver(ComponentName eventReceiver) {
    // if (eventReceiver == null) {
    // return;
    // }
    // if (!eventReceiver.getPackageName().equals(mContext.getPackageName())) {
    // Log.e(TAG, "registerMediaButtonEventReceiver() error: " +
    // "receiver and context package names don't match");
    // return;
    // }
    // // construct a PendingIntent for the media button and register it
    // Intent mediaButtonIntent = new Intent(Intent.ACTION_MEDIA_BUTTON);
    // // the associated intent will be handled by the component being registered
    // mediaButtonIntent.setComponent(eventReceiver);

  }

  /**
   * Unregister the receiver of MEDIA_BUTTON intents.
   * 
   * @param eventReceiver
   *          identifier of a {@link android.content.BroadcastReceiver} that was registered with
   *          {@link #registerMediaButtonEventReceiver(ComponentName)}.
   */
  public void unregisterMediaButtonEventReceiver(ComponentName eventReceiver) {
    // if (eventReceiver == null) {
    // return;
    // }
    // // construct a PendingIntent for the media button and unregister it
    // Intent mediaButtonIntent = new Intent(Intent.ACTION_MEDIA_BUTTON);
    // // the associated intent will be handled by the component being registered
    // mediaButtonIntent.setComponent(eventReceiver);
    // PendingIntent pi = PendingIntent.getBroadcast(mContext,
    // 0/*requestCode, ignored*/, mediaButtonIntent, 0/*flags*/);
    // unregisterMediaButtonIntent(pi);
  }

  /**
   * Registers the remote control client for providing information to display on the remote controls.
   * 
   * @param rcClient
   *          The remote control client from which remote controls will receive information to display.
   * @see RemoteControlClient
   */
  public void registerRemoteControlClient(RemoteControlClient rcClient) {
    // if ((rcClient == null) || (rcClient.getRcMediaIntent() == null)) {
    // return;
    // }
    // IAudioService service = getService();
    // try {
    // int rcseId = service.registerRemoteControlClient(
    // rcClient.getRcMediaIntent(), /* mediaIntent */
    // rcClient.getIRemoteControlClient(),/* rcClient */
    // // used to match media button event receiver and audio focus
    // mContext.getPackageName()); /* packageName */
    // rcClient.setRcseId(rcseId);
    // } catch (RemoteException e) {
    // Log.e(TAG, "Dead object in registerRemoteControlClient"+e);
    // }
  }

  /**
   * Unregisters the remote control client that was providing information to display on the remote controls.
   * 
   * @param rcClient
   *          The remote control client to unregister.
   * @see #registerRemoteControlClient(RemoteControlClient)
   */
  public void unregisterRemoteControlClient(RemoteControlClient rcClient) {
    // if ((rcClient == null) || (rcClient.getRcMediaIntent() == null)) {
    // return;
    // }
    // IAudioService service = getService();
    // try {
    // service.unregisterRemoteControlClient(rcClient.getRcMediaIntent(), /* mediaIntent */
    // rcClient.getIRemoteControlClient()); /* rcClient */
    // } catch (RemoteException e) {
    // Log.e(TAG, "Dead object in unregisterRemoteControlClient"+e);
    // }
  }

  public int requestAudioFocus(android.media.AudioManager.OnAudioFocusChangeListener param0, int param1,
                               int param2) {
    // int i = AndroidVerify.getInt(0, 3, "AudioManager.requestAudioFocus");
    // switch (i) {
    // case 0:
      param0.onAudioFocusChange(AudioManager.AUDIOFOCUS_GAIN);
      return AudioManager.AUDIOFOCUS_REQUEST_GRANTED;
    // case 1:
    // param0.onAudioFocusChange(AUDIOFOCUS_LOSS);
    // break;
    // case 2:
    // param0.onAudioFocusChange(AUDIOFOCUS_LOSS_TRANSIENT);
    // break;
    // case 3:
    // param0.onAudioFocusChange(AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK);
    // break;
    // }
    // return AudioManager.AUDIOFOCUS_REQUEST_FAILED;

  }

  public int abandonAudioFocus(android.media.AudioManager.OnAudioFocusChangeListener param0) {
    param0.onAudioFocusChange(AUDIOFOCUS_LOSS);
    return AudioManager.AUDIOFOCUS_REQUEST_GRANTED;
  }
}