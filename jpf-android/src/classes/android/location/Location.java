package android.location;

import gov.nasa.jpf.vm.Abstraction;

public class Location {

  public Location() {
  }

  public long getTime() {
    return Abstraction.TOP_LONG;
  }

  public double getLongitude() {
    return Abstraction.TOP_DOUBLE;
  }

  public double getLatitude() {
    return Abstraction.TOP_DOUBLE;
  }

  public double getAltitude() {
    return Abstraction.TOP_DOUBLE;
  }

  public java.lang.String getProvider() {
    return Abstraction.TOP_STRING;
  }

  public boolean hasAltitude() {
    return Abstraction.TOP_BOOL;
  }

  public boolean hasSpeed() {
    return Abstraction.TOP_BOOL;
  }

  public float getSpeed() {
    return Abstraction.TOP_FLOAT;
  }

  public boolean hasBearing() {
    return Abstraction.TOP_BOOL;
  }

  public float getBearing() {
    return Abstraction.TOP_FLOAT;
  }

  public boolean hasAccuracy() {
    return Abstraction.TOP_BOOL;
  }

  public float getAccuracy() {
    return Abstraction.TOP_FLOAT;
  }
}