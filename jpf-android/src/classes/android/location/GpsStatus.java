package android.location;

import gov.nasa.jpf.vm.Abstraction;

import java.util.LinkedList;
import java.util.List;

public final class GpsStatus {

  /**
   * Event sent when the GPS system has started.
   */
  public static final int GPS_EVENT_STARTED = 1;

  /**
   * Event sent when the GPS system has stopped.
   */
  public static final int GPS_EVENT_STOPPED = 2;

  /**
   * Event sent when the GPS system has received its first fix since starting. Call
   * {@link #getTimeToFirstFix()} to find the time from start to first fix.
   */
  public static final int GPS_EVENT_FIRST_FIX = 3;

  /**
   * Event sent periodically to report GPS satellite status. Call {@link #getSatellites()} to retrieve the
   * status for each satellite.
   */
  public static final int GPS_EVENT_SATELLITE_STATUS = 4;

  /**
   * Used for receiving NMEA sentences from the GPS. NMEA 0183 is a standard for communicating with marine
   * electronic devices and is a common method for receiving data from a GPS, typically over a serial port.
   * See <a href="http://en.wikipedia.org/wiki/NMEA_0183">NMEA 0183</a> for more details. You can implement
   * this interface and call {@link LocationManager#addNmeaListener} to receive NMEA data from the GPS engine.
   */
  public interface NmeaListener {
    void onNmeaReceived(long timestamp, String nmea);
  }

  /**
   * Used for receiving notifications when GPS status has changed.
   */
  public interface Listener {
    /**
     * Called to report changes in the GPS status. The event number is one of:
     * <ul>
     * <li> {@link GpsStatus#GPS_EVENT_STARTED}
     * <li> {@link GpsStatus#GPS_EVENT_STOPPED}
     * <li> {@link GpsStatus#GPS_EVENT_FIRST_FIX}
     * <li> {@link GpsStatus#GPS_EVENT_SATELLITE_STATUS}
     * </ul>
     *
     * When this method is called, the client should call {@link LocationManager#getGpsStatus} to get
     * additional status information.
     *
     * @param event
     *          event number for this notification
     */
    void onGpsStatusChanged(int event);
  }

  public GpsStatus() {
    sats.add(new GpsSatellite());
  }

  public int getMaxSatellites() {
    return Abstraction.TOP_INT;
  }

  public Iterable<GpsSatellite> getSatellites() {
    return sats;
  }

  public List<GpsSatellite> sats = new LinkedList<GpsSatellite>();
  public GpsStatus mStatus;

  public void setStatus(GpsStatus mGpsStatus) {
    this.mStatus = mGpsStatus;
  }
}