package android.view;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.util.event.InvalidEventException;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.EventImpl;
import gov.nasa.jpf.util.event.events.KeyPressEvent;
import gov.nasa.jpf.util.event.events.UIEvent;

import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.SurfaceHolder.Callback2;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * Models the Window and PhoneWindow classes. Each {@link Activity} has at least one Window. Each Dialog has
 * it's own Window as well, so if an Activity has one or more Dialogs, it will have multiple Windows. The
 * Window stores the view hierarchy and allows this hierarchy to be traversed and changed dynamically.
 * 
 * 
 * 
 * window.superDispatch is calles from dialog when event is not handled by it to throw it back to window for
 * handling
 * 
 * @author "Heila van der Merwe"
 * 
 */
public class JPFWindow extends Window {
  private final static String TAG = "Window";
  final static boolean DEBUG_WINDOW = false;

  /** Used to store name of the activity/dialog to which this Window belongs. */
  private String name = "";

  public Dialog mDialog = null;

  // From Android
  TextView mTitleView; /* --- from PhoneWindow */
  String mTitle = "test";/* --- from PhoneWindow */

  /** Reference to the context's LayoutInflater --- from PhoneWindow */
  private LayoutInflater mLayoutInflater;
  private WindowManager mWindowManager; /* -- override Window's private field */

  /**
   * Root of the ViewTree without action bar etc
   */
  private View mContentParent; /* --- from PhoneWindow */

  /**
   * Root of view hierarchy with action bar menu etc
   */
  private ViewGroup mDecorView;; /* --- from PhoneWindow */
  @FilterField
  Menu menu = null;

  public JPFWindow(Context context) {
    super(context);
    name = context.getClass().getName();
    Log.i(TAG, "Creating new Window for " + name);
    mDecorView = new FrameLayout(context);
    Log.i(TAG, "Creating new LayouInflator");
    mLayoutInflater = LayoutInflater.from(context);
  }

  public View getContentView() {
    return mContentParent;
  }

  @Override
  public void setWindowManager(WindowManager wm, IBinder activity, String appName, boolean hardwareAccelerated) {
    // name = appName;
    if (mWindowManager == null)
      mWindowManager = JPFWindowManager.getInstance();
  }

  /**
   * Return the window manager allowing this Window to display its own windows.
   * 
   * @return WindowManager The ViewManager.
   */
  @Override
  public WindowManager getWindowManager() {
    return mWindowManager;
  }

  @Override
  public View findViewById(int id) {
    View v = mDecorView.findViewById(id);
    if (DEBUG_WINDOW)
      Log.i(TAG, "findViewById(id=" + id + "found=" + (v != null)
          + ((v != null) ? " type=" + v.getClass().getSimpleName() : "") + ")");
    return v;
  }

  protected View findViewByName(String name) {
    System.out.println("JPFWindow:" + this.name + " find view by name: " + name + " decorview: "
        + getDecorView());
    View v = mDecorView.findViewByName(name);
    if (DEBUG_WINDOW)
      Log.i(TAG, "findViewByName(name=" + name + " found=" + (v != null)
          + ((v != null) ? " type=" + v.getClass().getSimpleName() : "") + ")");
    return v;
  }

  public View findViewByText(String text) {
    View v = mDecorView.findViewByText(name);
    if (DEBUG_WINDOW)
      Log.i(TAG, "findViewByName(name=" + name + " found=" + (v != null)
          + ((v != null) ? " type=" + v.getClass().getSimpleName() : "") + ")");
    return v;
  }

  /**
   * Sets the parent view of this Window
   * 
   * @param v
   */
  @Override
  public void setContentView(int layoutResID) {
    View layout = mLayoutInflater.inflate(layoutResID, null); // inflate
    layout.mParent = null;
    setContentView(layout);

  }

  @Override
  public void setContentView(View v) {
    if (DEBUG_WINDOW)
      Log.i(TAG, "setContentView(id=" + v.getId() + " found=" + (v != null)
          + ((v != null) ? " type=" + v.getClass().getSimpleName() : "") + ")");
    mContentParent = v;
    mDecorView.addView(mContentParent);
    final Callback cb = getCallback();
    if (cb != null && !isDestroyed()) {
      cb.onContentChanged();
    }
  }

  public void addContentView(View v) {
    setContentView(v);
  }

  @Override
  public void setContentView(View view, LayoutParams params) {
    setContentView(view);

  }

  @Override
  public void addContentView(View view, LayoutParams params) {
    addContentView(view);
  }

  @Override
  public void setTitle(CharSequence title) {
    mTitle = title.toString();
  }

  private native void setVisible0();

  /**
   * Used to dispatch key events from window manager
   * 
   * @param event
   */
  public boolean dispatchEvent(InputEvent event) {

    if (event instanceof KeyEvent) {
      boolean handled = false;
      Log.i(TAG, "Dispatching " + event + " to " + getCallback());

      if (getCallback() != null) {
        handled = getCallback().dispatchKeyEvent((KeyEvent) event);
      }
      if (handled) {
        return true;
      }
    }
    return false;

  }

  Pattern p = Pattern.compile("\\((.*?)\\)");

  public View getView(String target) throws Exception {

    View view = null;

    if (target.contains("findViewByText")) {
      Matcher m = p.matcher(target);
      String text = m.group(1);
      view = findViewByText(text);

    } else if (target.contains("findDialogByName")) {

    } else if (target.startsWith("$")) {
      // find the view object by name
      view = findViewByName(target.substring(1));
    } else {
      // TODO more options
    }

    if (view != null) {
      return view;
    } else {
      Log.e(TAG, "No view with name " + target + " exists for window " + this.name);
      throw new Exception("Could not find view for target " + target);
    }
  }

  @Override
  public LayoutInflater getLayoutInflater() {
    return mLayoutInflater;
  }

  @Override
  public String toString() {
    return "Window [name=" + name + ", mTitleView=" + mTitleView + ", mTitle=" + mTitle + "]";
  }

  @Override
  public View getCurrentFocus() {
    return null;
  }

  @Override
  public void restoreHierarchyState(Bundle windowState) {

  }

  @Override
  public Bundle saveHierarchyState() {
    return null;
  }

  @Override
  public void onConfigurationChanged(Configuration newConfig) {
  }

  @Override
  public void setCloseOnTouchOutside(boolean finish) {
  }

  public boolean shouldCloseOnTouch(Activity activity, MotionEvent event) {
    return false;
  }

  public List<Event> getKeyEvents() {
    List<Event> events = new LinkedList<Event>();

    // get the class
    Class c = null;
    if (mDialog != null)
      c = mDialog.getClass();
    else
      c = getContext().getClass();

    Method mDown = null, mUp = null;
    try {
      Class[] params = { int.class, KeyEvent.class };
      mDown = c.getMethod("onKeyDown", params);
      mUp = c.getMethod("onKeyUp", params);
    } catch (NoSuchMethodException e) {
      System.out.println(e.toString());
    } catch (SecurityException e) {
      System.out.println(e.toString());
    }
    if (mDown != null) {
      String classname = mDown.getDeclaringClass().getName();
      if (!classname.equals("android.app.Activity") && !classname.equals("android.app.Dialog")) {
        // method has been overwritten by subclass
        KeyPressEvent s = new KeyPressEvent("device", "onKeyDown");
        events = new LinkedList<Event>();
        events.add(s);
      }
    }
    if (mUp != null) {
      String classname = mUp.getDeclaringClass().getName();
      if (!classname.equals("android.app.Activity") && !classname.equals("android.app.Dialog")) {
        // method has been overwritten by subclass
        KeyPressEvent s = new KeyPressEvent("device", "onKeyUp");
        events = new LinkedList<Event>();
        events.add(s);
      }
    }

    return events;
  }

  private Event getBackKeyEvent() {
    // a dialog can not handle back key if cancelable
    if (mDialog != null && !mDialog.isCancelable()) {
      return null;
    }
    KeyPressEvent k = new KeyPressEvent("device", "back");
    k.setKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
    return k;
  }

  /**
   * Check if menu exists
   * 
   * @return
   */
  public List<Event> getMenuEvents() {
    List<Event> events = new LinkedList<Event>();

    // dialog has no menu
    if (mDialog != null) {
      return events;
    }

    // get the onCreateOptionsMenu method
    Class c = getContext().getClass();
    Method m = null;
    try {
      Class[] params = { Menu.class };
      m = c.getMethod("onCreateOptionsMenu", params);
    } catch (NoSuchMethodException e) {
      System.out.println(e.toString());
    } catch (SecurityException e) {
      System.out.println(e.toString());
    }

    // if a subclass of activity defines it - a menu is required
    if (m != null) {
      String name = m.getDeclaringClass().getName();
      if (!name.equals("android.app.Activity")) {

        // inflate menu
        if (menu == null) {
          menu = new MenuImpl(getContext());
          ((Activity) getContext()).onCreateOptionsMenu(menu);
        }
        events = menu.getNextEvents();
        return events;
      }
    }
    return events;
  }

  /**
   * Main methods that collects next events
   * 
   * @return
   */
  public Event[] getNextEvents() {
    Log.i(TAG, "Getting next events from " + this.getName());

    // these are the events from the view hierarchy
    List<Event> windowEvents = mDecorView.collectEvents();
    Log.i(TAG, "UIEvents found: " + windowEvents);

    List<Event> events = getKeyEvents();
    if (events != null) {
      windowEvents.addAll(events);
    }

    events = getMenuEvents();
    if (events != null) {
      windowEvents.addAll(events);
    }

    Event ev = getBackKeyEvent();
    if (ev != null) {
      windowEvents.add(ev);
    }

    Event[] ret = new Event[windowEvents.size()];
    int i = 0;
    for (Event e : windowEvents) {
      ret[i] = e;
      ((EventImpl) e).setWindowName(this.name);
      i++;
    }
    return ret;

  }

  /**
   * Used by jpf-android to dispatch key-events to the current activity
   * 
   * @param name
   * @param action
   * @throws InvalidEventException
   */
  void processViewEvent(UIEvent event) throws InvalidEventException {
    String name = event.getTarget();
    String action = event.getAction();
    Object[] arguments = event.getArguments();
    Log.i(TAG, "Dispatching " + name + "." + action + "(" + arguments + ")");
    try {
      View view = getView(name);
      if (view == null) {
        throw new InvalidEventException("Could not find view for action " + action + " on  view " + name
            + " for window " + this.name);
      } else {
        // invoke the action on this view object
        view.processEvent(event);
      }
    } catch (Exception e) {
      Log.w(TAG, "Could not execute action " + action + " on " + name + " for window " + this.name);
      e.printStackTrace();
      throw new InvalidEventException("Could not execute action " + action + " on " + name + " for window "
          + this.name);
    }
  }

  public int getLengthOfArgs(Object[] args) {
    int j = 0;
    if (args != null && args.length > 0) {
      do {
        if (args[j] == null) {
          break;
        }
        j++;
      } while (args != null && j < args.length);
    }
    return j;
  }

  /**
   * handles all incomming keypress events
   * 
   * @param keyEvent
   * @return
   */
  public boolean processKeyEvent(KeyPressEvent keyEvent) {
    Activity a = (Activity) getContext();
    if (keyEvent.getAction().equals("back")) {
      // dialog
      if (mDialog != null) {
        mDialog.dismiss();
        return true;
        // menu
      } else if (menu != null && menu.isVisible()) {
        menu.setVisible(false);
        return true;
        // window
      } else {
        boolean handled = this.dispatchEvent(keyEvent.getKeyEvent());
        return handled;
      }
      // } else if (keyEvent.getAction().equals("menu")) {
      // // assume this can only be fired if window is unobscured by menu/dialog
      // boolean handled = this.dispatchEvent(keyEvent.getKeyEvent());
      // if (!handled) {
      // if (menu != null) {
      // ((Activity) getContext()).onPrepareOptionsMenu(menu);
      // menu.setVisible(true);
      // }
      // return true;
      // } else
      // return true;
    } else if (keyEvent.getAction().equals("menuItem")) {
      if (menu != null) {
        ((Activity) getContext()).onPrepareOptionsMenu(menu);
        menu.setVisible(true);

        System.out.println("Window processing menuitem");
        menu.processEvent(keyEvent);
      }
      return true;
    } else {
      KeyEvent.Callback callb = (KeyEvent.Callback) getContext();
      return callb.onKeyDown(keyEvent.getKeyEvent().getKeyCode(), keyEvent.getKeyEvent());
    }
  }

  @Override
  public View getDecorView() {
    return mDecorView;
  }

  public void setWindowManager(WindowManager mWindowManager2, Object object, Object object2) {
    this.mWindowManager = mWindowManager2;
  }

  @Override
  public boolean shouldCloseOnTouch(Context mContext2, MotionEvent event) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean superDispatchKeyEvent(KeyEvent event) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean requestFeature(int featureId) {
    // TODO Auto-generated method stub
    return false;
  }

  /**
   * Request that key events come to this dialog. Use this if your dialog has no views with focus, but the
   * dialog still wants a chance to process key events.
   * 
   * @param get
   *          true if the dialog should receive key events, false otherwise
   * @see android.view.Window#takeKeyEvents
   */
  @Override
  public void takeKeyEvents(boolean get) {
    // TODO Auto-generated method stub

  }

  @Override
  public void closePanel(int featureOptionsPanel) {
    // TODO Auto-generated method stub

  }

  public void openPanel(int featureOptionsPanel, Object object) {
    // TODO Auto-generated method stub

  }

  @Override
  public void invalidatePanelMenu(int featureOptionsPanel) {
    // TODO Auto-generated method stub

  }

  @Override
  public void closeAllPanels() {
    // TODO Auto-generated method stub

  }

  @Override
  public void takeSurface(Callback2 callback) {
    // TODO Auto-generated method stub

  }

  @Override
  public boolean isFloating() {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public void alwaysReadCloseOnTouchAttr() {
    // TODO Auto-generated method stub

  }

  @Override
  public void setTitleColor(int textColor) {
    // TODO Auto-generated method stub

  }

  @Override
  public void openPanel(int featureId, KeyEvent event) {
    // TODO Auto-generated method stub

  }

  @Override
  public void togglePanel(int featureId, KeyEvent event) {
    // TODO Auto-generated method stub

  }

  @Override
  public boolean performPanelShortcut(int featureId, int keyCode, KeyEvent event, int flags) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean performPanelIdentifierAction(int featureId, int id, int flags) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean performContextMenuIdentifierAction(int id, int flags) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public void setBackgroundDrawable(Drawable drawable) {
    // TODO Auto-generated method stub

  }

  @Override
  public void setFeatureDrawableResource(int featureId, int resId) {
    // TODO Auto-generated method stub

  }

  @Override
  public void setFeatureDrawableUri(int featureId, Uri uri) {
    // TODO Auto-generated method stub

  }

  @Override
  public void setFeatureDrawable(int featureId, Drawable drawable) {
    // TODO Auto-generated method stub

  }

  @Override
  public void setFeatureDrawableAlpha(int featureId, int alpha) {
    // TODO Auto-generated method stub

  }

  @Override
  public void setFeatureInt(int featureId, int value) {
    // TODO Auto-generated method stub

  }

  @Override
  public boolean superDispatchKeyShortcutEvent(KeyEvent event) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean superDispatchTouchEvent(MotionEvent event) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean superDispatchTrackballEvent(MotionEvent event) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean superDispatchGenericMotionEvent(MotionEvent event) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public View peekDecorView() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  protected void onActive() {
    // TODO Auto-generated method stub

  }

  @Override
  public void setChildDrawable(int featureId, Drawable drawable) {
    // TODO Auto-generated method stub

  }

  @Override
  public void setChildInt(int featureId, int value) {
    // TODO Auto-generated method stub

  }

  @Override
  public boolean isShortcutKey(int keyCode, KeyEvent event) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public void setVolumeControlStream(int streamType) {
    // TODO Auto-generated method stub

  }

  @Override
  public int getVolumeControlStream() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public void takeInputQueue(android.view.InputQueue.Callback arg0) {
    // TODO Auto-generated method stub

  }

  public void setName(String string) {
    this.name = string;

  }

  public String getName() {
    return name;
  }

}
