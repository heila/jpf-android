package android.view;

import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.KeyPressEvent;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import android.app.Activity;
import android.content.Context;
import android.view.ContextMenu.ContextMenuInfo;

public class MenuImpl implements Menu {

  boolean visible = true;
  HashMap<Integer, MenuItem> menuItems = new HashMap<Integer, MenuItem>();
  HashMap<Integer, Menu> menus = new HashMap<Integer, Menu>();

  Context mContext;
  private ContextMenuInfo menuInfo;

  public MenuImpl(Context c) {
    mContext = c;
  }

  @Override
  // (int groupId, int itemId, int order, int titleRes)
  public MenuItem add(int groupId, int itemId, int order, int titleRes) {
    MenuItemImpl item = new MenuItemImpl(groupId, itemId, mContext.getString(titleRes));
    item.setEnabled(true);
    menuItems.put(itemId, item);
    return item;
  }

  @Override
  public SubMenu addSubMenu(int groupId, int itemId, int order, int titleRes) {
    SubMenu s = new SubMenuImpl(mContext);
    menus.put(itemId, s);
    return s;
  }

  @Override
  public MenuItem add(int groupId, int itemId, int order, CharSequence title) {
    MenuItemImpl item = new MenuItemImpl(groupId, itemId, title.toString());
    item.setEnabled(true);
    menuItems.put(itemId, item);
    return item;
  }

  @Override
  public android.view.MenuItem findItem(int param0) {
    MenuItem mi = menuItems.get(param0);
    if (mi == null) {
      for (Entry<Integer, Menu> menuEntry : menus.entrySet()) {
        Menu m = menuEntry.getValue();
        mi = m.findItem(param0);
        if (mi != null) {
          break;
        }
      }
    }
    return mi;
  }

  @Override
  public List<Event> getNextEvents() {
    System.out.println("Menu: getNextEvents " + menuItems.size() + " " + menus.size());
    List<Event> events = new LinkedList<Event>();
    if (isVisible()) {
      events.addAll(getEvents());

      for (Entry<Integer, Menu> menuEntry : menus.entrySet()) {
        Menu m = menuEntry.getValue();
        List<Event> e = m.getNextEvents();
        for (Event ee : e) {
          System.out.println("submenu" + ee);
          if (ee != null)
            events.add(ee);
        }
      }
    }
    return events;
  }

  public List<Event> getEvents() {
    List<Event> events = new LinkedList<Event>();

    for (Entry<Integer, MenuItem> mItem : menuItems.entrySet()) {
      if (mItem.getValue().isEnabled() && mItem.getValue().isVisible()) {
        KeyPressEvent k = new KeyPressEvent("device", "menuItem");
        k.setKeyEvent(null);
        k.addArgument(mItem.getValue().getTitle());
        events.add(k);
        System.out.println("Adding event " + k);
      }
    }
    return events;
  }

  @Override
  public boolean isVisible() {
    return visible;
  }

  @Override
  public void setVisible(boolean visible) {
    this.visible = visible;
  }

  @Override
  public void processEvent(KeyPressEvent keyEvent) {
    System.out.println("PROCESSING MENUITEM EVENT" + keyEvent);
    boolean processed = process(keyEvent);

    if (!processed) {
      for (Entry<Integer, Menu> menuEntry : menus.entrySet()) {
        Menu m = menuEntry.getValue();
        processed = m.process(keyEvent);
        if (processed) {
          break;
        }
      }
    }
  }

  @Override
  public boolean process(KeyPressEvent keyEvent) {
    String id = (String) keyEvent.getArguments()[0];
    System.out.println("PROCESSING MENUITEM EVENT 2:" + id);

    for (Entry<Integer, MenuItem> mItem : menuItems.entrySet()) {
      if (mItem.getValue().getTitle().equals(id)) {
        ((Activity) mContext).onOptionsItemSelected(mItem.getValue());
        return true;
      }
    }
    return false;
  }

  @Override
  public int size() {
    int s = menuItems.size();

    for (Entry<Integer, Menu> menuEntry : menus.entrySet())
      s += menuEntry.getValue().size();
    System.out.println("BBBB" + s);
    return s;
  }

  @Override
  public Collection<MenuItem> getItems() {
    Collection<MenuItem> s = menuItems.values();

    for (Entry<Integer, Menu> menuEntry : menus.entrySet())
      s.addAll(menuEntry.getValue().getItems());
    return s;
  }

  public void setInfo(ContextMenuInfo menuinfo) {
    this.menuInfo = menuinfo;

  }

  public ContextMenuInfo getInfo() {
    return menuInfo;
  }

}
