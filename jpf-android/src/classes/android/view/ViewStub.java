package android.view;

import android.content.Context;
import android.util.AttributeSet;

public final class ViewStub extends android.view.View {

  public ViewStub(Context context) {
    super(context);
  }

  public ViewStub(Context context, int layoutResource) {
    super(context);

  }

  public ViewStub(Context context, AttributeSet attrs) {
    super(context);

  }

  public ViewStub(Context context, AttributeSet attrs, int defStyle) {
    super(context);

  }
}