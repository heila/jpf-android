package android.view;

public interface MenuItem {

  public abstract android.view.ContextMenu.ContextMenuInfo getMenuInfo();

  public abstract android.view.MenuItem setIcon(int param0);

  public abstract android.view.MenuItem setTitle(int param0);

  public abstract int getItemId();

  public abstract android.view.MenuItem setEnabled(boolean param0);

  public abstract CharSequence getTitle();

  public abstract boolean isEnabled();

  public void setMenuInfo(android.view.ContextMenu.ContextMenuInfo info);

  public abstract MenuItem setActionView(View actionViewClass);

  public View getActionView();

  public MenuItem setVisible(boolean param0);

  boolean isVisible();
}