package android.os;

import android.content.Context;

import com.android.server.am.ActivityManagerService;


public class MockMessageQueue extends MessageQueue {

  MockMessageQueue(boolean quitAllowed) {
    super(quitAllowed);
  }

  @Override
  public boolean enqueueStopMessage() {
    boolean done;
    if (ServiceManager.getManager() != null) { // in case of testing
      ActivityManagerService am = (ActivityManagerService) ServiceManager
          .getSystemService(Context.ACTIVITY_SERVICE);
      done = am.stopApplication();

      if (done) {
        // stop application is done and the main looper can stop now
        synchronized (this) {
          if (mQuitting) { // to ensure we don't enqueue multiple stop messages
            return true;
          }
          mQuitting = true;
          mMessages.add(new Message());
          notify();
        }
      } else {
        // stop application pushed more message on queue so do not stop yet
      }
    } else {
      mQuitting = true;
    }
    return true;
  }


}