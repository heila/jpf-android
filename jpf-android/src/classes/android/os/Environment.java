package android.os;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.annotation.NeverBreak;

import java.io.File;

/**
 * Default stub for environment generated with OCSEGen and modified appropriately.
 *
 */
public class Environment {

  public static final java.lang.String MEDIA_UNKNOWN = "unknown";
  public static final java.lang.String MEDIA_REMOVED = "removed";
  public static final java.lang.String MEDIA_UNMOUNTED = "unmounted";
  public static final java.lang.String MEDIA_CHECKING = "checking";
  public static final java.lang.String MEDIA_NOFS = "nofs";
  public static final java.lang.String MEDIA_MOUNTED = "mounted";
  public static final java.lang.String MEDIA_MOUNTED_READ_ONLY = "mounted_ro";
  public static final java.lang.String MEDIA_SHARED = "shared";
  public static final java.lang.String MEDIA_BAD_REMOVAL = "bad_removal";
  public static final java.lang.String MEDIA_UNMOUNTABLE = "unmountable";

  @FilterField
  @NeverBreak
  private static final File out = new File("/storage/sdcard");
  @FilterField
  @NeverBreak
  private static final File root = new File("/storage");

  public static File getRootDirectory() {
    return root;
  }

  public static File getDataDirectory() {
    return out;
  }

  public static File getExternalStorageDirectory() {
    return out;
  }

  public static String DIRECTORY_MUSIC = "Music";
  public static String DIRECTORY_PODCASTS = "Podcasts";
  public static String DIRECTORY_RINGTONES = "Ringtones";
  public static String DIRECTORY_ALARMS = "Alarms";
  public static String DIRECTORY_NOTIFICATIONS = "Notifications";
  public static String DIRECTORY_PICTURES = "Pictures";
  public static String DIRECTORY_MOVIES = "Movies";
  public static String DIRECTORY_DOWNLOADS = "Download";
  public static String DIRECTORY_DCIM = "DCIM";

  public static File getExternalStoragePublicDirectory(String type) {
    return out;
  }

  public static File getDownloadCacheDirectory() {
    return out;
  }

  static final String[] states = { Environment.MEDIA_MOUNTED };// , MEDIA_SHARED, MEDIA_MOUNTED_READ_ONLY,

  // MEDIA_UNMOUNTED };
  // static final String state = AndroidVerify.getString(states, "Environment.getExternalStorageState");

  public static String getExternalStorageState() {
    return Environment.MEDIA_MOUNTED;
  }

  public static boolean isExternalStorageRemovable() {
    return false;
  }

  public static boolean isExternalStorageEmulated() {
    return true;
  }

}
