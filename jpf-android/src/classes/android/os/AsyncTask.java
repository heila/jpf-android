package android.os;

import gov.nasa.jpf.annotation.NeverBreak;

import java.util.concurrent.atomic.AtomicBoolean;
/**
 * This class models the AsyncTask class used to run operations that will finish
 * in a few seconds (quickly) on a thread other than the main thread and then to
 * post back the progress and result on the main thread since the GUI and
 * components of the application is not thread safe.
 * 
 * The original implementation makes use of a static SerialExecuter to schedule
 * all task of the Application (or component) one by one (by default thread pool
 * size = 1). I say Application/component since the SerialExecuter is static so
 * it is created when the class in loaded. In other words it depends on where
 * and for whom ClassLoaders are created. I think in the general case
 * ClassLoaders are shared between components since you can access static
 * variable between components (although you probably should not to avoid memory
 * leaks).
 * 
 * This implementation is too complex (in terms of scheduling possibilities) to
 * execute on JPF. We simplified the implementation by spawning a new Thread
 * each time a AsyncThread is executed to execute its doInBackground() method
 * and then post progress & results back to the main thread using a internal
 * handler.
 * 
 * 
 * Concurrency errors
 * 
 * An AsyncTask can only be executed once (see original documentation of
 * execute() method) since it stores a reference to the Runnable executed in the
 * doInBackground() method with specific parameters. If you try to execute it
 * again, the status of the thread is checked and an exception will be thrown if
 * this is not the first run. (see execute() method).
 * 
 * This means our fields are thread safe (only if accessed from main thread
 * while doInBackground() is called they can cause a problem). The main
 * possibility is that the AsyncThread may be passed a context, component or GUI
 * element reference that is updated in doInbackground(). This can cause a
 * deadlock or race condition since multiple AsyncThread or the main thread can
 * try to update the reference at the same time.
 * 
 * In the original implementation, there will not be a race condition between
 * the doInBackground() methods of two AsyncThreads since the SerialExecutor
 * executes the tasks one by one in the main thread.
 * 
 * Memory Leaks
 * 
 * If the Asynctask has a reference to a context, component or any element
 * created and used/update in the main thread this is a problem in the case
 * where the component that started the task is destroyed or crashes. In the
 * case of a resource being closed in the component (for example the camera)
 * this can also lead to a crash.
 * 
 * State Matching
 * 
 * Since we removed the queue of runnables by eliminating the Serial executor,
 * we will only take into account the number of AsyncTasks and their fields (as
 * well as where they are in terms of execution.)
 * 
 * We removed the ThreadFactory since it makes use of a counter to name the
 * Threads but this limits state matching.
 * 
 * 
 * NOT USING Future task to keep it simple for now
 * 
 * 
 * @author Heila
 *
 * @param <Params>
 * @param <Progress>
 * @param <Result>
 */
public abstract class AsyncTask<Params, Progress, Result> {
  private static final String LOG_TAG = "AsyncTask";

  @NeverBreak
  private final InternalHandler sHandler = new InternalHandler();
  private static final int MESSAGE_POST_RESULT = 0x1;
  private static final int MESSAGE_POST_PROGRESS = 0x2;

  private volatile Status mStatus = Status.PENDING;

  /**
   * Indicates the current status of the task. Each status will be set only once
   * during the lifetime of a task.
   */
  public enum Status {
    /**
     * Indicates that the task has not been executed yet.
     */
    PENDING,
    /**
     * Indicates that the task is running.
     */
    RUNNING,
    /**
     * Indicates that {@link AsyncTask#onPostExecute} has finished.
     */
    FINISHED,
  }

  // needs to be thread safe since it can be checked by main thread
  private final AtomicBoolean mCancelled = new AtomicBoolean();

  // The runnable to execute in new thread
  private final JPFRunnable<Params, Result> mRunnable;

  private final Thread mThread;

  public AsyncTask() {
    mRunnable = new JPFRunnable<Params, Result>() {

      @Override
      public void run() {
        Result r = doInBackground(mParams);
        postResult(r);
      }

    };
    mThread = new Thread(mRunnable);
    mThread.setName("AsyncThread");
   }

  private Result postResult(Result result) {
    @SuppressWarnings("unchecked")
    Message message = sHandler.obtainMessage(MESSAGE_POST_RESULT, new AsyncTaskResult<Result>(this,
        result));
    message.sendToTarget();
    return result;
  }

  public final Status getStatus() {
    return mStatus;
  }

  protected abstract Result doInBackground(Params... params);

  protected void onPreExecute() {
  }

  protected void onPostExecute(Result result) {
  }

  protected void onProgressUpdate(Progress... values) {
  }

  protected void onCancelled(Result result) {
    onCancelled();
  }

  protected void onCancelled() {
  }

  public final boolean isCancelled() {
    return mCancelled.get();

  }

  public final boolean cancel(boolean mayInterruptIfRunning) {
    mCancelled.set(true);
    if ((!mThread.isAlive()) || (mThread.isAlive() && mayInterruptIfRunning)) {
      mThread.interrupt();
    }
    // TODO cancel thread
    return mThread.isAlive();
  }

  public final AsyncTask<Params, Progress, Result> execute(Params... params) {
    if (mStatus != Status.PENDING) {
      switch (mStatus) {
      case RUNNING:
        throw new IllegalStateException("Cannot execute task:" + " the task is already running.");
      case FINISHED:
        throw new IllegalStateException("Cannot execute task:"
            + " the task has already been executed " + "(a task can be executed only once)");
      }
    }
    mStatus = Status.RUNNING;
    onPreExecute();
    mRunnable.mParams = params;
    mThread.start();
    return this;
  }

  protected final void publishProgress(Progress... values) {
    if (!isCancelled()) {
      sHandler.obtainMessage(MESSAGE_POST_PROGRESS, new AsyncTaskResult<Progress>(this, values))
          .sendToTarget();
    }
  }

  private void finish(Result result) {
    if (isCancelled()) {
      onCancelled(result);
    } else {
      onPostExecute(result);
    }
    mStatus = Status.FINISHED;
  }

  private static class InternalHandler extends Handler {
    @Override
    public void handleMessage(Message msg) {
      AsyncTaskResult result = (AsyncTaskResult) msg.obj;
      switch (msg.what) {
      case MESSAGE_POST_RESULT:
        // There is only one result
        result.mTask.finish(result.mData[0]);
        break;
      case MESSAGE_POST_PROGRESS:
        result.mTask.onProgressUpdate(result.mData);
        break;
      }
    }
  }

  public static abstract class JPFRunnable<Params, Result> implements Runnable {
    Params[] mParams;
  }

  @SuppressWarnings({ "RawUseOfParameterizedType" })
  private static class AsyncTaskResult<Data> {
    final AsyncTask mTask;
    final Data[] mData;

    AsyncTaskResult(AsyncTask task, Data... data) {
      mTask = task;
      mData = data;
    }
  }

}
