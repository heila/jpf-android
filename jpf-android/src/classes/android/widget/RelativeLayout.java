package android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;

public class RelativeLayout extends ViewGroup {

  public static class LayoutParams extends ViewGroup.LayoutParams {
    public static final LayoutParams TOP = new LayoutParams(null);

    public LayoutParams(android.view.ViewGroup.LayoutParams source) {
      super(source);
    }

    public LayoutParams(int i, int j) {
      super(i, j);
    }

    public void addRule(int i, int h) {

    }
  }

  public RelativeLayout(Context context) {
    super(context);
  }

  public RelativeLayout(Context context, AttributeSet att) {
    this(context);
  }

  @Override
  public android.view.ViewGroup.LayoutParams getLayoutParams() {
    return RelativeLayout.LayoutParams.TOP;
  }

}
