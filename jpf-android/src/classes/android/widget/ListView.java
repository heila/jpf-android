package android.widget;

import gov.nasa.jpf.vm.Abstraction;
import android.content.Context;
import android.view.View;

public class ListView extends AbsListView {

  /**
   * Used to indicate a no preference for a position type.
   */
  static final int NO_POSITION = -1;

  /**
   * When arrow scrolling, ListView will never scroll more than this factor times the height of the list.
   */
  private static final float MAX_SCROLL_FACTOR = 0.33f;

  /**
   * When arrow scrolling, need a certain amount of pixels to preview next items. This is usually the fading
   * edge, but if that is small enough, we want to make sure we preview at least this many pixels.
   */
  private static final int MIN_SCROLL_PREVIEW_PIXELS = 2;


  // private ArrayList<FixedViewInfo> mHeaderViewInfos = new ArrayList<FixedViewInfo>();
  // private ArrayList<FixedViewInfo> mFooterViewInfos = new ArrayList<FixedViewInfo>();

  public ListView(Context context) {
    super(context);
  }

  public ListView(android.content.Context param0, android.util.AttributeSet param1) {
    super(param0);

  }

  public ListView(android.content.Context param0, android.util.AttributeSet param1, int param2) {
    super(param0);
  }


  @Override
  public ListAdapter getAdapter() {
    return mAdapter;
  }

  public void setRemoteViewsAdapter(android.content.Intent param0) {
  }

  @Override
  public void setAdapter(android.widget.ListAdapter adapter) {
    System.out.println("TEST!!1" + adapter);
    super.setAdapter(adapter);
    resetList();
    if (adapter != null) {
    mOldSelectedPosition = INVALID_POSITION;
    mOldSelectedRowId = INVALID_ROW_ID;
    redraw();
    }
  }

  private void redraw() {
    int num = getAdapter().getCount();
    System.out.println("TEST!!1" + num);

    for (int i = 0; i < num; i++) {
      this.addView(mAdapter.getView(i, null, this));
    }
  }

  void resetList() {
    // The parent's resetList() will remove all views from the layout so we need to
    // cleanup the state of our footers and headers
    // clearRecycledState(mHeaderViewInfos);
    // clearRecycledState(mFooterViewInfos);

    // super.resetList();
    this.removeViews();

    // mLayoutMode = LAYOUT_NORMAL;
  }

  /**
   * A class that represents a fixed view in a list, for example a header at the top or a footer at the
   * bottom.
   */
  public class FixedViewInfo {
    /** The view to add to the list */
    public View view;
    /** The data backing the view. This is returned from {@link ListAdapter#getItem(int)}. */
    public Object data;
    /** <code>true</code> if the fixed view should be selectable in the list */
    public boolean isSelectable;
  }

  public int getMaxScrollAmount() {
    return Abstraction.TOP_INT;
  }

  public void addHeaderView(View v, Object data, boolean isSelectable) {
  }

  public void addHeaderView(View v) {

  }

  public int getHeaderViewsCount() {
    return 0;
  }

  public boolean removeHeaderView(android.view.View param0) {
    return true;
  }

  public void addFooterView(android.view.View param0, java.lang.Object param1, boolean param2) {
  }

  public void addFooterView(android.view.View param0) {
  }

  public int getFooterViewsCount() {
    return Abstraction.TOP_INT;
  }

  public boolean removeFooterView(android.view.View param0) {
    return Abstraction.TOP_BOOL;
  }

  @Override
  public boolean requestChildRectangleOnScreen(android.view.View param0, android.graphics.Rect param1,
                                               boolean param2) {
    return true;
  }

  public void smoothScrollToPosition(int param0) {
  }

  public void smoothScrollByOffset(int param0) {
  }

  @Override
  protected void onSizeChanged(int param0, int param1, int param2, int param3) {
  }

  @Override
  protected void onMeasure(int param0, int param1) {
  }

  protected boolean recycleOnMeasure() {
    return Abstraction.TOP_BOOL;
  }

  protected void layoutChildren() {
  }

  @Override
  protected boolean canAnimate() {
    return Abstraction.TOP_BOOL;
  }

  @Override
  public void setSelection(int param0) {
  }

  public void setSelectionFromTop(int param0, int param1) {
  }

  public void setSelectionAfterHeaderView() {
  }

  @Override
  public boolean dispatchKeyEvent(android.view.KeyEvent param0) {
    return Abstraction.TOP_BOOL;
  }

  @Override
  public boolean onKeyDown(int param0, android.view.KeyEvent param1) {
    return Abstraction.TOP_BOOL;
  }

  @Override
  public boolean onKeyMultiple(int param0, int param1, android.view.KeyEvent param2) {
    return Abstraction.TOP_BOOL;
  }

  @Override
  public boolean onKeyUp(int param0, android.view.KeyEvent param1) {
    return Abstraction.TOP_BOOL;
  }

  public void setItemsCanFocus(boolean param0) {
  }

  public boolean getItemsCanFocus() {
    return Abstraction.TOP_BOOL;
  }

  @Override
  public boolean isOpaque() {
    return Abstraction.TOP_BOOL;
  }

  public void setCacheColorHint(int param0) {
  }

  @Override
  protected void dispatchDraw(android.graphics.Canvas param0) {
  }

  protected boolean drawChild(android.graphics.Canvas param0, android.view.View param1, long param2) {
    return Abstraction.TOP_BOOL;
  }

  void drawDivider(android.graphics.Canvas param0, android.graphics.Rect param1, int param2) {
  }

  public android.graphics.drawable.Drawable getDivider() {
    return ((android.graphics.drawable.Drawable) Abstraction
        .randomObject("android.graphics.drawable.Drawable"));
  }

  public void setDivider(android.graphics.drawable.Drawable param0) {
  }

  public int getDividerHeight() {
    return Abstraction.TOP_INT;
  }

  public void setDividerHeight(int param0) {
  }

  public void setHeaderDividersEnabled(boolean param0) {
  }

  public boolean areHeaderDividersEnabled() {
    return Abstraction.TOP_BOOL;
  }

  public void setFooterDividersEnabled(boolean param0) {
  }

  public boolean areFooterDividersEnabled() {
    return Abstraction.TOP_BOOL;
  }

  public void setOverscrollHeader(android.graphics.drawable.Drawable param0) {
  }

  public android.graphics.drawable.Drawable getOverscrollHeader() {
    return ((android.graphics.drawable.Drawable) Abstraction
        .randomObject("android.graphics.drawable.Drawable"));
  }

  public void setOverscrollFooter(android.graphics.drawable.Drawable param0) {
  }

  public android.graphics.drawable.Drawable getOverscrollFooter() {
    return ((android.graphics.drawable.Drawable) Abstraction
        .randomObject("android.graphics.drawable.Drawable"));
  }

  @Override
  protected void onFocusChanged(boolean param0, int param1, android.graphics.Rect param2) {
  }

  @Override
  protected void onFinishInflate() {
  }

  public long[] getCheckItemIds() {
    return ((long[]) Abstraction.randomObject("long[]"));
  }

  @Override
  public void onInitializeAccessibilityEvent(android.view.accessibility.AccessibilityEvent param0) {
  }

  @Override
  public void onInitializeAccessibilityNodeInfo(android.view.accessibility.AccessibilityNodeInfo param0) {
  }

  public void onInitializeAccessibilityNodeInfoForItem(android.view.View param0, int param1,
                                                       android.view.accessibility.AccessibilityNodeInfo param2) {
  }

  @Override
  public View getSelectedView() {
    return null;
  }

  public void focusableViewAvailable(ListView mList) {
  }

}
