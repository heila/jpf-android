package android.widget;

import android.content.Context;
import android.util.AttributeSet;

public class Button extends TextView {
  public Button(Context context) {
    super(context);
  }

  public Button(Context c, AttributeSet att) {
    super(c, att);
  }
}