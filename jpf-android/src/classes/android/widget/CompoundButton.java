/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.widget;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.UIEvent;
import gov.nasa.jpf.vm.AndroidVerify;

import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.ViewDebug;

/**
 * <p>
 * A button with two states, checked and unchecked. When the button is pressed or clicked, the state changes
 * automatically.
 * </p>
 * 
 * <p>
 * <strong>XML attributes</strong>
 * </p>
 * <p>
 * See {@link android.R.styleable#CompoundButton CompoundButton Attributes},
 * {@link android.R.styleable#Button Button Attributes}, {@link android.R.styleable#TextView TextView
 * Attributes}, {@link android.R.styleable#View View Attributes}
 * </p>
 */
public abstract class CompoundButton extends Button implements Checkable {
  @FilterField
  private boolean mChecked;
  private boolean mBroadcasting;

  private OnCheckedChangeListener mOnCheckedChangeListener;
  private OnCheckedChangeListener mOnCheckedChangeWidgetListener;

  public CompoundButton(Context context) {
    super(context);
  }

  @Override
  public void toggle() {
    setChecked(!mChecked);
  }

  @Override
  public boolean performClick() {
    /*
     * XXX: These are tiny, need some surrounding 'expanded touch area', which will need to be implemented in
     * Button if we only override performClick()
     */

    /* When clicked, toggle the state */
    toggle();
    return super.performClick();
  }

  @Override
  @ViewDebug.ExportedProperty
  public boolean isChecked() {
    return mChecked;
  }

  /**
   * <p>
   * Changes the checked state of this button.
   * </p>
   * 
   * @param checked
   *          true to check the button, false to uncheck it
   */
  @Override
  public void setChecked(boolean checked) {
    if (mChecked != checked) {
      mChecked = checked;

      // Avoid infinite recursions if setChecked() is called from a listener
      if (mBroadcasting) {
        return;
      }

      mBroadcasting = true;
      if (mOnCheckedChangeListener != null) {
        mOnCheckedChangeListener.onCheckedChanged(this, mChecked);
      }
      if (mOnCheckedChangeWidgetListener != null) {
        mOnCheckedChangeWidgetListener.onCheckedChanged(this, mChecked);
      }

      mBroadcasting = false;
    }
  }

  /**
   * Register a callback to be invoked when the checked state of this button changes.
   * 
   * @param listener
   *          the callback to call on checked state change
   */
  public void setOnCheckedChangeListener(OnCheckedChangeListener listener) {
    mOnCheckedChangeListener = listener;
  }

  /**
   * Register a callback to be invoked when the checked state of this button changes. This callback is used
   * for internal purpose only.
   * 
   * @param listener
   *          the callback to call on checked state change
   * @hide
   */
  void setOnCheckedChangeWidgetListener(OnCheckedChangeListener listener) {
    mOnCheckedChangeWidgetListener = listener;
  }

  /**
   * Interface definition for a callback to be invoked when the checked state of a compound button changed.
   */
  public static interface OnCheckedChangeListener {
    /**
     * Called when the checked state of a compound button has changed.
     * 
     * @param buttonView
     *          The compound button view whose state has changed.
     * @param isChecked
     *          The new checked state of buttonView.
     */
    void onCheckedChanged(CompoundButton buttonView, boolean isChecked);
  }

  /**
   * Set the background to a given Drawable, identified by its resource id.
   * 
   * @param resid
   *          the resource id of the drawable to use as the background
   */
  public void setButtonDrawable(int resid) {
  }

  /**
   * Set the background to a given Drawable
   * 
   * @param d
   *          The Drawable to use as the background
   */
  public void setButtonDrawable(Drawable d) {
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);

  }

  static class SavedState {
    boolean checked;
  }

  @Override
  public List<Event> collectEvents() {
    List<Event> events = new LinkedList<Event>();
    if (mOnCheckedChangeListener != null) {
      UIEvent uiEvent = new UIEvent("$" + this.getName(), "onCheckedChange");
      events.add(uiEvent);
    }
    events.addAll(super.collectEvents());
    return events;
  }

  @Override
  public boolean processEvent(Event event) {
    if (mOnCheckedChangeListener != null && event instanceof UIEvent && ((UIEvent) event).getAction().equals("onCheckedChange")) {
      (this).mOnCheckedChangeListener.onCheckedChanged(this, AndroidVerify.getBoolean("Checkbutton.check"));
      return true;
    }

    return super.processEvent(event);
  }
}
