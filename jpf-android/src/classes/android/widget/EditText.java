package android.widget;

import gov.nasa.jpf.vm.Abstraction;
import android.content.Context;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.method.TransformationMethod;
import android.util.AttributeSet;

public class EditText extends TextView {

  public EditText(Context context) {
    super(context, true);
  }

  public EditText(Context context, AttributeSet attrs) {
    super(context);
  }

  public EditText(Context context, AttributeSet attrs, int defStyle) {
    super(context);
  }

  @Override
  public Editable getText() {
    CharSequence seq = super.getText();
    if (seq != null)
      return new SpannableStringBuilder(seq);
    return new SpannableStringBuilder("");
  }

  public void setSelection(int start, int stop) {
    // Selection.setSelection(getText(), start, stop);
  }

  public void setSelection(int index) {
    // Selection.setSelection(getText(), index);
  }

  public void selectAll() {
    // Selection.selectAll(getText());
  }

  public void extendSelection(int index) {
    // Selection.extendSelection(getText(), index);
  }

  @Override
  public void setTransformationMethod(TransformationMethod t) {

  }

  @Override
  public void setEllipsize(android.text.TextUtils.TruncateAt param0) {
  }

  @Override
  public void onInitializeAccessibilityEvent(android.view.accessibility.AccessibilityEvent param0) {
  }

  @Override
  public void onInitializeAccessibilityNodeInfo(android.view.accessibility.AccessibilityNodeInfo param0) {
  }

  @Override
  public boolean performAccessibilityAction(int param0, android.os.Bundle param1) {
    return Abstraction.TOP_BOOL;
  }
}
