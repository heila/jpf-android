package android.widget;

import android.content.Context;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

public abstract class CursorAdapter extends android.widget.BaseAdapter implements Filterable,
    CursorFilter.CursorFilterClient {

  /**
   * This field should be made private, so it is hidden from the SDK. {@hide}
   */
  protected boolean mDataValid;
  /**
   * This field should be made private, so it is hidden from the SDK. {@hide}
   */
  protected boolean mAutoRequery;
  /**
   * This field should be made private, so it is hidden from the SDK. {@hide}
   */
  protected Cursor mCursor;
  /**
   * This field should be made private, so it is hidden from the SDK. {@hide}
   */
  protected Context mContext;
  /**
   * This field should be made private, so it is hidden from the SDK. {@hide}
   */
  protected int mRowIDColumn;
  /**
   * This field should be made private, so it is hidden from the SDK. {@hide}
   */
  // protected ChangeObserver mChangeObserver;
  /**
   * This field should be made private, so it is hidden from the SDK. {@hide}
   */
  protected DataSetObserver mDataSetObserver;
  /**
   * This field should be made private, so it is hidden from the SDK. {@hide}
   */
  protected CursorFilter mCursorFilter;
  /**
   * This field should be made private, so it is hidden from the SDK. {@hide}
   */
  protected FilterQueryProvider mFilterQueryProvider;

  /**
   * If set the adapter will call requery() on the cursor whenever a content change notification is delivered.
   * Implies {@link #FLAG_REGISTER_CONTENT_OBSERVER}.
   *
   * @deprecated This option is discouraged, as it results in Cursor queries being performed on the
   *             application's UI thread and thus can cause poor responsiveness or even Application Not
   *             Responding errors. As an alternative, use {@link android.app.LoaderManager} with a
   *             {@link android.content.CursorLoader}.
   */
  @Deprecated
  public static final int FLAG_AUTO_REQUERY = 0x01;

  /**
   * If set the adapter will register a content observer on the cursor and will call
   * {@link #onContentChanged()} when a notification comes in. Be careful when using this flag: you will need
   * to unset the current Cursor from the adapter to avoid leaks due to its registered observers. This flag is
   * not needed when using a CursorAdapter with a {@link android.content.CursorLoader}.
   */
  public static final int FLAG_REGISTER_CONTENT_OBSERVER = 0x02;

  public CursorAdapter(Context context, Cursor c) {
    init(context, c, FLAG_AUTO_REQUERY);
  }

  public CursorAdapter(Context context, Cursor c, boolean autoRequery) {
    init(context, c, autoRequery ? FLAG_AUTO_REQUERY : FLAG_REGISTER_CONTENT_OBSERVER);

  }

  public CursorAdapter(Context context, Cursor c, int flags) {
    init(context, c, flags);
  }

  void init(Context context, Cursor c, int flags) {
    if ((flags & FLAG_AUTO_REQUERY) == FLAG_AUTO_REQUERY) {
      flags |= FLAG_REGISTER_CONTENT_OBSERVER;
      mAutoRequery = true;
    } else {
      mAutoRequery = false;
    }
    boolean cursorPresent = c != null;
    mCursor = c;
    mDataValid = cursorPresent;
    mContext = context;
    mRowIDColumn = 0;// cursorPresent ? c.getColumnIndexOrThrow("_id") : -1;
    // if ((flags & FLAG_REGISTER_CONTENT_OBSERVER) == FLAG_REGISTER_CONTENT_OBSERVER) {
    // mChangeObserver = new ChangeObserver();
    // mDataSetObserver = new MyDataSetObserver();
    // } else {
    // mChangeObserver = null;
    // mDataSetObserver = null;
    // }

    // if (cursorPresent) {
    // if (mChangeObserver != null)
    // c.registerContentObserver(mChangeObserver);
    // if (mDataSetObserver != null)
    // c.registerDataSetObserver(mDataSetObserver);
    // }
  }

  @Override
  public CharSequence convertToString(Cursor cursor) {
    return cursor == null ? "" : cursor.toString();
  }

  @Override
  public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
    if (mFilterQueryProvider != null) {
      return mFilterQueryProvider.runQuery(constraint);
    }

    return mCursor;
  }

  @Override
  public android.database.Cursor getCursor() {
    return mCursor;
  }

  /**
   * @see android.widget.ListAdapter#getCount()
   */
  @Override
  public int getCount() {
    if (mDataValid && mCursor != null) {
      return mCursor.getCount();
    } else {
      return 0;
    }
  }

  /**
   * @see android.widget.ListAdapter#getItem(int)
   */
  @Override
  public Object getItem(int position) {
    if (mDataValid && mCursor != null) {
      mCursor.moveToPosition(position);
      return mCursor;
    } else {
      return null;
    }
  }

  /**
   * @see android.widget.ListAdapter#getItemId(int)
   */
  @Override
  public long getItemId(int position) {
    if (mDataValid && mCursor != null) {
      if (mCursor.moveToPosition(position)) {
        return mCursor.getLong(mRowIDColumn);
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  }

  @Override
  public boolean hasStableIds() {
    return true;
  }

  /**
   * @see android.widget.ListAdapter#getView(int, View, ViewGroup)
   */
  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    if (!mDataValid) {
      throw new IllegalStateException("this should only be called when the cursor is valid");
    }
    if (!mCursor.moveToPosition(position)) {
      throw new IllegalStateException("couldn't move cursor to position " + position);
    }
    View v;
    if (convertView == null) {
      v = newView(mContext, mCursor, parent);
    } else {
      v = convertView;
    }
    System.out.println("TEST!!2" + mCursor);

    bindView(v, mContext, mCursor);
    return v;
  }

  @Override
  public View getDropDownView(int position, View convertView, ViewGroup parent) {
    if (mDataValid) {
      mCursor.moveToPosition(position);
      View v;
      if (convertView == null) {
        v = newDropDownView(mContext, mCursor, parent);
      } else {
        v = convertView;
      }
      bindView(v, mContext, mCursor);
      return v;
    } else {
      return null;
    }
  }

  public abstract View newView(Context context, Cursor cursor, ViewGroup parent);

  public View newDropDownView(Context context, Cursor cursor, ViewGroup parent) {
    return newView(context, cursor, parent);
  }

  public abstract void bindView(View view, Context context, Cursor cursor);

  @Override
  public void changeCursor(Cursor cursor) {
    Cursor old = swapCursor(cursor);
    if (old != null) {
      old.close();
    }
  }

  public Cursor swapCursor(Cursor newCursor) {
    if (newCursor == mCursor) {
      return null;
    }
    Cursor oldCursor = mCursor;
    // if (oldCursor != null) {
    // if (mChangeObserver != null)
    // oldCursor.unregisterContentObserver(mChangeObserver);
    // if (mDataSetObserver != null)
    // oldCursor.unregisterDataSetObserver(mDataSetObserver);
    // }
    mCursor = newCursor;
    if (newCursor != null) {
      // if (mChangeObserver != null)
      // newCursor.registerContentObserver(mChangeObserver);
      // if (mDataSetObserver != null)
      // newCursor.registerDataSetObserver(mDataSetObserver);
      mRowIDColumn = newCursor.getColumnIndexOrThrow("_id");
      mDataValid = true;
      // notify the observers about the new cursor
      notifyDataSetChanged();
    } else {
      mRowIDColumn = -1;
      mDataValid = false;
      // notify the observers about the lack of a data set
      notifyDataSetInvalidated();
    }
    return oldCursor;
  }

  @Override
  public Filter getFilter() {
    if (mCursorFilter == null) {
      mCursorFilter = new CursorFilter(this);
    }
    return mCursorFilter;
  }

  public FilterQueryProvider getFilterQueryProvider() {
    return mFilterQueryProvider;
  }

  public void setFilterQueryProvider(FilterQueryProvider filterQueryProvider) {
    mFilterQueryProvider = filterQueryProvider;
  }

  protected void onContentChanged() {
    if (mAutoRequery && mCursor != null && !mCursor.isClosed()) {
      if (false)
        Log.v("Cursor", "Auto requerying " + mCursor + " due to update");
      mDataValid = mCursor.requery();
    }
  }

  // private class ChangeObserver extends ContentObserver {
  // public ChangeObserver() {
  // super(new Handler());
  // }
  //
  // @Override
  // public boolean deliverSelfNotifications() {
  // return true;
  // }
  //
  // @Override
  // public void onChange(boolean selfChange) {
  // onContentChanged();
  // }
  // }
  //
  // private class MyDataSetObserver extends DataSetObserver {
  // @Override
  // public void onChanged() {
  // mDataValid = true;
  // notifyDataSetChanged();
  // }
  //
  // @Override
  // public void onInvalidated() {
  // mDataValid = false;
  // notifyDataSetInvalidated();
  // }
  //
  // }

}