package android.widget;

import gov.nasa.jpf.vm.Abstraction;
import android.content.Context;
import android.graphics.drawable.Drawable;

public class ImageView extends android.view.View {
  public static enum ScaleType {

    MATRIX;
  }

  public static android.widget.ImageView TOP;

  public ImageView(Context c) {
    super(c);
    if (TOP == null) {
      TOP = this;
    }
  }

  public void setImageBitmap(android.graphics.Bitmap param0) {
  }

  public void setImageMatrix(android.graphics.Matrix param0) {
  }

  public android.graphics.drawable.Drawable getDrawable() {
    return new Drawable();
  }

  public void setImageDrawable(android.graphics.drawable.Drawable param0) {
  }

  @Override
  public void setVisibility(int param0) {
  }

  public void setScaleType(android.widget.ImageView.ScaleType param0) {
  }

  public android.graphics.Matrix getImageMatrix() {
    return android.graphics.Matrix.IDENTITY_MATRIX;
  }

  public android.widget.ImageView.ScaleType getScaleType() {
    return android.widget.ImageView.ScaleType.MATRIX;
  }

  public ImageView(android.content.Context param0, android.util.AttributeSet param1) {
    super(param0);
  }

  public void invalidate() {

  }

  protected boolean verifyDrawable(android.graphics.drawable.Drawable param0) {
    return Abstraction.TOP_BOOL;
  }

  public void jumpDrawablesToCurrentState() {
  }

  public void invalidateDrawable(android.graphics.drawable.Drawable param0) {
  }

  public boolean hasOverlappingRendering() {
    return Abstraction.TOP_BOOL;
  }

  public void onPopulateAccessibilityEvent(android.view.accessibility.AccessibilityEvent param0) {
  }

  public boolean getAdjustViewBounds() {
    return Abstraction.TOP_BOOL;
  }

  public void setAdjustViewBounds(boolean param0) {
  }

  public int getMaxWidth() {
    return Abstraction.TOP_INT;
  }

  public void setMaxWidth(int param0) {
  }

  public int getMaxHeight() {
    return Abstraction.TOP_INT;
  }

  public void setMaxHeight(int param0) {
  }

  // public android.graphics.drawable.Drawable getDrawable(){
  // return android.graphics.drawable.Drawable.TOP;
  // }

  public void setImageResource(int param0) {
  }

  public void setImageURI(android.net.Uri param0) {
  }

  // public void setImageDrawable(android.graphics.drawable.Drawable param0){
  // }

  public void setImageTintList(android.content.res.ColorStateList param0) {
  }

  public android.content.res.ColorStateList getImageTintList() {
    return ((android.content.res.ColorStateList) Abstraction
        .randomObject("android.content.res.ColorStateList"));
  }

  public void setImageTintMode(android.graphics.PorterDuff.Mode param0) {
  }

  public android.graphics.PorterDuff.Mode getImageTintMode() {
    return ((android.graphics.PorterDuff.Mode) Abstraction.randomObject("android.graphics.PorterDuff.Mode"));
  }

  // public void setImageBitmap(android.graphics.Bitmap param0){
  // }

  public void setImageState(int[] param0, boolean param1) {
  }

  public void setSelected(boolean param0) {
  }

  public void setImageLevel(int param0) {
  }

  // public void setScaleType(android.widget.ImageView.ScaleType param0){
  // }
  //
  // public android.widget.ImageView.ScaleType getScaleType(){
  // return
  // ((android.widget.ImageView.ScaleType)Abstraction.randomObject("android.widget.ImageView.ScaleType"));
  // }

  // public android.graphics.Matrix getImageMatrix(){
  // return null;
  // }

  // public void setImageMatrix(android.graphics.Matrix param0){
  // }

  public boolean getCropToPadding() {
    return Abstraction.TOP_BOOL;
  }

  public void setCropToPadding(boolean param0) {
  }

  public int[] onCreateDrawableState(int param0) {
    return ((int[]) Abstraction.randomObject("int[]"));
  }

  public void onRtlPropertiesChanged(int param0) {
  }

  protected void onMeasure(int param0, int param1) {
  }

  protected boolean setFrame(int param0, int param1, int param2, int param3) {
    return Abstraction.TOP_BOOL;
  }

  protected void drawableStateChanged() {
  }

  public void drawableHotspotChanged(float param0, float param1) {
  }

  @Override
  protected void onDraw(android.graphics.Canvas param0) {
  }

  public int getBaseline() {
    return Abstraction.TOP_INT;
  }

  public void setBaseline(int param0) {
  }

  public void setBaselineAlignBottom(boolean param0) {
  }

  public boolean getBaselineAlignBottom() {
    return Abstraction.TOP_BOOL;
  }

  public final void setColorFilter(int param0, android.graphics.PorterDuff.Mode param1) {
  }

  public final void setColorFilter(int param0) {
  }

  public final void clearColorFilter() {
  }

  public android.graphics.ColorFilter getColorFilter() {
    return null;
  }

  public void setColorFilter(android.graphics.ColorFilter param0) {
  }

  public int getImageAlpha() {
    return Abstraction.TOP_INT;
  }

  public void setImageAlpha(int param0) {
  }

  public void setAlpha(int param0) {
  }

  public boolean isOpaque() {
    return Abstraction.TOP_BOOL;
  }

  // public void setVisibility(int param0){
  // }

  protected void onAttachedToWindow() {
  }

  protected void onDetachedFromWindow() {
  }

  public void onInitializeAccessibilityEvent(android.view.accessibility.AccessibilityEvent param0) {
  }

  public void onInitializeAccessibilityNodeInfo(android.view.accessibility.AccessibilityNodeInfo param0) {
  }

  public void setContentDescription(CharSequence disc) {

  }
}