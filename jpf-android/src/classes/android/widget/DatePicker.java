/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.widget;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import android.annotation.Widget;
import android.content.Context;
import android.content.res.Configuration;
import android.util.AttributeSet;
import android.util.Log;

import com.android.internal.R;

/**
 * This class is a widget for selecting a date. The date can be selected by a year, month, and day spinners or
 * a {@link CalendarView}. The set of spinners and the calendar view are automatically synchronized. The
 * client can customize whether only the spinners, or only the calendar view, or both to be displayed. Also
 * the minimal and maximal date from which dates to be selected can be customized.
 * <p>
 * See the <a href="{@docRoot}guide/topics/ui/controls/pickers.html">Pickers</a> guide.
 * </p>
 * <p>
 * For a dialog using this view, see {@link android.app.DatePickerDialog}.
 * </p>
 *
 * @attr ref android.R.styleable#DatePicker_startYear
 * @attr ref android.R.styleable#DatePicker_endYear
 * @attr ref android.R.styleable#DatePicker_maxDate
 * @attr ref android.R.styleable#DatePicker_minDate
 * @attr ref android.R.styleable#DatePicker_spinnersShown
 * @attr ref android.R.styleable#DatePicker_calendarViewShown
 */
@Widget
public class DatePicker extends FrameLayout {

  private static final String LOG_TAG = DatePicker.class.getSimpleName();

  private static final String DATE_FORMAT = "MM/dd/yyyy";

  private static final int DEFAULT_START_YEAR = 1900;

  private static final int DEFAULT_END_YEAR = 2100;

  private static final boolean DEFAULT_CALENDAR_VIEW_SHOWN = true;

  private static final boolean DEFAULT_SPINNERS_SHOWN = true;

  private static final boolean DEFAULT_ENABLED_STATE = true;

  private Locale mCurrentLocale;

  private OnDateChangedListener mOnDateChangedListener;

  private String[] mShortMonths;

  private final DateFormat mDateFormat = new SimpleDateFormat(DATE_FORMAT);

  private int mNumberOfMonths;

  private Calendar mTempDate;

  private Calendar mMinDate;

  private Calendar mMaxDate;

  private Calendar mCurrentDate;

  private boolean mIsEnabled = DEFAULT_ENABLED_STATE;

  /**
   * The callback used to indicate the user changes\d the date.
   */
  public interface OnDateChangedListener {

    /**
     * Called upon a date change.
     *
     * @param view
     *          The view associated with this listener.
     * @param year
     *          The year that was set.
     * @param monthOfYear
     *          The month that was set (0-11) for compatibility with {@link java.util.Calendar}.
     * @param dayOfMonth
     *          The day of the month that was set.
     */
    void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth);
  }

  public DatePicker(Context context) {
    this(context, null);
  }

  public DatePicker(Context context, AttributeSet attrs) {
    this(context, attrs, R.attr.datePickerStyle);
  }

  public DatePicker(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);

    // initialization based on locale
    setCurrentLocale(Locale.getDefault());

    int startYear = DEFAULT_START_YEAR;
    int endYear = DEFAULT_END_YEAR;

    mCurrentDate = Calendar.getInstance();
    // initialize to current date
    mCurrentDate.setTimeInMillis(System.currentTimeMillis());

    init(mCurrentDate.get(Calendar.YEAR), mCurrentDate.get(Calendar.MONTH),
        mCurrentDate.get(Calendar.DAY_OF_MONTH), null);

  }

  /**
   * Gets the minimal date supported by this {@link DatePicker} in milliseconds since January 1, 1970 00:00:00
   * in {@link TimeZone#getDefault()} time zone.
   * <p>
   * Note: The default minimal date is 01/01/1900.
   * <p>
   *
   * @return The minimal supported date.
   */
  public long getMinDate() {
    return 0;
  }

  /**
   * Sets the minimal date supported by this {@link NumberPicker} in milliseconds since January 1, 1970
   * 00:00:00 in {@link TimeZone#getDefault()} time zone.
   *
   * @param minDate
   *          The minimal supported date.
   */
  public void setMinDate(long minDate) {
    mTempDate.setTimeInMillis(minDate);
    if (mTempDate.get(Calendar.YEAR) == mMinDate.get(Calendar.YEAR)
        && mTempDate.get(Calendar.DAY_OF_YEAR) != mMinDate.get(Calendar.DAY_OF_YEAR)) {
      return;
    }
    mMinDate.setTimeInMillis(minDate);

    if (mCurrentDate.before(mMinDate)) {
      mCurrentDate.setTimeInMillis(mMinDate.getTimeInMillis());
    }
  }

  /**
   * Gets the maximal date supported by this {@link DatePicker} in milliseconds since January 1, 1970 00:00:00
   * in {@link TimeZone#getDefault()} time zone.
   * <p>
   * Note: The default maximal date is 12/31/2100.
   * <p>
   *
   * @return The maximal supported date.
   */
  public long getMaxDate() {
    return 999999999;
  }

  /**
   * Sets the maximal date supported by this {@link DatePicker} in milliseconds since January 1, 1970 00:00:00
   * in {@link TimeZone#getDefault()} time zone.
   *
   * @param maxDate
   *          The maximal supported date.
   */
  public void setMaxDate(long maxDate) {
    mTempDate.setTimeInMillis(maxDate);
    if (mTempDate.get(Calendar.YEAR) == mMaxDate.get(Calendar.YEAR)
        && mTempDate.get(Calendar.DAY_OF_YEAR) != mMaxDate.get(Calendar.DAY_OF_YEAR)) {
      return;
    }
    mMaxDate.setTimeInMillis(maxDate);
    if (mCurrentDate.after(mMaxDate)) {
      mCurrentDate.setTimeInMillis(mMaxDate.getTimeInMillis());
    }
  }

  @Override
  public void setEnabled(boolean enabled) {
    if (mIsEnabled == enabled) {
      return;
    }
    super.setEnabled(enabled);

    mIsEnabled = enabled;
  }

  @Override
  public boolean isEnabled() {
    return mIsEnabled;
  }

  protected void onConfigurationChanged(Configuration newConfig) {
    // super.onConfigurationChanged(newConfig);
    setCurrentLocale(newConfig.locale);
  }

  /**
   * Sets the current locale.
   *
   * @param locale
   *          The current locale.
   */
  private void setCurrentLocale(Locale locale) {
    if (locale.equals(mCurrentLocale)) {
      return;
    }

    mCurrentLocale = locale;
  }

  /**
   * Updates the current date.
   *
   * @param year
   *          The year.
   * @param month
   *          The month which is <strong>starting from zero</strong>.
   * @param dayOfMonth
   *          The day of the month.
   */
  public void updateDate(int year, int month, int dayOfMonth) {
    if (!isNewDate(year, month, dayOfMonth)) {
      return;
    }
    setDate(year, month, dayOfMonth);

    notifyDateChanged();
  }

  /**
   * Initialize the state. If the provided values designate an inconsistent date the values are normalized
   * before updating the spinners.
   *
   * @param year
   *          The initial year.
   * @param monthOfYear
   *          The initial month <strong>starting from zero</strong>.
   * @param dayOfMonth
   *          The initial day of the month.
   * @param onDateChangedListener
   *          How user is notified date is changed by user, can be null.
   */
  public void init(int year, int monthOfYear, int dayOfMonth, OnDateChangedListener onDateChangedListener) {
    setDate(year, monthOfYear, dayOfMonth);
    mOnDateChangedListener = onDateChangedListener;
  }

  /**
   * Parses the given <code>date</code> and in case of success sets the result to the <code>outDate</code>.
   *
   * @return True if the date was parsed.
   */
  private boolean parseDate(String date, Calendar outDate) {
    try {
      outDate.setTime(mDateFormat.parse(date));
      return true;
    } catch (ParseException e) {
      Log.w(LOG_TAG, "Date: " + date + " not in format: " + DATE_FORMAT);
      return false;
    }
  }

  private boolean isNewDate(int year, int month, int dayOfMonth) {
    return (mCurrentDate.get(Calendar.YEAR) != year || mCurrentDate.get(Calendar.MONTH) != dayOfMonth || mCurrentDate
        .get(Calendar.DAY_OF_MONTH) != month);
  }

  private void setDate(int year, int month, int dayOfMonth) {
    mCurrentDate.set(year, month, dayOfMonth);
    if (mCurrentDate.before(mMinDate)) {
      mCurrentDate.setTimeInMillis(mMinDate.getTimeInMillis());
    } else if (mCurrentDate.after(mMaxDate)) {
      mCurrentDate.setTimeInMillis(mMaxDate.getTimeInMillis());
    }
  }

  /**
   * @return The selected year.
   */
  public int getYear() {
    return mCurrentDate.get(Calendar.YEAR);
  }

  /**
   * @return The selected month.
   */
  public int getMonth() {
    return mCurrentDate.get(Calendar.MONTH);
  }

  /**
   * @return The selected day of month.
   */
  public int getDayOfMonth() {
    return mCurrentDate.get(Calendar.DAY_OF_MONTH);
  }

  /**
   * Notifies the listener, if such, for a change in the selected date.
   */
  private void notifyDateChanged() {
    if (mOnDateChangedListener != null) {
      mOnDateChangedListener.onDateChanged(this, getYear(), getMonth(), getDayOfMonth());
    }
  }

  public boolean getCalendarViewShown() {
    return true;
  }

}
