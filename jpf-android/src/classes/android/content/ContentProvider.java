package android.content;

import gov.nasa.jpf.vm.Abstraction;
import gov.nasa.jpf.vm.Verify;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

import android.content.pm.ProviderInfo;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ICancellationSignal;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.util.Log;

public class ContentProvider implements android.content.ComponentCallbacks2 {
  private static final String TAG = "ContentProvider";

  private android.content.Context mContext;
  private java.lang.String mReadPermission;
  private java.lang.String mWritePermission;
  private boolean mExported;
  public static android.content.ContentProvider TOP = new android.content.ContentProvider();

  /**
   * IContentProvider that directs all calls to this MockContentProvider.
   */
  private class InversionIContentProvider implements IContentProvider {
    @Override
    public ContentProviderResult[] applyBatch(String callingPackage,
                                              ArrayList<ContentProviderOperation> operations)
        throws RemoteException, OperationApplicationException {
      return ContentProvider.this.applyBatch(operations);
    }

    @Override
    public int bulkInsert(String callingPackage, Uri url, ContentValues[] initialValues)
        throws RemoteException {
      return ContentProvider.this.bulkInsert(url, initialValues);
    }

    @Override
    public int delete(String callingPackage, Uri url, String selection, String[] selectionArgs)
        throws RemoteException {
      return ContentProvider.this.delete(url, selection, selectionArgs);
    }

    @Override
    public String getType(Uri url) throws RemoteException {
      return ContentProvider.this.getType(url);
    }

    @Override
    public Uri insert(String callingPackage, Uri url, ContentValues initialValues) throws RemoteException {
      return ContentProvider.this.insert(url, initialValues);
    }

    @Override
    public AssetFileDescriptor openAssetFile(String callingPackage, Uri url, String mode,
                                             ICancellationSignal signal) throws RemoteException,
        FileNotFoundException {
      return ContentProvider.this.openAssetFile(url, mode);
    }

    @Override
    public ParcelFileDescriptor openFile(String callingPackage, Uri url, String mode,
                                         ICancellationSignal signal) throws RemoteException,
        FileNotFoundException {
      return ContentProvider.this.openFile(url, mode);
    }

    @Override
    public Cursor query(String callingPackage, Uri url, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder, ICancellationSignal cancellationSignal)
        throws RemoteException {
      return ContentProvider.this.query(url, projection, selection, selectionArgs, sortOrder);
    }

    @Override
    public int update(String callingPackage, Uri url, ContentValues values, String selection,
                      String[] selectionArgs) throws RemoteException {
      return ContentProvider.this.update(url, values, selection, selectionArgs);
    }

    @Override
    public Bundle call(String callingPackage, String method, String request, Bundle args)
        throws RemoteException {
      return ContentProvider.this.call(method, request, args);
    }

    @Override
    public IBinder asBinder() {
      throw new UnsupportedOperationException();
    }

    @Override
    public String[] getStreamTypes(Uri url, String mimeTypeFilter) throws RemoteException {
      return ContentProvider.this.getStreamTypes(url, mimeTypeFilter);
    }

    @Override
    public AssetFileDescriptor openTypedAssetFile(String callingPackage, Uri url, String mimeType,
                                                  Bundle opts, ICancellationSignal signal)
        throws RemoteException, FileNotFoundException {
      return ContentProvider.this.openTypedAssetFile(url, mimeType, opts);
    }

    @Override
    public ICancellationSignal createCancellationSignal() throws RemoteException {
      return null;
    }

    @Override
    public Uri canonicalize(String callingPkg, Uri uri) throws RemoteException {
      return ContentProvider.this.canonicalize(uri);
    }

    @Override
    public Uri uncanonicalize(String callingPkg, Uri uri) throws RemoteException {
      return ContentProvider.this.uncanonicalize(uri);
    }
  }

  private final InversionIContentProvider mIContentProvider = new InversionIContentProvider();

  public ContentProvider() {
  }

  public ContentProvider(android.content.Context mContext, java.lang.String param1, java.lang.String param2,
      android.content.pm.PathPermission[] param3) {
    this.mContext = mContext;
  }

  public android.content.Context getContext() {
    return this.mContext;
  }

  private java.lang.String setCallingPackage(java.lang.String param0) {
    // no may se;
    return ((java.lang.String) Verify.randomObject("java.lang.String"));
  }

  public java.lang.String getCallingPackage() {
    // no may se;
    return ((java.lang.String) Verify.randomObject("java.lang.String"));
  }

  protected void setReadPermission(java.lang.String param0) {
    // no may se;
  }

  public java.lang.String getReadPermission() {
    // no may se;
    return this.mReadPermission;
  }

  protected void setWritePermission(java.lang.String param0) {
    // no may se;
  }

  public java.lang.String getWritePermission() {
    // no may se;
    return this.mWritePermission;
  }

  protected void setPathPermissions(android.content.pm.PathPermission[] param0) {
    // no may se;
  }

  public android.content.pm.PathPermission[] getPathPermissions() {
    // no may se;
    return ((android.content.pm.PathPermission[]) Verify.randomObject("android.content.pm.PathPermission[]"));
  }

  public android.app.AppOpsManager getAppOpsManager() {
    // no may se;
    return ((android.app.AppOpsManager) Verify.randomObject("android.app.AppOpsManager"));
  }

  public boolean onCreate() {
    // no may se;
    return Abstraction.TOP_BOOL;
  }

  @Override
  public void onConfigurationChanged(android.content.res.Configuration param0) {
    // no may se;
  }

  @Override
  public void onLowMemory() {
    // no may se;
  }

  @Override
  public void onTrimMemory(int param0) {
    // no may se;
  }

  public android.database.Cursor rejectQuery(android.net.Uri param0, java.lang.String[] param1,
                                             java.lang.String param2, java.lang.String[] param3,
                                             java.lang.String param4, android.os.CancellationSignal param5) {
    // no may se;
    return ((android.database.Cursor) Verify.randomObject("android.database.Cursor"));
  }

  public android.database.Cursor query(android.net.Uri param0, java.lang.String[] param1,
                                       java.lang.String param2, java.lang.String[] param3,
                                       java.lang.String param4) {
    // no may se;
    return ((android.database.Cursor) Verify.randomObject("android.database.Cursor"));
  }

  public android.database.Cursor query(android.net.Uri param0, java.lang.String[] param1,
                                       java.lang.String param2, java.lang.String[] param3,
                                       java.lang.String param4, android.os.CancellationSignal param5) {
    // no may se;
    return ((android.database.Cursor) Verify.randomObject("android.database.Cursor"));
  }

  public java.lang.String getType(android.net.Uri param0) {
    // no may se;
    return Abstraction.TOP_STRING;
  }

  public android.net.Uri canonicalize(android.net.Uri param0) {
    // no may se;
    return ((android.net.Uri) Verify.randomObject("android.net.Uri"));
  }

  public android.net.Uri uncanonicalize(android.net.Uri param0) {
    // no may se;
    return ((android.net.Uri) Verify.randomObject("android.net.Uri"));
  }

  public android.net.Uri rejectInsert(android.net.Uri param0, android.content.ContentValues param1) {
    // no may se;
    return ((android.net.Uri) Verify.randomObject("android.net.Uri"));
  }

  public android.net.Uri insert(android.net.Uri param0, android.content.ContentValues param1) {
    // no may se;
    return ((android.net.Uri) Verify.randomObject("android.net.Uri"));
  }

  public int bulkInsert(android.net.Uri param0, android.content.ContentValues[] param1) {
    return Abstraction.TOP_INT;
  }

  public int delete(android.net.Uri param0, java.lang.String param1, java.lang.String[] param2) {
    return Abstraction.TOP_INT;
  }

  public int update(android.net.Uri param0, android.content.ContentValues param1, java.lang.String param2,
                    java.lang.String[] param3) {
    return Abstraction.TOP_INT;
  }

  public android.os.ParcelFileDescriptor openFile(android.net.Uri param0, java.lang.String param1)
      throws java.io.FileNotFoundException {
    return null;
  }

  public android.os.ParcelFileDescriptor openFile(android.net.Uri param0, java.lang.String param1,
                                                  android.os.CancellationSignal param2)
      throws java.io.FileNotFoundException {
    return null;
  }

  public android.content.res.AssetFileDescriptor openAssetFile(android.net.Uri param0, java.lang.String param1)
      throws java.io.FileNotFoundException {
    return null;
  }

  public android.content.res.AssetFileDescriptor openAssetFile(android.net.Uri param0,
                                                               java.lang.String param1,
                                                               android.os.CancellationSignal param2)
      throws java.io.FileNotFoundException {
    return null;
  }

  protected android.os.ParcelFileDescriptor openFileHelper(android.net.Uri param0, java.lang.String param1)
      throws java.io.FileNotFoundException {
    return null;
  }

  public java.lang.String[] getStreamTypes(android.net.Uri param0, java.lang.String param1) {
    return null;
    // no may se;
  }

  public android.content.res.AssetFileDescriptor openTypedAssetFile(android.net.Uri param0,
                                                                    java.lang.String param1,
                                                                    android.os.Bundle param2)
      throws java.io.FileNotFoundException {
    return null;
  }

  public android.content.res.AssetFileDescriptor openTypedAssetFile(android.net.Uri param0,
                                                                    java.lang.String param1,
                                                                    android.os.Bundle param2,
                                                                    android.os.CancellationSignal param3)
      throws java.io.FileNotFoundException {
    return null;
  }

  // public android.os.ParcelFileDescriptor openPipeHelper(android.net.Uri param0, java.lang.String param1,
  // android.os.Bundle param2, java.lang.Object param3, android.content.ContentProvider$PipeDataWriter param4)
  // throws java.io.FileNotFoundException {
  // // no may se;
  // return ((android.os.ParcelFileDescriptor)Verify.randomObject("android.os.ParcelFileDescriptor"));
  // }

  protected boolean isTemporary() {
    // no may se;
    return false;
  }

  public void attachInfo(android.content.Context context, android.content.pm.ProviderInfo info) {
    attachInfo(context, info, false);
  }

  private void attachInfo(Context context, ProviderInfo info, boolean testing) {

    /*
     * Only allow it to be set once, so after the content service gives this to us clients can't change it.
     */
    if (mContext == null) {
      mContext = context;
      if (info != null) {
        setReadPermission(info.readPermission);
        setWritePermission(info.writePermission);
        setPathPermissions(info.pathPermissions);
        mExported = info.exported;
      }
      ContentProvider.this.onCreate();
    }
  }

  public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> operations)
      throws android.content.OperationApplicationException {
    return null;
  }

  public Bundle call(String method, String arg, Bundle extras) {
    return null;
  }

  public void shutdown() {
    Log.w(TAG, "implement ContentProvider shutdown() to make sure all database "
        + "connections are gracefully shutdown");
  }

  public void dump(FileDescriptor fd, PrintWriter writer, String[] args) {
    writer.println("nothing to dump");
  }

}