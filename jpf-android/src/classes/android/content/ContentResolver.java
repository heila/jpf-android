package android.content;

import gov.nasa.jpf.vm.Abstraction;
import gov.nasa.jpf.vm.Verify;
import android.app.ActivityThread;
import android.database.Cursor;
import android.net.Uri;
import android.os.CancellationSignal;

public abstract class ContentResolver {

  /**
   * @deprecated instead use
   *             {@link #requestSync(android.accounts.Account, String, android.os.Bundle)}
   */
  @Deprecated
  public static final String SYNC_EXTRAS_ACCOUNT = "account";

  /**
   * If this extra is set to true, the sync request will be scheduled at the
   * front of the sync request queue and without any delay
   */
  public static final String SYNC_EXTRAS_EXPEDITED = "expedited";

  /**
   * @deprecated instead use {@link #SYNC_EXTRAS_MANUAL}
   */
  @Deprecated
  public static final String SYNC_EXTRAS_FORCE = "force";

  /**
   * If this extra is set to true then the sync settings (like
   * getSyncAutomatically()) are ignored by the sync scheduler.
   */
  public static final String SYNC_EXTRAS_IGNORE_SETTINGS = "ignore_settings";

  /**
   * If this extra is set to true then any backoffs for the initial attempt
   * (e.g. due to retries) are ignored by the sync scheduler. If this request
   * fails and gets rescheduled then the retries will still honor the backoff.
   */
  public static final String SYNC_EXTRAS_IGNORE_BACKOFF = "ignore_backoff";

  /**
   * If this extra is set to true then the request will not be retried if it
   * fails.
   */
  public static final String SYNC_EXTRAS_DO_NOT_RETRY = "do_not_retry";

  /**
   * Setting this extra is the equivalent of setting both
   * {@link #SYNC_EXTRAS_IGNORE_SETTINGS} and
   * {@link #SYNC_EXTRAS_IGNORE_BACKOFF}
   */
  public static final String SYNC_EXTRAS_MANUAL = "force";

  /**
   * Indicates that this sync is intended to only upload local changes to the
   * server. For example, this will be set to true if the sync is initiated by a
   * call to
   * {@link ContentResolver#notifyChange(android.net.Uri, android.database.ContentObserver, boolean)}
   */
  public static final String SYNC_EXTRAS_UPLOAD = "upload";

  /**
   * Indicates that the sync adapter should proceed with the delete operations,
   * even if it determines that there are too many. See
   * {@link SyncResult#tooManyDeletions}
   */
  public static final String SYNC_EXTRAS_OVERRIDE_TOO_MANY_DELETIONS = "deletions_override";

  /**
   * Indicates that the sync adapter should not proceed with the delete
   * operations, if it determines that there are too many. See
   * {@link SyncResult#tooManyDeletions}
   */
  public static final String SYNC_EXTRAS_DISCARD_LOCAL_DELETIONS = "discard_deletions";

  /**
   * Set by the SyncManager to request that the SyncAdapter initialize itself
   * for the given account/authority pair. One required initialization step is
   * to ensure that
   * {@link #setIsSyncable(android.accounts.Account, String, int)} has been
   * called with a >= 0 value. When this flag is set the SyncAdapter does not
   * need to do a full sync, though it is allowed to do so.
   */
  public static final String SYNC_EXTRAS_INITIALIZE = "initialize";

  public static final String SCHEME_CONTENT = "content";
  public static final String SCHEME_ANDROID_RESOURCE = "android.resource";
  public static final String SCHEME_FILE = "file";

  /**
   * This is the Android platform's base MIME type for a content: URI containing
   * a Cursor of a single item. Applications should use this as the base type
   * along with their own sub-type of their content: URIs that represent a
   * particular item. For example, hypothetical IMAP email client may have a URI
   * <code>content://com.company.provider.imap/inbox/1</code> for a particular
   * message in the inbox, whose MIME type would be reported as
   * <code>CURSOR_ITEM_BASE_TYPE + "/vnd.company.imap-msg"</code>
   *
   * <p>
   * Compare with {@link #CURSOR_DIR_BASE_TYPE}.
   */
  public static final String CURSOR_ITEM_BASE_TYPE = "vnd.android.cursor.item";

  /**
   * This is the Android platform's base MIME type for a content: URI containing
   * a Cursor of zero or more items. Applications should use this as the base
   * type along with their own sub-type of their content: URIs that represent a
   * directory of items. For example, hypothetical IMAP email client may have a
   * URI <code>content://com.company.provider.imap/inbox</code> for all of the
   * messages in its inbox, whose MIME type would be reported as
   * <code>CURSOR_DIR_BASE_TYPE + "/vnd.company.imap-msg"</code>
   *
   * <p>
   * Note how the base MIME type varies between this and
   * {@link #CURSOR_ITEM_BASE_TYPE} depending on whether there is one single
   * item or multiple items in the data set, while the sub-type remains the same
   * because in either case the data structure contained in the cursor is the
   * same.
   */
  public static final String CURSOR_DIR_BASE_TYPE = "vnd.android.cursor.dir";

  private static final String[] SYNC_ERROR_NAMES = new String[] { "already-in-progress",
      "authentication-error", "io-error", "parse-error", "conflict", "too-many-deletions",
      "too-many-retries", "internal-error", };

  public static final int SYNC_OBSERVER_TYPE_SETTINGS = 1 << 0;
  public static final int SYNC_OBSERVER_TYPE_PENDING = 1 << 1;
  public static final int SYNC_OBSERVER_TYPE_ACTIVE = 1 << 2;

  // Always log queries which take 500ms+; shorter queries are
  // sampled accordingly.
  private static final boolean ENABLE_CONTENT_SAMPLE = false;
  private static final int SLOW_THRESHOLD_MILLIS = 500;

  public ContentResolver(Context context) {
    mContext = context != null ? context : ActivityThread.currentApplication();
    mPackageName = mContext.getOpPackageName();
  }

  protected android.content.Context mContext;
  java.lang.String mPackageName;

  public ContentResolver() {
    // no may se;
  }

  public final java.lang.String getType(android.net.Uri param0) {
    return "";
  }

  public java.lang.String[] getStreamTypes(android.net.Uri param0, java.lang.String param1) {
    return null;
  }

  public final Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                            String sortOrder) {
    return query(uri, projection, selection, selectionArgs, sortOrder, null);
  }

  public final Cursor query(final Uri uri, String[] projection, String selection,
                            String[] selectionArgs, String sortOrder,
                            CancellationSignal cancellationSignal) {
    ContentProvider cp = acquireProvider(uri);

    return cp.query(uri, projection, selection, selectionArgs, sortOrder, cancellationSignal);
  }

  public final ContentProvider acquireUnstableProvider(Uri uri) {
    if (!SCHEME_CONTENT.equals(uri.getScheme())) {
      return null;
    }
    String auth = uri.getAuthority();
    if (auth != null) {
      return acquireProvider(mContext, uri.getAuthority());
    }
    return null;
  }

  public final android.net.Uri canonicalize(android.net.Uri param0) {
    // no may se;
    return ((android.net.Uri) Verify.randomObject("android.net.Uri"));
  }

  public final android.net.Uri uncanonicalize(android.net.Uri param0) {
    // no may se;
    return ((android.net.Uri) Verify.randomObject("android.net.Uri"));
  }

  public final java.io.InputStream openInputStream(android.net.Uri param0)
      throws java.io.FileNotFoundException {
    // no may se;
    return ((java.io.InputStream) Verify.randomObject("java.io.InputStream"));
  }

  public final java.io.OutputStream openOutputStream(android.net.Uri param0)
      throws java.io.FileNotFoundException {
    // no may se;
    return ((java.io.OutputStream) Verify.randomObject("java.io.OutputStream"));
  }

  public final java.io.OutputStream openOutputStream(android.net.Uri param0, java.lang.String param1)
      throws java.io.FileNotFoundException {
    // no may se;
    return ((java.io.OutputStream) Verify.randomObject("java.io.OutputStream"));
  }

  public final android.os.ParcelFileDescriptor openFileDescriptor(android.net.Uri param0,
                                                                  java.lang.String param1)
      throws java.io.FileNotFoundException {
    // no may se;
    return ((android.os.ParcelFileDescriptor) Verify
        .randomObject("android.os.ParcelFileDescriptor"));
  }

  public final android.os.ParcelFileDescriptor openFileDescriptor(android.net.Uri param0,
                                                                  java.lang.String param1,
                                                                  android.os.CancellationSignal param2)
      throws java.io.FileNotFoundException {
    // no may se;
    return ((android.os.ParcelFileDescriptor) Verify
        .randomObject("android.os.ParcelFileDescriptor"));
  }

  public final android.content.res.AssetFileDescriptor openAssetFileDescriptor(android.net.Uri param0,
                                                                               java.lang.String param1)
      throws java.io.FileNotFoundException {
    // no may se;
    return ((android.content.res.AssetFileDescriptor) Verify
        .randomObject("android.content.res.AssetFileDescriptor"));
  }

  public final android.content.res.AssetFileDescriptor openAssetFileDescriptor(android.net.Uri param0,
                                                                               java.lang.String param1,
                                                                               android.os.CancellationSignal param2)
      throws java.io.FileNotFoundException {
    // no may se;
    return ((android.content.res.AssetFileDescriptor) Verify
        .randomObject("android.content.res.AssetFileDescriptor"));
  }

  public final android.content.res.AssetFileDescriptor openTypedAssetFileDescriptor(android.net.Uri param0,
                                                                                    java.lang.String param1,
                                                                                    android.os.Bundle param2)
      throws java.io.FileNotFoundException {
    // no may se;
    return ((android.content.res.AssetFileDescriptor) Verify
        .randomObject("android.content.res.AssetFileDescriptor"));
  }

  public final android.content.res.AssetFileDescriptor openTypedAssetFileDescriptor(android.net.Uri param0,
                                                                                    java.lang.String param1,
                                                                                    android.os.Bundle param2,
                                                                                    android.os.CancellationSignal param3)
      throws java.io.FileNotFoundException {
    // no may se;
    return ((android.content.res.AssetFileDescriptor) Verify
        .randomObject("android.content.res.AssetFileDescriptor"));
  }

  // public android.content.ContentResolver.OpenResourceIdResult
  // getResourceId(android.net.Uri param0)
  // throws java.io.FileNotFoundException {
  // // no may se;
  // return ((android.content.ContentResolver.OpenResourceIdResult) Verify
  // .randomObject("android.content.ContentResolver$OpenResourceIdResult"));
  // }

  public final android.net.Uri insert(android.net.Uri uri, android.content.ContentValues values) {
    ContentProvider provider = acquireProvider(uri);
    if (provider == null) {
      throw new IllegalArgumentException("Unknown URL " + uri);
    }
    Uri createdRow = provider.insert(uri, values);
    return createdRow;
  }

  public android.content.ContentProviderResult[] applyBatch(java.lang.String param0,
                                                            java.util.ArrayList param1)
      throws android.os.RemoteException, android.content.OperationApplicationException {
    // no may se;
    return ((android.content.ContentProviderResult[]) Verify
        .randomObject("android.content.ContentProviderResult[]"));
  }

  public final int bulkInsert(android.net.Uri param0, android.content.ContentValues[] param1) {
    // no may se;
    return Abstraction.TOP_INT;
  }

  public final int delete(android.net.Uri param0, java.lang.String param1, java.lang.String[] param2) {
    // no may se;
    return Abstraction.TOP_INT;
  }

  public final int update(android.net.Uri param0, android.content.ContentValues param1,
                          java.lang.String param2, java.lang.String[] param3) {
    // no may se;
    return Abstraction.TOP_INT;
  }

  public final android.os.Bundle call(android.net.Uri param0, java.lang.String param1,
                                      java.lang.String param2, android.os.Bundle param3) {
    // no may se;
    return ((android.os.Bundle) Verify.randomObject("android.os.Bundle"));
  }

  public abstract ContentProvider acquireProvider(android.net.Uri param0);

  public final android.content.ContentProvider acquireExistingProvider(android.net.Uri param0) {
    return null;
  }

  public abstract android.content.ContentProvider acquireProvider(java.lang.String param0);

  public final android.content.IContentProvider acquireUnstableProvider(java.lang.String param0) {
    return null;
  }

  public final android.content.ContentProviderClient acquireContentProviderClient(android.net.Uri param0) {
    return null;
  }

  public final android.content.ContentProviderClient acquireContentProviderClient(java.lang.String param0) {
    return null;
  }

  public final android.content.ContentProviderClient acquireUnstableContentProviderClient(android.net.Uri param0) {
    return null;
  }

  public final android.content.ContentProviderClient acquireUnstableContentProviderClient(java.lang.String param0) {
    return null;
  }

  public final void registerContentObserver(android.net.Uri param0, boolean param1,
                                            android.database.ContentObserver param2) {
  }

  public final void registerContentObserver(android.net.Uri param0, boolean param1,
                                            android.database.ContentObserver param2, int param3) {
  }

  public final void unregisterContentObserver(android.database.ContentObserver param0) {
  }

  public void notifyChange(android.net.Uri param0, android.database.ContentObserver param1) {
  }

  public void notifyChange(android.net.Uri param0, android.database.ContentObserver param1,
                           boolean param2) {
  }

  public void notifyChange(android.net.Uri param0, android.database.ContentObserver param1,
                           boolean param2, int param3) {
  }

  public void takePersistableUriPermission(android.net.Uri param0, int param1) {
  }

  public void releasePersistableUriPermission(android.net.Uri param0, int param1) {
  }

  public java.util.List getPersistedUriPermissions() {
    return null;
  }

  public java.util.List getOutgoingPersistedUriPermissions() {
    return null;
  }

  public void startSync(android.net.Uri param0, android.os.Bundle param1) {
  }

  public static void requestSync(android.accounts.Account param0, java.lang.String param1,
                                 android.os.Bundle param2) {
  }

  public static void requestSync(android.content.SyncRequest param0) {
  }

  public static void validateSyncExtrasBundle(android.os.Bundle param0) {
    // no may se;
  }

  public void cancelSync(android.net.Uri param0) {
  }

  public static void cancelSync(android.accounts.Account param0, java.lang.String param1) {
  }

  public static android.content.SyncAdapterType[] getSyncAdapterTypes() {
    return new SyncAdapterType[] { new SyncAdapterType() };
  }

  public static boolean getSyncAutomatically(android.accounts.Account param0,
                                             java.lang.String param1) {
    return false;
  }

  public static void setSyncAutomatically(android.accounts.Account param0, java.lang.String param1,
                                          boolean param2) {
  }

  public static void addPeriodicSync(android.accounts.Account param0, java.lang.String param1,
                                     android.os.Bundle param2, long param3) {
  }

  public static void removePeriodicSync(android.accounts.Account param0, java.lang.String param1,
                                        android.os.Bundle param2) {
  }

  public static java.util.List getPeriodicSyncs(android.accounts.Account param0,
                                                java.lang.String param1) {
    return null;
  }

  public static int getIsSyncable(android.accounts.Account param0, java.lang.String param1) {
    return Abstraction.TOP_INT;
  }

  public static void setIsSyncable(android.accounts.Account param0, java.lang.String param1,
                                   int param2) {
  }

  public static boolean getMasterSyncAutomatically() {
    return Abstraction.TOP_BOOL;
  }

  public static void setMasterSyncAutomatically(boolean param0) {
  }

  public static boolean isSyncActive(android.accounts.Account param0, java.lang.String param1) {
    return Abstraction.TOP_BOOL;
  }

  public static android.content.SyncInfo getCurrentSync() {
    return null;
  }

  public static java.util.List getCurrentSyncs() {
    return null;
  }

  public static android.content.SyncStatusInfo getSyncStatus(android.accounts.Account param0,
                                                             java.lang.String param1) {
    return null;
  }

  public static boolean isSyncPending(android.accounts.Account param0, java.lang.String param1) {
    return Abstraction.TOP_BOOL;
  }

  public static java.lang.Object addStatusChangeListener(int param0,
                                                         android.content.SyncStatusObserver param1) {
    return param1;
  }

  public static void removeStatusChangeListener(java.lang.Object param0) {
  }

  private int samplePercentForDuration(long param0) {
    int value = 100;
    return value;
  }

  private void maybeLogQueryToEventLog(long param0, android.net.Uri param1,
                                       java.lang.String[] param2, java.lang.String param3,
                                       java.lang.String param4) {
  }

  private void maybeLogUpdateToEventLog(long param0, android.net.Uri param1,
                                        java.lang.String param2, java.lang.String param3) {
  }

  protected ContentProvider acquireProvider(Context context, String auth) {
    // TODO Auto-generated method stub
    return null;
  }

}