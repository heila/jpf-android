package android.text.format;

import gov.nasa.jpf.vm.Abstraction;

public class DateFormat {
  public static final char AM_PM = 97;
  public static final char CAPITAL_AM_PM = 65;
  public static final char DATE = 100;
  public static final char DAY = 69;
  public static final char HOUR = 104;
  public static final char HOUR_OF_DAY = 107;
  public static final char MINUTE = 109;
  public static final char MONTH = 77;
  public static final char QUOTE = 39;
  public static final char SECONDS = 115;
  public static final char STANDALONE_MONTH = 76;
  public static final char TIME_ZONE = 122;
  public static final char YEAR = 121;

  private DateFormat() {

  }

  public static boolean is24HourFormat(android.content.Context param0) {
    return Abstraction.TOP_BOOL;
  }

  public static java.lang.String getBestDateTimePattern(java.util.Locale param0, java.lang.String param1) {
    return Abstraction.TOP_STRING;
  }

  public static java.text.DateFormat getTimeFormat(android.content.Context param0) {
    return new java.text.SimpleDateFormat();
  }

  public static java.text.DateFormat getDateFormat(android.content.Context param0) {
    return new java.text.SimpleDateFormat();
  }

  public static java.text.DateFormat getLongDateFormat(android.content.Context param0) {
    return new java.text.SimpleDateFormat();
  }

  public static java.text.DateFormat getMediumDateFormat(android.content.Context param0) {
    return new java.text.SimpleDateFormat();
  }

  public static char[] getDateFormatOrder(android.content.Context param0) {
    return ((char[]) Abstraction.randomObject("char[]"));
  }

  public static java.lang.CharSequence format(java.lang.CharSequence param0, long param1) {
    return ((java.lang.CharSequence) Abstraction.randomObject("java.lang.CharSequence"));
  }

  public static java.lang.CharSequence format(java.lang.CharSequence param0, java.util.Date param1) {
    return ((java.lang.CharSequence) Abstraction.randomObject("java.lang.CharSequence"));
  }

  public static java.lang.CharSequence format(java.lang.CharSequence param0, java.util.Calendar param1) {
    return ((java.lang.CharSequence) Abstraction.randomObject("java.lang.CharSequence"));
  }
}