package android.text.format;

import java.util.Formatter;

import android.content.Context;

public class DateUtils {
  public static final long SECOND_IN_MILLIS = 1000;
  public static final long MINUTE_IN_MILLIS = SECOND_IN_MILLIS * 60;
  public static final long HOUR_IN_MILLIS = MINUTE_IN_MILLIS * 60;
  public static final long DAY_IN_MILLIS = HOUR_IN_MILLIS * 24;
  public static final long WEEK_IN_MILLIS = DAY_IN_MILLIS * 7;
  /**
   * This constant is actually the length of 364 days, not of a year!
   */
  public static final long YEAR_IN_MILLIS = WEEK_IN_MILLIS * 52;

  // The following FORMAT_* symbols are used for specifying the format of
  // dates and times in the formatDateRange method.
  public static final int FORMAT_SHOW_TIME = 0x00001;
  public static final int FORMAT_SHOW_WEEKDAY = 0x00002;
  public static final int FORMAT_SHOW_YEAR = 0x00004;
  public static final int FORMAT_NO_YEAR = 0x00008;
  public static final int FORMAT_SHOW_DATE = 0x00010;
  public static final int FORMAT_NO_MONTH_DAY = 0x00020;
  @Deprecated
  public static final int FORMAT_12HOUR = 0x00040;
  @Deprecated
  public static final int FORMAT_24HOUR = 0x00080;
  @Deprecated
  public static final int FORMAT_CAP_AMPM = 0x00100;
  public static final int FORMAT_NO_NOON = 0x00200;
  @Deprecated
  public static final int FORMAT_CAP_NOON = 0x00400;
  public static final int FORMAT_NO_MIDNIGHT = 0x00800;
  @Deprecated
  public static final int FORMAT_CAP_MIDNIGHT = 0x01000;
  /**
   * @deprecated Use {@link #formatDateRange(Context, Formatter, long, long, int, String) formatDateRange} and
   *             pass in {@link Time#TIMEZONE_UTC Time.TIMEZONE_UTC} for the timeZone instead.
   */
  @Deprecated
  public static final int FORMAT_UTC = 0x02000;
  public static final int FORMAT_ABBREV_TIME = 0x04000;
  public static final int FORMAT_ABBREV_WEEKDAY = 0x08000;
  public static final int FORMAT_ABBREV_MONTH = 0x10000;
  public static final int FORMAT_NUMERIC_DATE = 0x20000;
  public static final int FORMAT_ABBREV_RELATIVE = 0x40000;
  public static final int FORMAT_ABBREV_ALL = 0x80000;
  @Deprecated
  public static final int FORMAT_CAP_NOON_MIDNIGHT = (FORMAT_CAP_NOON | FORMAT_CAP_MIDNIGHT);
  @Deprecated
  public static final int FORMAT_NO_NOON_MIDNIGHT = (FORMAT_NO_NOON | FORMAT_NO_MIDNIGHT);

  // Date and time format strings that are constant and don't need to be
  // translated.
  /**
   * This is not actually the preferred 24-hour date format in all locales.
   * 
   * @deprecated Use {@link java.text.SimpleDateFormat} instead.
   */
  @Deprecated
  public static final String HOUR_MINUTE_24 = "%H:%M";
  public static final String MONTH_FORMAT = "%B";
  /**
   * This is not actually a useful month name in all locales.
   * 
   * @deprecated Use {@link java.text.SimpleDateFormat} instead.
   */
  @Deprecated
  public static final String ABBREV_MONTH_FORMAT = "%b";
  public static final String NUMERIC_MONTH_FORMAT = "%m";
  public static final String MONTH_DAY_FORMAT = "%-d";
  public static final String YEAR_FORMAT = "%Y";
  public static final String YEAR_FORMAT_TWO_DIGITS = "%g";
  public static final String WEEKDAY_FORMAT = "%A";
  public static final String ABBREV_WEEKDAY_FORMAT = "%a";

  /** @deprecated Do not use. */
  @Deprecated
  public static final int[] sameYearTable = null;

  /** @deprecated Do not use. */
  @Deprecated
  public static final int[] sameMonthTable = null;

  /**
   * Request the full spelled-out name. For use with the 'abbrev' parameter of {@link #getDayOfWeekString} and
   * {@link #getMonthString}.
   *
   * @more <p>
   *       e.g. "Sunday" or "January"
   * @deprecated Use {@link java.text.SimpleDateFormat} instead.
   */
  @Deprecated
  public static final int LENGTH_LONG = 10;

  /**
   * Request an abbreviated version of the name. For use with the 'abbrev' parameter of
   * {@link #getDayOfWeekString} and {@link #getMonthString}.
   *
   * @more <p>
   *       e.g. "Sun" or "Jan"
   * @deprecated Use {@link java.text.SimpleDateFormat} instead.
   */
  @Deprecated
  public static final int LENGTH_MEDIUM = 20;

  /**
   * Request a shorter abbreviated version of the name. For use with the 'abbrev' parameter of
   * {@link #getDayOfWeekString} and {@link #getMonthString}.
   * 
   * @more <p>
   *       e.g. "Su" or "Jan"
   *       <p>
   *       In most languages, the results returned for LENGTH_SHORT will be the same as the results returned
   *       for {@link #LENGTH_MEDIUM}.
   * @deprecated Use {@link java.text.SimpleDateFormat} instead.
   */
  @Deprecated
  public static final int LENGTH_SHORT = 30;

  /**
   * Request an even shorter abbreviated version of the name. Do not use this. Currently this will always
   * return the same result as {@link #LENGTH_SHORT}.
   * 
   * @deprecated Use {@link java.text.SimpleDateFormat} instead.
   */
  @Deprecated
  public static final int LENGTH_SHORTER = 40;

  /**
   * Request an even shorter abbreviated version of the name. For use with the 'abbrev' parameter of
   * {@link #getDayOfWeekString} and {@link #getMonthString}.
   * 
   * @more <p>
   *       e.g. "S", "T", "T" or "J"
   *       <p>
   *       In some languages, the results returned for LENGTH_SHORTEST will be the same as the results
   *       returned for {@link #LENGTH_SHORT}.
   * @deprecated Use {@link java.text.SimpleDateFormat} instead.
   */
  @Deprecated
  public static final int LENGTH_SHORTEST = 50;

  // make native method
  public static String formatDateTime(Context mContext, long timeInMillis, int i) {
    return String.valueOf(timeInMillis);
  }

  // make native method
  public static CharSequence getRelativeTimeSpanString(long time) {

    return String.valueOf((time - System.currentTimeMillis()));
  }

}
