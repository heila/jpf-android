package android.app;

import java.util.Iterator;
import java.util.LinkedList;

public class NotificationManager {

  private static class NotificationWrapper {
    Notification mNotification;
    int mId;
    String mTag;

    public NotificationWrapper(int id, String tag, Notification n) {
      mNotification = n;
      mId = id;
      mTag = tag;

    }
  }

  private static LinkedList<NotificationWrapper> notifications = new LinkedList<NotificationWrapper>();

  public NotificationManager() {
  }

  public void notify(int id, android.app.Notification n) {
    notify(null, id, n);
  }

  public void notify(java.lang.String tag, int id, android.app.Notification n) {
    // user must technically swipe away
    // notifications.add(new NotificationWrapper(id, tag, n));
  }

  public void cancel(int id) {
    for (Iterator<NotificationWrapper> it = notifications.iterator(); it.hasNext();) {
      NotificationWrapper n = it.next();
      if (n.mId == id) {
        it.remove();
        return;
      }
    }

  }

  public void cancel(java.lang.String tag, int id) {
    for (Iterator<NotificationWrapper> it = notifications.iterator(); it.hasNext();) {
      NotificationWrapper n = it.next();
      if (n.mTag.equals(tag)) {
        it.remove();
        return;
      }
    }
  }

  public void cancelAll() {
    notifications = new LinkedList<NotificationWrapper>();
  }
}