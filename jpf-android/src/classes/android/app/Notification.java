package android.app;

import gov.nasa.jpf.vm.Abstraction;

public class Notification implements android.os.Parcelable {
  // public static final android.media.AudioAttributes AUDIO_ATTRIBUTES_DEFAULT =
  // ("android.media.AudioAttributes") Abstraction.randomObject("android.media.AudioAttributes");
  public static final java.lang.String CATEGORY_ALARM = "alarm";
  public static final java.lang.String CATEGORY_CALL = "call";
  public static final java.lang.String CATEGORY_EMAIL = "email";
  public static final java.lang.String CATEGORY_ERROR = "err";
  public static final java.lang.String CATEGORY_EVENT = "event";
  public static final java.lang.String CATEGORY_MESSAGE = "msg";
  public static final java.lang.String CATEGORY_PROGRESS = "progress";
  public static final java.lang.String CATEGORY_PROMO = "promo";
  public static final java.lang.String CATEGORY_RECOMMENDATION = "recommendation";
  public static final java.lang.String CATEGORY_SERVICE = "service";
  public static final java.lang.String CATEGORY_SOCIAL = "social";
  public static final java.lang.String CATEGORY_STATUS = "status";
  public static final java.lang.String CATEGORY_SYSTEM = "sys";
  public static final java.lang.String CATEGORY_TRANSPORT = "transport";
  public static final int COLOR_DEFAULT = 0;
  public static final android.os.Parcelable.Creator CREATOR = ((android.os.Parcelable.Creator) Abstraction
      .randomObject("android.os.Parcelable.Creator"));
  public static final int DEFAULT_ALL = -1;
  public static final int DEFAULT_LIGHTS = 4;
  public static final int DEFAULT_SOUND = 1;
  public static final int DEFAULT_VIBRATE = 2;
  public static final java.lang.String EXTRA_BACKGROUND_IMAGE_URI = "android.backgroundImageUri";
  public static final java.lang.String EXTRA_BIG_TEXT = "android.bigText";
  public static final java.lang.String EXTRA_COMPACT_ACTIONS = "android.compactActions";
  public static final java.lang.String EXTRA_INFO_TEXT = "android.infoText";
  public static final java.lang.String EXTRA_LARGE_ICON = "android.largeIcon";
  public static final java.lang.String EXTRA_LARGE_ICON_BIG = "android.largeIcon.big";
  public static final java.lang.String EXTRA_MEDIA_SESSION = "android.mediaSession";
  public static final java.lang.String EXTRA_PEOPLE = "android.people";
  public static final java.lang.String EXTRA_PICTURE = "android.picture";
  public static final java.lang.String EXTRA_PROGRESS = "android.progress";
  public static final java.lang.String EXTRA_PROGRESS_INDETERMINATE = "android.progressIndeterminate";
  public static final java.lang.String EXTRA_PROGRESS_MAX = "android.progressMax";
  public static final java.lang.String EXTRA_SHOW_CHRONOMETER = "android.showChronometer";
  public static final java.lang.String EXTRA_SHOW_WHEN = "android.showWhen";
  public static final java.lang.String EXTRA_SMALL_ICON = "android.icon";
  public static final java.lang.String EXTRA_SUB_TEXT = "android.subText";
  public static final java.lang.String EXTRA_SUMMARY_TEXT = "android.summaryText";
  public static final java.lang.String EXTRA_TEMPLATE = "android.template";
  public static final java.lang.String EXTRA_TEXT = "android.text";
  public static final java.lang.String EXTRA_TEXT_LINES = "android.textLines";
  public static final java.lang.String EXTRA_TITLE = "android.title";
  public static final java.lang.String EXTRA_TITLE_BIG = "android.title.big";
  public static final int FLAG_AUTO_CANCEL = 16;
  public static final int FLAG_FOREGROUND_SERVICE = 64;
  public static final int FLAG_GROUP_SUMMARY = 512;
  public static final int FLAG_HIGH_PRIORITY = 128;
  public static final int FLAG_INSISTENT = 4;
  public static final int FLAG_LOCAL_ONLY = 256;
  public static final int FLAG_NO_CLEAR = 32;
  public static final int FLAG_ONGOING_EVENT = 2;
  public static final int FLAG_ONLY_ALERT_ONCE = 8;
  public static final int FLAG_SHOW_LIGHTS = 1;
  public static final java.lang.String INTENT_CATEGORY_NOTIFICATION_PREFERENCES = "android.intent.category.NOTIFICATION_PREFERENCES";
  public static final int PRIORITY_DEFAULT = 0;
  public static final int PRIORITY_HIGH = 1;
  public static final int PRIORITY_LOW = -1;
  public static final int PRIORITY_MAX = 2;
  public static final int PRIORITY_MIN = -2;
  public static final int STREAM_DEFAULT = -1;
  public static final int VISIBILITY_PRIVATE = 0;
  public static final int VISIBILITY_PUBLIC = 1;
  public static final int VISIBILITY_SECRET = -1;
  // public android.app.Notification.Action[] actions;
  // public android.media.AudioAttributes audioAttributes;
  public int audioStreamType;
  public android.widget.RemoteViews bigContentView;
  public java.lang.String category;
  public int color;
  public android.app.PendingIntent contentIntent;
  public android.widget.RemoteViews contentView;
  public int defaults;
  public android.app.PendingIntent deleteIntent;
  public android.os.Bundle extras;
  public int flags;
  public android.app.PendingIntent fullScreenIntent;
  public android.widget.RemoteViews headsUpContentView;
  public int icon;
  public int iconLevel;
  public android.graphics.Bitmap largeIcon;
  public int ledARGB;
  public int ledOffMS;
  public int ledOnMS;
  public int number;
  public int priority;
  public android.app.Notification publicVersion;
  public android.net.Uri sound;
  public java.lang.CharSequence tickerText;
  public android.widget.RemoteViews tickerView;
  public long[] vibrate;
  public int visibility;
  public long when;
  public static android.app.Notification TOP = new android.app.Notification();

  public Notification() {
  }

  public Notification(int param0, java.lang.CharSequence param1, long param2) {
  }

  public Notification(android.os.Parcel param0) {
  }

  public java.lang.String getGroup() {
    return Abstraction.TOP_STRING;
  }

  public java.lang.String getSortKey() {
    return Abstraction.TOP_STRING;
  }

  @Override
  public android.app.Notification clone() {
    return android.app.Notification.TOP;
  }

  @Override
  public int describeContents() {
    return Abstraction.TOP_INT;
  }

  @Override
  public void writeToParcel(android.os.Parcel param0, int param1) {
  }

  public void setLatestEventInfo(android.content.Context param0, java.lang.CharSequence param1,
                                 java.lang.CharSequence param2, android.app.PendingIntent param3) {
  }

  @Override
  public java.lang.String toString() {
    return Abstraction.TOP_STRING;
  }

  // public static class Action {
  // public static android.app.Notification.Action TOP = new android.app.Notification.Action();
  //
  // public class Builder {
  // public static android.app.Notification.Action.Builder TOP = new
  // android.app.Notification.Action.Builder();
  //
  // public Builder() {
  // }
  // }
  //
  // public class WearableExtender {
  // public static android.app.Notification.Action.WearableExtender TOP = new
  // android.app.Notification.Action.WearableExtender();
  //
  // public WearableExtender() {
  // }
  // }
  //
  // public Action() {
  // }
  // }
}