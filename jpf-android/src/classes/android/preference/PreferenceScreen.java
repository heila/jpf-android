package android.preference;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceScreen extends PreferenceGroup {

  public PreferenceScreen(Context context) {
    super(context);
  }


  public SharedPreferences getSharedPreferences() {
    return getContext().getSharedPreferences("", 0);
  }

}
