package android.preference;

import gov.nasa.jpf.annotation.FilterField;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

import android.content.Context;
import android.util.AttributeSet;
import android.util.AttributeSetImpl;
import android.util.Log;
import android.view.InflateException;

public class PreferenceInflater {
  public static final String TAG = "PreferenceInflater";
  private static final boolean DEBUG = false;

  /** The package where Android Widgets are stored. */
  private static final String PACKAGE = "android.preference";

  private static final int TYPE = 0;
  private static final int ID = 1;
  private static final int NAME = 2;
  private static final int HASHCODE = 3;
  private static final int TEXT = 4;
  private static final int LISTENER = 5;

  /**
   * Context of the Activity
   */
  Context c;

  /**
   * Used to create an unique name and id field for components that are not named in the R.java file. Window
   * has count 0.
   */
  @FilterField
  private static int count = 1;

  public PreferenceInflater(Context c) {
    this.c = c;
  }

  /**
   * Obtains the LayoutInflater from the given context.
   */
  public static PreferenceInflater from(Context context) {

    if (instance == null) {
      instance = new PreferenceInflater(context);
    } else {
      instance.c = context;
    }
    return instance;
  }

  /**
   * Inflate a new view hierarchy from the specified xml resource. Throws {@link InflateException} if there is
   * an error.
   * 
   * @param resource
   *          ID for an XML layout resource to load (e.g., <code>R.layout.main_page</code>)
   * @param root
   *          Optional view to be the parent of the generated hierarchy.
   * @return The root View of the inflated hierarchy. If root was supplied, this is the root View; otherwise
   *         it is the root of the inflated XML file.
   * @throws Exception
   */
  public Preference inflate(int resourceID, Preference root) {
    String filename = null;
    try {
      filename = loadXMLFile(resourceID);
      int rootNativeHash = getRootHash(resourceID);

      Preference pref = visit(rootNativeHash, resourceID);
      // print(view, "\t");

      if (root != null && pref != null)
        ((PreferenceGroup) root).addItemFromInflater(pref);
      else
        return pref;

    } catch (Exception e) {
      Log.e(TAG, "Error inflating preference file \"" + filename + "\":" + e.getMessage());
      e.printStackTrace();
      throw new RuntimeException(e);
    }
    return root;

  }

  private native int getRootHash(int resourceID);

  private native String loadXMLFile(int resourceID);

  public Preference visit(int nodeCode, int resourceID) throws Exception {
    if (nodeCode == -1)
      return null;

    // set the info of the view
    String[] node = getNodeInfo(nodeCode, resourceID);
    Object[] nodeAttr = getNodeAttributes(nodeCode, resourceID);

    Preference pref = null;
    try {
      // inflate the pref
      pref = inflatePreference(node, nodeAttr);
      if (pref == null) {
        Log.w(TAG, "Error inflating preference " + node + " " + nodeAttr);
        return null;
      }

      // visit the children of the view
      int[] childrenIds = getChildren(nodeCode, resourceID);
      Preference child = null;
      if (childrenIds != null) {
        for (int id : childrenIds) {
          // visit the child
          child = visit(id, resourceID);
          if (child != null) {
            // add child to parent
            ((PreferenceGroup) pref).addItemFromInflater(child);
          }
        }
      }
    } catch (Exception e) {
      Log.e(TAG, "Error inflating preference " + node + " " + nodeAttr);
      throw e;
    }
    return pref;
  }

  public native String[] getNodeInfo(int hash, int resourceID);

  public native Object[] getNodeAttributes(int hash, int resourceID);

  public native int[] getChildren(int hash, int resourceID);

  private void print(Preference v, String space) {
    if (v == null) {
      System.out.println("NULL VIEW");
      return;
    }
    System.out.print(v.toString() + "{");
    if (v instanceof PreferenceGroup) {
      for (int i = 0; i < ((PreferenceGroup) v).getPreferenceCount(); i++) {
        Preference c = ((PreferenceGroup) v).getPreference(i);
        if (c != null) {
          System.out.print("\n");
          System.out.print(space);
          print(c, space + "\t");
        }
      }
    }

    System.out.print("}");
  }

  /**
   * Makes use of Java reflection to create an instance of a Preference object.
   * 
   */
  public Preference inflatePreference(final String[] info, Object[] attributes) throws ClassNotFoundException, NoSuchMethodException,
      IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
    Preference pref = null;
    // inflate the view
    if (info[TYPE].contains(".")) { // custom preference
      Class<? extends Preference> cls = (Class<? extends Preference>) Class.forName(info[TYPE]);
      Class[] intArgsClass = new Class[] { Context.class, AttributeSet.class };
      Object[] intArgs = new Object[] { c, (new AttributeSetImpl()) };
      Constructor intArgsConstructor = cls.getConstructor(intArgsClass);
      System.out.println("pref = " + cls.getName() + intArgsConstructor + c);
      Object o = intArgsConstructor.newInstance(intArgs);
      System.out.println("pref = " + cls.getName() + intArgsConstructor + c + 0);

      pref = (Preference) o;

      System.out.println("pref = " + pref);

      for (Object att : attributes) {
        Object[] attribute = (Object[]) att;
        System.out.println(Arrays.toString(attribute));
        switch (((String) attribute[0])) {
        case "android:title": {
          pref.setTitle((String) attribute[1]);
          break;
        }
        case "android:defaultValue": {
          try {
            int i = Integer.valueOf((String) attribute[1]);
            pref.setDefaultValue(i);
            break;
          } catch (Exception e) {
          }
          try {
            float i = Float.valueOf((String) attribute[1]);
            pref.setDefaultValue(i);
            break;
          } catch (Exception e) {
          }

          pref.setDefaultValue(attribute[1]);
          break;
        }
        case "android:summary": {
          break;
        }

        case "android:key": {
          pref.setKey((String) attribute[1]);
          break;
        }
        case "android:entries": {
          ((ListPreference) pref).setEntries((String[]) attribute[1]);
          break;
        }
        case "android:entryValues": {
          System.out.println("entryValues: " + Arrays.toString((String[]) attribute[1]));
          ((ListPreference) pref).setEntryValues((String[]) attribute[1]);
          break;
        }
        case "android:dialogLayout": {
          System.out.println(attribute[1]);
          int layout = (int) attribute[1];
          // System.out.println(layout);
          // System.out.println(getRValue(layout));
          ((DialogPreference) pref).setLayout(layout);
          break;
        }
        default: {
          break;
        }
        }

        // android:dialogLayout="@layout/database_settings"
        // android:positiveButtonText="@string/entry_save"
        // android:negativeButtonText="@string/entry_cancel"/>
        if (pref.getKey() == null) {
          pref.setKey("-1");
        }
        if (pref instanceof DialogPreference) {
          // View v = ((DialogPreference) pref).onCreateDialogView();
          // ((DialogPreference) pref).setView(v);
          // ((DialogPreference) pref).onBindDialogView(v);
          // ((DialogPreference) pref).onDialogClosed(true);
        }
      }
    } else if (info[TYPE].equals("intent")) {

    } else {

      Class<? extends Preference> cls = (Class<? extends Preference>) Class.forName(PACKAGE + "." + info[TYPE]);
      Class[] intArgsClass = new Class[] { Context.class };
      Object[] intArgs = new Object[] { c };
      Constructor intArgsConstructor = cls.getConstructor(intArgsClass);
      // System.out.println("pref = " + cls.getName() + intArgsConstructor + c);
      Object o = intArgsConstructor.newInstance(intArgs);
      // System.out.println("pref = " + cls.getName() + intArgsConstructor + c +
      // 0);

      pref = (Preference) o;

      // System.out.println("pref = " + pref);

      for (Object att : attributes) {
        Object[] attribute = (Object[]) att;
        // System.out.println(Arrays.toString(attribute));
        switch (((String) attribute[0])) {
        case "android:title": {
          pref.setTitle((String) attribute[1]);
          break;
        }
        case "android:defaultValue": {
          try {
            int i = Integer.valueOf((String) attribute[1]);
            pref.setDefaultValue(i);
            break;
          } catch (Exception e) {
          }
          try {
            float i = Float.valueOf((String) attribute[1]);
            pref.setDefaultValue(i);
            break;
          } catch (Exception e) {
          }
          try {
            boolean i = Boolean.valueOf((String) attribute[1]);
            pref.setDefaultValue(i);
            break;
          } catch (Exception e) {
          }
          pref.setDefaultValue(attribute[1]);
          break;
        }
        case "android:summary": {
          break;
        }
        case "android:key": {
          pref.setKey((String) attribute[1]);
          System.out.println("PERFS.key: " + pref.getKey());
          break;
        }
        case "android:entries": {
          ((ListPreference) pref).setEntries((String[]) attribute[1]);
          break;
        }
        case "android:entryValues": {
          System.out.println("entryValues: " + Arrays.toString((String[]) attribute[1]));
          ((ListPreference) pref).setEntryValues((String[]) attribute[1]);
          break;
        }
        default: {
          break;
        }
        }
      }
    }
    if (pref != null && pref.getKey() == null) {
      pref.setKey("-1");
    }
    if (DEBUG)
      Log.v(TAG, "Inflated " + pref.toString());
    return pref;

  }

  private native int getRValue(String name);

  private native String loadPreferences(int resourceID);

  @FilterField
  public static PreferenceInflater instance = null;

}
