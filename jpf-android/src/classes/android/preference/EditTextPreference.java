package android.preference;

import android.content.Context;
import android.widget.EditText;

public class EditTextPreference extends android.preference.Preference {

  EditText edit;

  public EditTextPreference(Context context) {
    super(context);
    edit = new EditText(context);
  }

  @Override
  public void setSummary(int summary) {

  }

  public void setDialogTitle(int titleRes) {

  }

  public EditText getEditText() {
    return edit;

  }

  public String getText() {
    return edit.getText().toString();
  }

}