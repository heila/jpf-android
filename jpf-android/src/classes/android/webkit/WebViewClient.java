package android.webkit;

import android.graphics.Bitmap;

public class WebViewClient {

  public WebViewClient() {
  }

  public boolean shouldOverrideUrlLoading(WebView view, String url) {
    return false;
  }

  public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

  }

  public void onPageStarted(WebView view, String url, Bitmap favicon) {

  }

  public void onPageFinished(WebView view, String url) {
  }
}