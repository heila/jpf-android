package android.webkit;

import gov.nasa.jpf.vm.AndroidVerify;
import android.graphics.Bitmap;

public class WebView extends android.view.ViewGroup {

  public WebView(android.content.Context context) {
    super(context);
  }

  @Override
  public void setVerticalScrollBarEnabled(boolean b) {

  }

  @Override
  public void setHorizontalScrollBarEnabled(boolean b) {

  }

  WebChromeClient mWebChromeClient;

  public void setWebChromeClient(android.webkit.WebChromeClient param0) {
    this.mWebChromeClient = param0;
  }

  WebViewClient mWebViewClient;

  public void setWebViewClient(android.webkit.WebViewClient param0) {
    this.mWebViewClient = param0;
  }

  public void loadDataWithBaseURL(String baseUrl, String data, String mimeType, String encoding,
                                  String historyUrl) {
    loadUrl(baseUrl);
  }

  public android.webkit.WebSettings getSettings() {
    return android.webkit.WebSettings.TOP;
  }

  public void loadUrl(java.lang.String param0) {
    if (mWebViewClient != null) {
      mWebViewClient.shouldOverrideUrlLoading(this, param0);
      mWebViewClient.onPageStarted(this, param0, Bitmap.TOP);

      boolean i = AndroidVerify.getBoolean("WebView.loadURL() success?");
      if (!i) {
        mWebViewClient.onPageFinished(this, param0);
      } else {
        mWebViewClient.onReceivedError(this, 2, "Error occured", param0);
      }
    }
    if (mWebChromeClient != null)
      mWebChromeClient.onProgressChanged(this, 20);

  }

  public void stopLoading() {
  }

  @Override
  public void setLayoutParams(android.view.ViewGroup.LayoutParams param0) {
  }

  @Override
  protected void onLayout(boolean changed, int l, int t, int r, int b) {
  }

  public void destroy() {
  }

}