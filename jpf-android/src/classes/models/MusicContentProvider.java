package models;

import gov.nasa.jpf.annotation.FilterField;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.CancellationSignal;

public class MusicContentProvider extends ContentProvider {

  @Override
  public boolean onCreate() {
    return true;
  }

  @Override
  public Cursor query(Uri param0, String[] param1, String param2, String[] param3, String param4,
                      CancellationSignal param5) {
    return new MusicCursor();
  }

  @Override
  public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
    return new MusicCursor();
  }

  @Override
  public String getType(Uri uri) {
    return null;
  }

  @Override
  public Uri insert(Uri uri, ContentValues values) {
    return uri;

  }

  @Override
  public int delete(Uri uri, String selection, String[] selectionArgs) {
    return 0;
  }

  @Override
  public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
    return 0;

  }

  private class MusicCursor implements Cursor {

    @FilterField
    int count = 2;
    int position = -1;

    @Override
    public int getCount() {
      return count;
    }

    @Override
    public int getPosition() {
      // TODO Auto-generated method stub
      return position;
    }

    @Override
    public boolean move(int offset) {
      // TODO Auto-generated method stub
      return false;
    }

    @Override
    public boolean moveToPosition(int position) {
      if (position <= count && position > -1) {
        this.position = position;
        return true;
      } else {
        return false;
      }
    }

    @Override
    public boolean moveToFirst() {
      position = 0;
      return true;
    }

    @Override
    public boolean moveToLast() {
      position = 2;
      return true;
    }

    @Override
    public boolean moveToNext() {
      if (position < count) {
        position++;
        return true;
      } else {
        return false;
      }
    }

    @Override
    public boolean moveToPrevious() {
      if (position > -1) {
        position--;
        return true;
      } else {
        return false;
      }
    }

    @Override
    public boolean isFirst() {
      return position == 0;
    }

    @Override
    public boolean isLast() {
      // TODO Auto-generated method stub
      return position == count;
    }

    @Override
    public boolean isBeforeFirst() {
      // TODO Auto-generated method stub
      return false;
    }

    @Override
    public boolean isAfterLast() {
      // TODO Auto-generated method stub
      return false;
    }

    @Override
    public int getColumnIndex(String columnName) {
      // TODO Auto-generated method stub
      return 0;
    }

    @Override
    public int getColumnIndexOrThrow(String columnName) throws IllegalArgumentException {
      // TODO Auto-generated method stub
      return 0;
    }

    @Override
    public String getColumnName(int columnIndex) {
      // TODO Auto-generated method stub
      return "Name";
    }

    @Override
    public String[] getColumnNames() {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public int getColumnCount() {
      // TODO Auto-generated method stub
      return 0;
    }

    @Override
    public byte[] getBlob(int columnIndex) {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public String getString(int columnIndex) {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public void copyStringToBuffer(int columnIndex, CharArrayBuffer buffer) {
      // TODO Auto-generated method stub

    }

    @Override
    public short getShort(int columnIndex) {
      // TODO Auto-generated method stub
      return 0;
    }

    @Override
    public int getInt(int columnIndex) {
      // TODO Auto-generated method stub
      return 0;
    }

    @Override
    public long getLong(int columnIndex) {
      // TODO Auto-generated method stub
      return 0;
    }

    @Override
    public float getFloat(int columnIndex) {
      // TODO Auto-generated method stub
      return 0;
    }

    @Override
    public double getDouble(int columnIndex) {
      // TODO Auto-generated method stub
      return 0;
    }

    @Override
    public int getType(int columnIndex) {
      // TODO Auto-generated method stub
      return 0;
    }

    @Override
    public boolean isNull(int columnIndex) {
      // TODO Auto-generated method stub
      return false;
    }

    @Override
    public void deactivate() {
      // TODO Auto-generated method stub

    }

    @Override
    public boolean requery() {
      // TODO Auto-generated method stub
      return false;
    }

    @Override
    public void close() {
      // TODO Auto-generated method stub

    }

    @Override
    public boolean isClosed() {
      // TODO Auto-generated method stub
      return false;
    }

    @Override
    public void registerContentObserver(ContentObserver observer) {
      // TODO Auto-generated method stub

    }

    @Override
    public void unregisterContentObserver(ContentObserver observer) {
      // TODO Auto-generated method stub

    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
      // TODO Auto-generated method stub

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
      // TODO Auto-generated method stub

    }

    @Override
    public void setNotificationUri(ContentResolver cr, Uri uri) {
      // TODO Auto-generated method stub

    }

    @Override
    public Uri getNotificationUri() {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public boolean getWantsAllOnMoveCalls() {
      // TODO Auto-generated method stub
      return false;
    }

    @Override
    public Bundle getExtras() {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public Bundle respond(Bundle extras) {
      // TODO Auto-generated method stub
      return null;
    }

  }
}
