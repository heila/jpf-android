package models;

import gov.nasa.jpf.vm.Abstraction;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.DefaultCursor;
import android.net.Uri;
import android.os.CancellationSignal;

public class ContactsContentProvider extends ContentProvider {

  public static class Contacts {
    /**
     * Opens an InputStream for the contacts's thumbnail photo and returns the photo as a byte stream.
     * 
     * @param cr
     *          The content resolver to use for querying
     * @param contactUri
     *          the contact whose photo should be used. This can be used with either a {@link #CONTENT_URI} or
     *          a {@link #CONTENT_LOOKUP_URI} URI.
     * @return an InputStream of the photo, or null if no photo is present
     * @see #openContactPhotoInputStream(ContentResolver, Uri, boolean), if instead of the thumbnail the
     *      high-res picture is preferred
     */
    public static InputStream openContactPhotoInputStream(ContentResolver cr, Uri contactUri) {
      return new ByteArrayInputStream(new byte[] { 0 });
    }
  }

  @Override
  public boolean onCreate() {
    return true;
  }

  @Override
  public Cursor query(Uri uri, String[] projection, String param2, String[] param3, String param4, CancellationSignal param5) {
    return new ContactsCursor(projection);
  }

  @Override
  public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
    return new ContactsCursor(projection);
  }

  @Override
  public String getType(Uri uri) {
    return null;
  }

  @Override
  public Uri insert(Uri uri, ContentValues values) {
    System.out.println("###ContactsContentProvider");
    return Uri.withAppendedPath(uri, "1");

  }

  @Override
  public int delete(Uri uri, String selection, String[] selectionArgs) {
    return 0;
  }

  @Override
  public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
    return 0;

  }

  private static class ContactsCursor extends DefaultCursor {

    private String[] columns;

    public ContactsCursor(String[] projection) {
      this.columns = projection;
    }

    @Override
    public int getColumnIndex(String param0) {
      if (columns != null) {
        for (int i = 0; i < columns.length; i++) {
          if (columns[i].equals(param0)) {
            return i;
          }
        }
      }
      return 0;
    }

    @Override
    public int getColumnIndexOrThrow(String columnName) throws IllegalArgumentException {
      if (columns != null) {
        for (int i = 0; i < columns.length; i++) {
          if (columns[i].equals(columnName)) {
            return i;
          }
        }
      }
      return 0;
    }

    @Override
    public String getColumnName(int columnIndex) {
      if (columns != null) {
        return columns[columnIndex];
      } else
        return Abstraction.TOP_STRING;
    }

    @Override
    public int getColumnCount() {
      return columns.length;
    }

    @Override
    public String getString(int columnIndex) {
      return "1";
    }

    @Override
    public int getInt(int columnIndex) {
      return 1;
      // if (getColumnName(columnIndex) != null && getColumnName(columnIndex).equals(Phones.TYPE))
      // return (int) AndroidVerify.getValues(new Integer[] { 1, 2, 3 }, "ContactsCursor.getInt(" +
      // columnIndex + ")");
      // return (int) AndroidVerify.getValues(new Integer[] { -1, 0, 1 }, "ContactsCursor.getInt");
    }
  }
}
