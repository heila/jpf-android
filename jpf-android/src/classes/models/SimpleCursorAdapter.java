package models;
//package android.widget;
//
//import android.content.Context;
//import android.database.Cursor;
//import android.net.Uri;
//import android.view.View;
//
///**
// * What would we want to check: data comming through in cursor might be null or of "" length of even correct
// * and then we parse it incorrectly. In other words we want to execute the the bindView method for each of the
// * values in the cursor to make sure it does not crash in there.
// * 
// * Check that the columns in "from" are valid.
// * 
// * Set the data in the widgets so that we can use it later maybe for a next view or something
// * 
// * @author Heila
// * 
// */
//public class SimpleCursorAdapter extends ResourceCursorAdapter {
//
//  /**
//   * A list of columns containing the data to bind to the UI. This field should be made private, so it is
//   * hidden from the SDK. {@hide}
//   */
//  protected int[] mFrom;
//  /**
//   * A list of View ids representing the views to which the data must be bound. This field should be made
//   * private, so it is hidden from the SDK. {@hide}
//   */
//  protected int[] mTo;
//
//  private int mStringConversionColumn = -1;
//  private CursorToStringConverter mCursorToStringConverter;
//  private ViewBinder mViewBinder;
//
//  String[] mOriginalFrom;
//
//  @Deprecated
//  public SimpleCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
//    super(context, layout, c);
//    mTo = to;
//    mOriginalFrom = from;
//    findColumns(c, from);
//  }
//
//  public SimpleCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
//    super(context, layout, c, flags);
//    mTo = to;
//    mOriginalFrom = from;
//    findColumns(c, from);
//  }
//
//  @Override
//  public void bindView(View view, Context context, Cursor cursor) {
//    final ViewBinder binder = mViewBinder;
//    final int count = mTo.length;
//    final int[] from = mFrom;
//    final int[] to = mTo;
//
//    for (int i = 0; i < count; i++) {
//      final View v = view.findViewById(to[i]);
//      if (v != null) {
//        boolean bound = false;
//        if (binder != null) {
//          bound = binder.setViewValue(v, cursor, from[i]);
//        }
//
//        if (!bound) {
//          String text = cursor.getString(from[i]);
//          if (text == null) {
//            text = "";
//          }
//
//          if (v instanceof TextView) {
//            setViewText((TextView) v, text);
//          } else if (v instanceof ImageView) {
//            setViewImage((ImageView) v, text);
//          } else {
//            throw new IllegalStateException(v.getClass().getName() + " is not a "
//                + " view that can be bounds by this SimpleCursorAdapter");
//          }
//        }
//      }
//    }
//  }
//
//
//  public ViewBinder getViewBinder() {
//    return mViewBinder;
//  }
//
//  public void setViewBinder(ViewBinder viewBinder) {
//    mViewBinder = viewBinder;
//  }
//
//  public void setViewImage(ImageView v, String value) {
//    try {
//      v.setImageResource(Integer.parseInt(value));
//    } catch (NumberFormatException nfe) {
//      v.setImageURI(Uri.parse(value));
//    }
//  }
//  public void setViewText(TextView v, String text) {
//    v.setText(text);
//  }
//
//  public int getStringConversionColumn() {
//    return mStringConversionColumn;
//  }
//
//  public void setStringConversionColumn(int stringConversionColumn) {
//    mStringConversionColumn = stringConversionColumn;
//  }
//
//  public void setCursorToStringConverter(CursorToStringConverter cursorToStringConverter) {
//    mCursorToStringConverter = cursorToStringConverter;
//  }
//
//
//  public CursorToStringConverter getCursorToStringConverter() {
//    return mCursorToStringConverter;
//  }
//
//  @Override
//  public CharSequence convertToString(Cursor cursor) {
//    if (mCursorToStringConverter != null) {
//      return mCursorToStringConverter.convertToString(cursor);
//    } else if (mStringConversionColumn > -1) {
//      return cursor.getString(mStringConversionColumn);
//    }
//
//    return super.convertToString(cursor);
//  }
//  
//  /**
//   * Create a map from an array of strings to an array of column-id integers in mCursor. If mCursor is null,
//   * the array will be discarded.
//   * 
//   * @param from
//   *          the Strings naming the columns of interest
//   */
//  private void findColumns(Cursor c, String[] from) {
//    if (c != null) {
//      int i;
//      int count = from.length;
//      if (mFrom == null || mFrom.length != count) {
//        mFrom = new int[count];
//      }
//      for (i = 0; i < count; i++) {
//        mFrom[i] = c.getColumnIndexOrThrow(from[i]);
//      }
//    } else {
//      mFrom = null;
//    }
//  }
//
//  @Override
//  public Cursor swapCursor(Cursor c) {
//    // mCursor.close();
//    // Cursor temp = mCursor;
//    // mCursor = c;
//    // return temp;
//    // super.swapCursor() will notify observers before we have
//    // a valid mapping, make sure we have a mapping before this
//    // happens
//    findColumns(c, mOriginalFrom);
//    return super.swapCursor(c);
//  }
//
//
//  public void changeCursorAndColumns(Cursor c, String[] from, int[] to) {
//    mOriginalFrom = from;
//    mTo = to;
//    // super.changeCursor() will notify observers before we have
//    // a valid mapping, make sure we have a mapping before this
//    // happens
//    findColumns(c, mOriginalFrom);
//    super.changeCursor(c);
//  }
//
//  /**
//   * This class can be used by external clients of SimpleCursorAdapter to bind values fom the Cursor to views.
//   *
//   * You should use this class to bind values from the Cursor to views that are not directly supported by
//   * SimpleCursorAdapter or to change the way binding occurs for views supported by SimpleCursorAdapter.
//   *
//   * @see SimpleCursorAdapter#bindView(android.view.View, android.content.Context, android.database.Cursor)
//   * @see SimpleCursorAdapter#setViewImage(ImageView, String)
//   * @see SimpleCursorAdapter#setViewText(TextView, String)
//   */
//  public static interface ViewBinder {
//    /**
//     * Binds the Cursor column defined by the specified index to the specified view.
//     *
//     * When binding is handled by this ViewBinder, this method must return true. If this method returns false,
//     * SimpleCursorAdapter will attempts to handle the binding on its own.
//     *
//     * @param view
//     *          the view to bind the data to
//     * @param cursor
//     *          the cursor to get the data from
//     * @param columnIndex
//     *          the column at which the data can be found in the cursor
//     *
//     * @return true if the data was bound to the view, false otherwise
//     */
//    boolean setViewValue(View view, Cursor cursor, int columnIndex);
//  }
//
//  /**
//   * This class can be used by external clients of SimpleCursorAdapter to define how the Cursor should be
//   * converted to a String.
//   *
//   * @see android.widget.CursorAdapter#convertToString(android.database.Cursor)
//   */
//  public static interface CursorToStringConverter {
//    /**
//     * Returns a CharSequence representing the specified Cursor.
//     *
//     * @param cursor
//     *          the cursor for which a CharSequence representation is requested
//     *
//     * @return a non-null CharSequence representing the cursor
//     */
//    CharSequence convertToString(Cursor cursor);
//  }
//
// }
