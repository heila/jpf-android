package models;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.annotation.NeverBreak;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import android.app.ActivityThread;
import android.os.Looper;

public class DefaultSharedPreferences implements android.content.SharedPreferences {
  @NeverBreak
  public final WeakHashMap<OnSharedPreferenceChangeListener, Object> mListeners = new WeakHashMap<OnSharedPreferenceChangeListener, Object>();
  @NeverBreak
  @FilterField
  private Map<String, Object> mMap; // guarded by 'this'
  @FilterField
  @NeverBreak
  private int mMode;
  @FilterField
  @NeverBreak
  private static final Object mContent = new Object();

  public DefaultSharedPreferences() {
    mMap = new HashMap<String, Object>();
  }

  DefaultSharedPreferences(File file, int mode) {
    mMode = mode;
    mMap = new HashMap<String, Object>();
  }

  @Override
  public boolean getBoolean(String key, boolean defValue) {
    synchronized (this) {
      Boolean v = (Boolean) mMap.get(key);
      return v != null ? v : defValue;
      // return AndroidVerify.getBoolean("SharedPreferencesImpl.getBool(" + key + ")");
    }
  }

  @Override
  public int getInt(String key, int defValue) {
    synchronized (this) {
      Integer v = (Integer) mMap.get(key);
      return v != null ? v : defValue;
    }
  }

  @Override
  public android.content.SharedPreferences.Editor edit() {
    return new EditorImpl();
  }

  @Override
  public long getLong(String key, long defValue) {
    synchronized (this) {
      Long v = (Long) mMap.get(key);
      return v != null ? v : defValue;
    }
  }

  @Override
  public java.lang.String getString(String key, String defValue) {
    synchronized (this) {
      String v = (String) mMap.get(key);
      return v != null ? v : defValue;
    }
  }

  public void startReloadIfChangedUnexpectedly() {
    // do nothing
  }

  @Override
  public Map<String, ?> getAll() {
    synchronized (this) {
      return new HashMap<String, Object>(mMap);
    }
  }

  @Override
  public Set<String> getStringSet(String key, Set<String> defValues) {
    synchronized (this) {
      Set<String> v = (Set<String>) mMap.get(key);
      return v != null ? v : defValues;
    }
  }

  @Override
  public float getFloat(String key, float defValue) {
    synchronized (this) {
      Float v = (Float) mMap.get(key);
      return v != null ? v : defValue;
    }
  }

  @Override
  public boolean contains(String key) {
    synchronized (this) {
      return mMap.containsKey(key);
    }
  }

  @Override
  public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
    synchronized (this) {
      mListeners.put(listener, mContent);
    }

  }

  @Override
  public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
    synchronized (this) {
      mListeners.remove(listener);
    }
  }

  private void enqueueDiskWrite(final MemoryCommitResult mcr, final Runnable postWriteRunnable) {
    // we are not writing to disk
  }

  // Return value from EditorImpl#commitToMemory()
  private static class MemoryCommitResult {
    public boolean changesMade; // any keys different?
    public List<String> keysModified; // may be null
    public Set<OnSharedPreferenceChangeListener> listeners; // may be null
    public Map<?, ?> mapToWriteToDisk;
    // public final CountDownLatch writtenToDiskLatch = new CountDownLatch(1);
    public volatile boolean writeToDiskResult = false;

    public void setDiskWriteResult(boolean result) {
      writeToDiskResult = result;
      // writtenToDiskLatch.countDown();
    }
  }

  public final class EditorImpl implements Editor {
    private final Map<String, Object> mModified = new HashMap<String, Object>();
    private boolean mClear = false;

    @Override
    public Editor putString(String key, String value) {
      synchronized (this) {
        mModified.put(key, value);
        return this;
      }
    }

    @Override
    public Editor putStringSet(String key, Set<String> values) {
      synchronized (this) {
        mModified.put(key, (values == null) ? null : new HashSet<String>(values));
        return this;
      }
    }

    @Override
    public Editor putInt(String key, int value) {
      synchronized (this) {
        mModified.put(key, value);
        return this;
      }
    }

    @Override
    public Editor putLong(String key, long value) {
      synchronized (this) {
        mModified.put(key, value);
        return this;
      }
    }

    @Override
    public Editor putFloat(String key, float value) {
      synchronized (this) {
        mModified.put(key, value);
        return this;
      }
    }

    @Override
    public Editor putBoolean(String key, boolean value) {
      synchronized (this) {
        mModified.put(key, value);
        return this;
      }
    }

    @Override
    public Editor remove(String key) {
      synchronized (this) {
        mModified.put(key, this);
        return this;
      }
    }

    @Override
    public Editor clear() {
      synchronized (this) {
        mClear = true;
        return this;
      }
    }

    @Override
    public void apply() {
      final MemoryCommitResult mcr = commitToMemory();

      DefaultSharedPreferences.this.enqueueDiskWrite(mcr, null);

      // Okay to notify the listeners before it's hit disk
      // because the listeners should always get the same
      // SharedPreferences instance back, which has the
      // changes reflected in memory.
      notifyListeners(mcr);
    }

    // Returns true if any changes were made
    private MemoryCommitResult commitToMemory() {
      MemoryCommitResult mcr = new MemoryCommitResult();
      synchronized (DefaultSharedPreferences.this) {
        mcr.mapToWriteToDisk = mMap;

        boolean hasListeners = mListeners.size() > 0;
        if (hasListeners) {
          mcr.keysModified = new ArrayList<String>();
          mcr.listeners = new HashSet<OnSharedPreferenceChangeListener>(mListeners.keySet());
        }

        synchronized (this) {
          if (mClear) {
            if (!mMap.isEmpty()) {
              mcr.changesMade = true;
              mMap.clear();
            }
            mClear = false;
          }

          for (Map.Entry<String, Object> e : mModified.entrySet()) {
            String k = e.getKey();
            Object v = e.getValue();
            if (v == this) { // magic value for a removal mutation
              if (!mMap.containsKey(k)) {
                continue;
              }
              mMap.remove(k);
            } else {
              boolean isSame = false;
              if (mMap.containsKey(k)) {
                Object existingValue = mMap.get(k);
                if (existingValue != null && existingValue.equals(v)) {
                  continue;
                }
              }
              mMap.put(k, v);
            }

            mcr.changesMade = true;
            if (hasListeners) {
              mcr.keysModified.add(k);
            }
          }

          mModified.clear();
        }
      }
      return mcr;
    }

    @Override
    public boolean commit() {
      MemoryCommitResult mcr = commitToMemory();
      /* sync write on this thread okay */
      DefaultSharedPreferences.this.enqueueDiskWrite(mcr, null);

      notifyListeners(mcr);
      return mcr.writeToDiskResult;
    }

    private void notifyListeners(final MemoryCommitResult mcr) {
      if (mcr.listeners == null || mcr.keysModified == null || mcr.keysModified.size() == 0) {
        return;
      }
      if (Looper.myLooper() == Looper.getMainLooper()) {
        for (int i = mcr.keysModified.size() - 1; i >= 0; i--) {
          final String key = mcr.keysModified.get(i);
          for (OnSharedPreferenceChangeListener listener : mcr.listeners) {
            if (listener != null) {
              listener.onSharedPreferenceChanged(DefaultSharedPreferences.this, key);
            }
          }
        }
      } else {
        // Run this function on the main thread.
        ActivityThread.sMainThreadHandler.post(new Runnable() {
          @Override
          public void run() {
            notifyListeners(mcr);
          }
        });
      }
    }
  }

  @Override
  public Map<OnSharedPreferenceChangeListener, Object> getListeners() {
    return mListeners;
  }

}