JPF-Android
========

JPF-Android is a model checker and analysis tool for Android applications built on Java PathFinder.

For more **information** and **instructions** on the tool please go to the tool [website](http://heila.bitbucket.org/jpf-android)

The docker image setup as a development environment for the tool can be found in the jpf-android-docker [repo](http://bitbucket.org/heila/jpf-android-docker). The container can download jpf-android and its examples.

The examples directory contains simple example applications but the full set of example apps can be found in the [jpf-android-examples](http://bitbucket.org/heila/jpf-android-examples) repo. This repo must be checked out in the directory containing the apps. The app code is also available at http://bitbucket.org/heila/jpf-android-examples/apps.tar.gz. 

The master branch is the lastest unstable development branch. If you require a stable release please check-out one of the release branches.

License
-------

This software is distributed under the terms of the Apache Software License 2.0.
See the LICENSE file for further details.
